(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-user-management-user-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/user-management/role/role.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-management/role/role.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #topdiv> </div>\r\n<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\" [hidden]=\"!RoleList\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>Role Master</h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                        <div class=\"table-responsive\">\r\n                            <table style=\"margin-top: -20px;\">\r\n                                <tr>\r\n                                    <td style=\"width: 10%;\">\r\n                                        <i *ngIf=\"IsMaker\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; margin-left: 10px; vertical-align: middle; cursor: pointer !important; color: #589092;\"\r\n                                            title=\"Create (alt+c)\" (click)=\"createRole()\">add</i>\r\n                                        <i *ngIf=\"IsExport\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; cursor: pointer !important; vertical-align: middle; margin-left: 15px; color: #589092;\"\r\n                                            title=\"Export (alt+x)\" (click)=\"exportRole()\">play_for_work</i>\r\n                                    </td>\r\n                                    <td>\r\n                                        <mat-form-field>\r\n                                            <input matInput (keyup)=\"applyFilter($event.target.value)\"\r\n                                                placeholder=\"Filter\" autocomplete=\"off\">\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n                                <ng-container matColumnDef=\"Id\">\r\n                                    <th mat-header-cell *matHeaderCellDef (mousedown)=\"onResizeColumn($event, 0)\"\r\n                                        style=\"width:20%\">\r\n                                        <span *ngIf=\"IsEdit\"> Action </span>\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                        <button *ngIf=\"IsEdit\" mat-raised-button type=\"button\" title=\"Edit\"\r\n                                            (click)=\"updateRole(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #589092;\">edit</i>\r\n                                        </button>\r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Code\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width:20%\"> Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Code}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Desc\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width:20%;\"> Description\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Desc}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"IsCentralAccess\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:20%\">Central Access\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.IsCentralAccess}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"IsLobwiseAccess\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width:20%\"> Lob wise Access\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.IsLobwiseAccess}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"IsActive\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:10%\"> IsActive\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.IsActive}}</td>\r\n                                </ng-container>\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"main-content\" [hidden]=\"!RoleCreate\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-8\">\r\n                <div class=\"card\">\r\n                    <div class=\"card-form-header-title card-header-Grid\">\r\n                        <h4 class=\"card-title\">Role Master</h4> \r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"RoleForm\">\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"Id\">\r\n                            </mat-form-field>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Code\" type=\"text\" formControlName=\"Code\" required>\r\n                                        <mat-error *ngIf=\"Code.hasError('required')\">\r\n                                            Code is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Code.hasError('pattern') && !Code.hasError('required')\">\r\n                                            Please enter a valid Code\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Code.hasError('maxLength')\">\r\n                                            Code should have less than 100 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Desc\" type=\"text\" formControlName=\"Desc\"\r\n                                            required></textarea>\r\n                                        <mat-error *ngIf=\"Desc.hasError('required')\">\r\n                                            Description is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Desc.hasError('pattern') && !Desc.hasError('required')\">\r\n                                            Please enter a valid Description\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Desc.hasError('maxLength')\">\r\n                                            Description should have less than 2000 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8\" style=\"margin-bottom: 15px;\">\r\n                                    <mat-checkbox class=\"example-full-width\" formControlName=\"IsCentralAccess\"> Is\r\n                                        Central Access\r\n                                    </mat-checkbox>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8\" style=\"margin-bottom: 15px;\">\r\n                                    <mat-checkbox class=\"example-full-width\" formControlName=\"IsLobwiseAccess\">\r\n                                        IsLobwiseAccess\r\n                                    </mat-checkbox>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8\" style=\"margin-bottom: 15px;\">\r\n                                    <mat-checkbox class=\"example-full-width\" formControlName=\"IsActive\"> Is Active\r\n                                    </mat-checkbox>\r\n                                </div>\r\n                            </div>\r\n                            <button mat-raised-button type=\"reset\" matTooltip=\"Back (alt b)\"\r\n                                class=\"btn btn-warning pull-right\" (click)=\"backRole()\">Back</button>\r\n\r\n                            <button mat-raised-button type=\"submit\" matTooltip=\"Save (alt s)\"\r\n                                class=\"btn btn-success pull-right\" [disabled]=\"RoleForm.invalid\"\r\n                                (click)=\"saveRole()\">Save</button>\r\n                            <div class=\"clearfix\"></div>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/user-management/ui-role/ui-role.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-management/ui-role/ui-role.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>UI Role Config</h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\"> \r\n                    <div class=\"card-body\">\r\n                    <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"UIRoleConfigForm\">\r\n \r\n                        <div clas=\"row\">\r\n                            <div class=\"col-md-8\">\r\n                                <mat-form-field class=\"example-full-width\">\r\n                                <input matInput placeholder=\"Role\" type=\"text\" formControlName=\"Role\"\r\n                                    [matAutocomplete]=\"autoRole\" (input)=\"inputRole($event.target.value)\" (focus)=\"onFocusRole($event.target.value)\" required>\r\n                                <mat-autocomplete #autoRole=\"matAutocomplete\"\r\n                                    (optionSelected)=\"selectedRole($event.option.value)\"\r\n                                    [displayWith]=\"displayWithRole\"> \r\n                                    <mat-option *ngFor=\"let role of filteredRole | async\" [value]=\"role\">\r\n                                        {{ role.Code }}\r\n                                    </mat-option>\r\n                                </mat-autocomplete>\r\n                                <button mat-button *ngIf=\"Role.value\" matSuffix mat-icon-button aria-label=\"Clear\" (click)=\"clearRole()\">\r\n                                    <mat-icon> close</mat-icon>\r\n                                </button>\r\n                                <mat-error *ngIf=\"Role.hasError('required')\">\r\n                                    Role is <strong>required</strong>\r\n                                </mat-error>\r\n                                <mat-error *ngIf=\"Role.hasError('incorrect')\">\r\n                                    Please select a valid Role\r\n                                </mat-error>\r\n                            </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div clas=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <div class=\"role-table-container\">\r\n                                    <mat-table #table [dataSource]=\"dataSource\" formArrayName=\"UIRoleMap\" class=\"role-table\">\r\n                                        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                                        <mat-row [ngClass]=\"{'child-menu': row.get('ParentId').value != '0' , \r\n                                        'parent-menu' : row.get('ParentId').value == '0' }\" \r\n                                        *matRowDef=\"let row; let i= index; columns: displayedColumns;\">\r\n                                    </mat-row>\r\n\r\n                                    <ng-container matColumnDef=\"RoleId\">\r\n                                        <mat-header-cell *matHeaderCellDef> UI Menus </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let element; let index= index;\" [formGroupName]=\"index\">\r\n                                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                                <input matInput type=\"text\" formControlName=\"UIId\">\r\n                                                <input matInput type=\"text\" formControlName=\"ParentId\">\r\n                                            </mat-form-field>\r\n                                            <input matInput placeholder=\"UI\" type=\"text\" formControlName=\"UI\">\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Viewer\">\r\n                                        <mat-header-cell *matHeaderCellDef> Viewer </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let element; let index= index;\" [formGroupName]=\"index\">\r\n                                             <mat-slide-toggle [checked]=\"element.Viewer\" color='custom' formControlName=\"Viewer\">\r\n                                             </mat-slide-toggle>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Maker\">\r\n                                        <mat-header-cell *matHeaderCellDef> Maker </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let element; let index= index;\" [formGroupName]=\"index\">\r\n                                             <mat-slide-toggle [checked]=\"element.Maker\" color='custom' formControlName=\"Maker\">\r\n                                             </mat-slide-toggle>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Checker\">\r\n                                        <mat-header-cell *matHeaderCellDef> Checker </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let element; let index= index;\" [formGroupName]=\"index\">\r\n                                             <mat-slide-toggle [checked]=\"element.Checker\" color='custom' formControlName=\"Checker\">\r\n                                             </mat-slide-toggle>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Edit\">\r\n                                        <mat-header-cell *matHeaderCellDef> Edit </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let element; let index= index;\" [formGroupName]=\"index\">\r\n                                             <mat-slide-toggle [checked]=\"element.Edit\" color='custom' formControlName=\"Edit\">\r\n                                             </mat-slide-toggle>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Export\">\r\n                                        <mat-header-cell *matHeaderCellDef> Export </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let element; let index= index;\" [formGroupName]=\"index\">\r\n                                             <mat-slide-toggle [checked]=\"element.Export\" color='custom' formControlName=\"Export\">\r\n                                             </mat-slide-toggle>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Upload\">\r\n                                        <mat-header-cell *matHeaderCellDef> Upload </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let element; let index= index;\" [formGroupName]=\"index\">\r\n                                             <mat-slide-toggle [checked]=\"element.Upload\" color='custom' formControlName=\"Upload\">\r\n                                             </mat-slide-toggle>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    </mat-table>                                \r\n                                </div> \r\n                            </div>\r\n                        </div>\r\n                        <button mat-raised-button type=\"submit\" matTooltip=\"Save (alt+s)\" class=\"btn btn-success pull-right\"\r\n                        [disabled]=\"UIRoleConfigForm.invalid\" (click)=\"saveUIRoleConfig()\"> SAVE </button>\r\n                        <div class=\"clearfix\"></div>\r\n                    </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/user-management/user-management.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-management/user-management.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/user-management/user/user.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-management/user/user.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #topdiv> </div>\r\n<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\" [hidden]=\"!UserIndex\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>User Master</h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                        <div class=\"table-responsive\">\r\n                            <table style=\"margin-top: -20px;\">\r\n                                <tr>\r\n                                    <td style=\"width: 10%;\">\r\n                                        <i *ngIf=\"IsMaker\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; margin-left: 10px; vertical-align: middle; cursor: pointer !important; color: #589092;\"\r\n                                            title=\"Create (alt+c)\" (click)=\"addUser()\">add</i>\r\n                                        <i *ngIf=\"IsExport\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; cursor: pointer !important; vertical-align: middle; margin-left: 15px; color: #589092;\"\r\n                                            title=\"Export (alt+x)\" (click)=\"exportUser()\">play_for_work</i>\r\n                                    </td>\r\n                                    <td>\r\n                                        <mat-form-field>\r\n                                            <input matInput (keyup)=\"applyFilter($event.target.value)\"\r\n                                                placeholder=\"Filter\" autocomplete=\"off\">\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n                                <ng-container matColumnDef=\"Id\">\r\n                                    <th mat-header-cell *matHeaderCellDef (mousedown)=\"onResizeColumn($event, 0)\">\r\n                                        <span *ngIf=\"IsEdit\"> Action </span>\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                        <button *ngIf=\"IsEdit\" mat-raised-button type=\"button\" title=\"Edit\"\r\n                                            (click)=\"updateUser(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #589092;\">edit</i>\r\n                                        </button>\r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"LoginId\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width:10%\"> Login Id\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.LoginId}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"EmpCode\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width:15%\"> Employee Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.EmpCode}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"EmpName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:20%;\"> Employee Name\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.EmpName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"LOBCode\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width:20%\">LOB\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.LOBCode}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"SubLOBCode\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%\"> Sub LOB\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.SubLOBCode}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"RoleCode\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 6)\" style=\"width:20%\"> Role\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RoleCode}}</td>\r\n                                </ng-container>                                \r\n                                <ng-container matColumnDef=\"IsActive\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 7)\" style=\"width:10%\"> Status\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.IsActive}}</td>\r\n                                </ng-container>\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"main-content\" [hidden]=\"!UserCreate\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"card\">\r\n                    <div class=\"card-form-header-title card-header-Grid\">\r\n                        <h4 class=\"card-title\">User Master</h4> \r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"UserForm\">\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"Id\">\r\n                                <input matInput type=\"text\" formControlName=\"LOBId\">\r\n                                <input matInput type=\"text\" formControlName=\"EntityId\">\r\n                                <input matInput type=\"text\" formControlName=\"SubLOBId\">\r\n                                <input matInput type=\"text\" formControlName=\"RoleId\">\r\n                            </mat-form-field>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-8\">\r\n                                     <mat-checkbox class=\"example-full-width\" formControlName=\"ADUser\" (change)=\"onADUserChange($event)\">\r\n                                         AD User\r\n                                     </mat-checkbox>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\" [hidden]=\"!UserForm.value.ADUser\"> \r\n                                        <input matInput placeholder=\"Search Name\" type=\"text\" formControlName=\"SearchName\"\r\n                                        [matAutocomplete]=\"searchname\" (input)=\"inputUser($event.target.value)\">\r\n                                    <mat-autocomplete #searchname=\"matAutocomplete\" (optionSelected)=\"selectedUser($event.option.value)\">\r\n                                        <mat-option *ngFor=\"let user of users\" [value]=\"user\">\r\n                                            {{ user.displayName }}\r\n                                        </mat-option>\r\n                                    </mat-autocomplete>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"LoginId\" type=\"text\" formControlName=\"LoginId\" required [readonly]=\"Readonly\">\r\n                                        <mat-error *ngIf=\"LoginId.hasError('required')\">\r\n                                            Login Id is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"LoginId.hasError('pattern') && !LoginId.hasError('required')\">\r\n                                            Please enter a valid Login Id\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"LoginId.hasError('maxLength')\">\r\n                                            Login Id should have less than 100 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                             \r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"EmpCode\" type=\"text\" formControlName=\"EmpCode\" required [readonly]=\"Readonly\">\r\n                                        <mat-error *ngIf=\"EmpCode.invalid\">\r\n                                           {{ getEmpCodeErrorMessage() }}\r\n                                        </mat-error>                                       \r\n                                    </mat-form-field>\r\n                                </div>\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Employee Name\" type=\"text\" formControlName=\"EmpName\" required [readonly]=\"Readonly\">\r\n                                        <mat-error *ngIf=\"EmpName.hasError('required')\">\r\n                                            Employee Name is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"EmpName.hasError('pattern') && !EmpName.hasError('required')\">\r\n                                            Please enter a valid Employee Name\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"EmpName.hasError('maxLength')\">\r\n                                            Employee Name should have less than 100 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Email Id\" type=\"text\" formControlName=\"EmailId\"\r\n                                            required [readonly]=\"Readonly\">\r\n                                        <mat-error *ngIf=\"EmailId.hasError('required')\">\r\n                                            Email Id is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"EmailId.hasError('pattern') && !EmailId.hasError('required')\">\r\n                                            Please enter a valid Email Id\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"EmailId.hasError('maxLength')\">\r\n                                            Email Id should have less than 100 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                           \r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Entity\" type=\"text\" formControlName=\"Entity\"\r\n                                            [matAutocomplete]=\"autoEntity\" (input)=\"inputEntity($event.target.value)\"\r\n                                            >\r\n                                        <mat-autocomplete #autoEntity=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedEntity($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let entity of filteredentities | async\" [value]=\"entity\">\r\n                                                {{ entity.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"EntityId.value\" matSuffix\r\n                                            mat-icon-button aria-label=\"Clear\" (click)=\"clearEntity()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"EntityId.hasError('required')\">\r\n                                            Entity is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"EntityId.hasError('incorrect')\">\r\n                                            Please select a valid Entity\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                      <mat-chip-list #lobList>\r\n                                          <mat-chip *ngFor=\"let lob of lobs\" [selectable]=\"true\" [removable]=\"true\"\r\n                                          (removed)=\"removeLOB(lob)\">\r\n                                            {{ lob.Code }}\r\n                                            <mat-icon matChipRemove>cancel</mat-icon>\r\n                                        </mat-chip>\r\n                                        <input placeholder=\"LOB\" #lobInput formControlName=\"LOB\" [matAutocomplete]=\"autoLob\"\r\n                                        [matChipInputFor]=\"lobList\" [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n                                        [matChipInputAddOnBlur]=\"true\" (matChipInputTokenEnd)=\"addLOB($event)\">\r\n                                      </mat-chip-list>  \r\n                                      <mat-autocomplete #autoLob=\"matAutocomplete\" (optionSelected)=\"selectedLOB($event)\">\r\n                                        <mat-option *ngFor=\"let lob of filteredLobs | async\" [value]=\"lob\">\r\n                                            {{ lob.Code }}\r\n                                        </mat-option>\r\n                                      </mat-autocomplete>\r\n                                      <mat-error>\r\n                                          LOB is <strong>required</strong>\r\n                                      </mat-error>\r\n                                    </mat-form-field>\r\n                                </div> \r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                      <mat-chip-list #sublobList>\r\n                                          <mat-chip *ngFor=\"let sublob of sublobs\" [selectable]=\"true\" [removable]=\"true\"\r\n                                          (removed)=\"removeSubLOB(sublob)\">\r\n                                            {{ sublob.Code }}\r\n                                            <mat-icon matChipRemove>cancel</mat-icon>\r\n                                        </mat-chip>\r\n                                        <input placeholder=\"Sub LOB\" #sublobInput formControlName=\"SubLOB\" [matAutocomplete]=\"autoSubLob\"\r\n                                        [matChipInputFor]=\"sublobList\" [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n                                        [matChipInputAddOnBlur]=\"true\" (matChipInputTokenEnd)=\"addSubLOB($event)\">\r\n                                      </mat-chip-list>  \r\n                                      <mat-autocomplete #autoSubLob=\"matAutocomplete\" (optionSelected)=\"selectedSubLOB($event)\">\r\n                                        <mat-option *ngFor=\"let sublob of filteredSubLobs | async\" [value]=\"sublob\">\r\n                                            {{ sublob.Code }}\r\n                                        </mat-option>\r\n                                      </mat-autocomplete>\r\n                                      <mat-error>\r\n                                          Sub LOB is <strong>required</strong>\r\n                                      </mat-error>\r\n                                    </mat-form-field>\r\n                                </div> \r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                      <mat-chip-list #roleList>\r\n                                          <mat-chip *ngFor=\"let role of roles\" [selectable]=\"true\" [removable]=\"true\"\r\n                                          (removed)=\"removeRole(role)\">\r\n                                            {{ role.Code }}\r\n                                            <mat-icon matChipRemove>cancel</mat-icon>\r\n                                        </mat-chip>\r\n                                        <input placeholder=\"Role\" #roleInput formControlName=\"Role\" [matAutocomplete]=\"autoRole\"\r\n                                        [matChipInputFor]=\"roleList\" [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n                                        [matChipInputAddOnBlur]=\"true\" (matChipInputTokenEnd)=\"addRole($event)\">\r\n                                      </mat-chip-list>  \r\n                                      <mat-autocomplete #autoRole=\"matAutocomplete\" (optionSelected)=\"selectedRole($event)\">\r\n                                        <mat-option *ngFor=\"let role of filteredRoles | async\" [value]=\"role\">\r\n                                            {{ role.Code }}\r\n                                        </mat-option>\r\n                                      </mat-autocomplete>\r\n                                      <mat-error>\r\n                                          Role is <strong>required</strong>\r\n                                      </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>   \r\n                            \r\n                                <div class=\"col-md-4\" style=\"margin-bottom: 15px;\">\r\n                                    <mat-checkbox class=\"example-full-width\" formControlName=\"IsActive\"> Is Active\r\n                                    </mat-checkbox>\r\n                                </div>\r\n                            </div>\r\n                            <button mat-raised-button type=\"reset\" matTooltip=\"Back (alt b)\"\r\n                                class=\"btn btn-warning pull-right\" (click)=\"backUser()\">Back</button>\r\n\r\n                            <button mat-raised-button type=\"submit\" matTooltip=\"Save (alt s)\"\r\n                                class=\"btn btn-success pull-right\" [disabled]=\"UserForm.invalid\"\r\n                                (click)=\"saveUser()\">Save</button>\r\n                            <div class=\"clearfix\"></div>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/user-management/role/role.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/user-management/role/role.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #044da1;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n\n.card-form-header-title {\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  border-radius: 0px;\n  width: 100%;\n  padding: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L3JvbGUvRTpcXERBUiBVQVQgRmlsZXNcXEFzc2V0LVVJL3NyY1xcYXBwXFx1c2VyLW1hbmFnZW1lbnRcXHJvbGVcXHJvbGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3VzZXItbWFuYWdlbWVudC9yb2xlL3JvbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtBQ0NKOztBREVBO0VBQ0ksMEJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBQ0NKOztBREVBOztFQUVJLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBRUkscUJBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3Q0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3VzZXItbWFuYWdlbWVudC9yb2xlL3JvbGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxudGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItY2VsbHtcclxuICAgIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50OyBcclxuICAgIGNvbG9yOiAjMDQ0ZGExO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGN1cnNvcjogY29sLXJlc2l6ZTtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcblxyXG50ci5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50OyBcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcclxuICAgIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xyXG59XHJcbiAgXHJcbi5tYXQtdGFibGV7XHJcbiAgICBib3JkZXItc3BhY2luZzogMDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCwgIFxyXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xyXG4gICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItY29sb3I6ICNlYWVlZWY7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDsgXHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlci10aXRsZXtcclxuICAgIHdpZHRoOiA1NSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYig3MiwgMTMwLCAxOTcpO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xyXG4gICAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzOyBcclxuICAgIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbn1cclxuLmNhcmQtZm9ybS1oZWFkZXItdGl0bGUge1xyXG5cclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcclxuICAgIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMzsgXHJcbiAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDhweDtcclxufSIsIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtaGVhZGVyLWNlbGwge1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgY29sb3I6ICMwNDRkYTE7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGN1cnNvcjogY29sLXJlc2l6ZTtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxudHIubWF0LWhlYWRlci1yb3cge1xuICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcbiAgaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5tYXQtdGFibGUge1xuICBib3JkZXItc3BhY2luZzogMDtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG50ZC5tYXQtY2VsbCwgdGQubWF0LWZvb3Rlci1jZWxsLFxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmNhcmQtaGVhZGVyLXRpdGxlIHtcbiAgd2lkdGg6IDU1JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcbiAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xuICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7XG4gIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuLmNhcmQtZm9ybS1oZWFkZXItdGl0bGUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogOHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/user-management/role/role.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user-management/role/role.component.ts ***!
  \********************************************************/
/*! exports provided: RoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleComponent", function() { return RoleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var RoleComponent = /** @class */ (function () {
    function RoleComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.RoleList = false;
        this.RoleCreate = false;
        this.displayedColumns = [];
        this.columns = [
            { field: 'Id', width: 5 }, { field: 'Code', width: 30 }, { field: 'Desc', width: 60 }, { field: 'IsCentralAccess', width: 5 }, { field: 'IsLobwiseAccess', width: 5 }, { field: 'IsActive', width: 5 }
        ];
        this.location = location;
    }
    RoleComponent.prototype.ngOnInit = function () {
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.IsEdit = this.UIObj.UIRoles[0].Edit;
        this.RoleForm = this.formBuilder.group({
            Id: [''],
            Code: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z0-9\\s\\(\\)\\-&.\\\\,/_]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            Desc: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z0-9\\s\\(\\)\\-&.\\\\,/_]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(2000)]],
            IsActive: [''],
            IsCentralAccess: [''],
            IsLobwiseAccess: ['']
        });
        this.getAllRoles();
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
    };
    Object.defineProperty(RoleComponent.prototype, "Id", {
        get: function () { return this.RoleForm.get('Id'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RoleComponent.prototype, "Code", {
        get: function () { return this.RoleForm.get('Code'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RoleComponent.prototype, "Desc", {
        get: function () { return this.RoleForm.get('Desc'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RoleComponent.prototype, "IsActive", {
        get: function () { return this.RoleForm.get('IsActive'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RoleComponent.prototype, "IsCentralAccess", {
        get: function () { return this.RoleForm.get('IsCentralAccess'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RoleComponent.prototype, "IsLobwiseAccess", {
        get: function () { return this.RoleForm.get('IsLobwiseAccess'); },
        enumerable: true,
        configurable: true
    });
    RoleComponent.prototype.getAllRoles = function () {
        var _this = this;
        this.rest.getAll(this.global.getapiendpoint() + 'role/GetAllRole').subscribe(function (data) {
            var tableData = [];
            data.Data.forEach(function (element) {
                tableData.push({ Id: element.Id, Code: element.Code, Desc: element.Desc,
                    IsCentralAccess: element.IsCentralAccess ? "Yes" : "No", IsLobwiseAccess: element.IsLobwiseAccess ? "Yes" : "No",
                    IsActive: element.IsActive ? "Active" : "Inactive" });
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](tableData);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
        this.RoleCreate = false;
        this.RoleList = true;
    };
    RoleComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    RoleComponent.prototype.createRole = function () {
        this.form.resetForm();
        this.RoleForm.markAsUntouched();
        this.Code.enable();
        this.Id.setValue('');
        this.IsActive.setValue(true);
        this.RoleCreate = true;
        this.RoleList = false;
    };
    RoleComponent.prototype.backRole = function () {
        this.RoleCreate = false;
        this.RoleList = true;
        this.topdiv.nativeElement.scrollIntoView({ behavior: 'smooth' });
    };
    RoleComponent.prototype.updateRole = function (Id) {
        var _this = this;
        this.rest.getById(this.global.getapiendpoint() + 'role/GetRoleById/', Id).subscribe(function (data) {
            _this.Id.setValue(data.Data.Id);
            _this.Code.setValue(data.Data.Code);
            _this.Code.disable();
            _this.Desc.setValue(data.Data.Desc);
            _this.IsActive.setValue(data.Data.IsActive);
            _this.IsCentralAccess.setValue(data.Data.IsCentralAccess);
            _this.IsLobwiseAccess.setValue(data.Data.IsLobwiseAccess);
            _this.RoleCreate = true;
            _this.RoleList = false;
        });
    };
    RoleComponent.prototype.exportRole = function () {
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'Role');
        this.toastr.showNotification('top', 'right', 'Exported Successfully', 'success');
    };
    RoleComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    RoleComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    RoleComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    RoleComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].A_KEY) {
            this.createRole();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].B_KEY) {
            this.backRole();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].U_KEY) {
            // this.uploadRole();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].S_KEY) {
            this.saveRole();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].X_KEY) {
            this.exportRole();
        }
    };
    RoleComponent.prototype.saveRole = function () {
        var _this = this;
        this.rest.checkDuplicate(this.global.getapiendpoint() + 'role/CheckDuplicateRole/', this.Code.value.toString().trim(), (this.Id.value !== '' ? this.Id.value : '0')).subscribe(function (data) {
            if (data.Data) {
                _this.toastr.showNotification('top', 'right', data.Message, 'danger');
            }
            else {
                var model = {
                    Id: _this.Id.value,
                    Code: _this.Code.value.trim(),
                    Desc: _this.Desc.value.trim(),
                    IsActive: _this.IsActive.value,
                    IsCentralAccess: _this.IsCentralAccess.value,
                    IsLobwiseAccess: _this.IsLobwiseAccess.value,
                    UserId: _this.userLoggedIn.Id,
                    UserRoleId: _this.userLoggedIn.DefaultRoleId
                };
                var apiUrl = '';
                if (_this.Id.value == '') {
                    apiUrl = 'role/CreateRole';
                }
                else {
                    apiUrl = 'role/UpdateRole';
                }
                _this.rest.create(_this.global.getapiendpoint() + apiUrl, model).subscribe(function (data) {
                    if (data.Success) {
                        _this.toastr.showNotification('top', 'right', data.Message, 'success');
                    }
                    else {
                        _this.toastr.showNotification('top', 'right', data.Message, 'danger');
                    }
                    _this.getAllRoles();
                });
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], RoleComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('topdiv', { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], RoleComponent.prototype, "topdiv", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], RoleComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], RoleComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], RoleComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"])
    ], RoleComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], RoleComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], RoleComponent.prototype, "keyEvent", null);
    RoleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-role',
            template: __webpack_require__(/*! raw-loader!./role.component.html */ "./node_modules/raw-loader/index.js!./src/app/user-management/role/role.component.html"),
            styles: [__webpack_require__(/*! ./role.component.scss */ "./src/app/user-management/role/role.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExcelService"]])
    ], RoleComponent);
    return RoleComponent;
}());



/***/ }),

/***/ "./src/app/user-management/ui-role/ui-role.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/user-management/ui-role/ui-role.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\n.child-menu {\n  background-color: #eff0f1;\n}\n\n.parent-menu {\n  background-color: #ffeae8;\n}\n\n.role-container {\n  position: relative;\n  min-height: 200px;\n}\n\n.role-table-container {\n  position: relative;\n  max-height: 400px;\n  overflow: auto;\n}\n\n.role-table-loading-shade {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 56px;\n  right: 0;\n  background: rgba(0, 0, 0, 0.15);\n  z-index: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\nmat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #044da1;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n\n.card-form-header-title {\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  border-radius: 0px;\n  width: 100%;\n  padding: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L3VpLXJvbGUvRTpcXERBUiBVQVQgRmlsZXNcXEFzc2V0LVVJL3NyY1xcYXBwXFx1c2VyLW1hbmFnZW1lbnRcXHVpLXJvbGVcXHVpLXJvbGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3VzZXItbWFuYWdlbWVudC91aS1yb2xlL3VpLXJvbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtBQ0NKOztBREVBO0VBQ0kseUJBQUE7QUNDSjs7QURFQTtFQUNJLHlCQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0VBQ0EsK0JBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNDSjs7QURFQTtFQUNJLHdCQUFBO0FDQ0o7O0FERUE7RUFDSSxXQUFBO0FDQ0o7O0FERUE7RUFDSSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSw2QkFBQTtFQUNBLHNCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FDQ0o7O0FERUE7O0VBRUksbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1DQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FEQ0E7RUFFSSxxQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L3VpLXJvbGUvdWktcm9sZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xyXG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY2hpbGQtbWVudSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWZmMGYxO1xyXG59XHJcblxyXG4ucGFyZW50LW1lbnUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZWFlODtcclxufVxyXG5cclxuLnJvbGUtY29udGFpbmVyIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xyXG59XHJcblxyXG4ucm9sZS10YWJsZS1jb250YWluZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWF4LWhlaWdodDogNDAwcHg7XHJcbiAgICBvdmVyZmxvdzogYXV0bztcclxufVxyXG5cclxuLnJvbGUtdGFibGUtbG9hZGluZy1zaGFkZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgYm90dG9tOiA1NnB4O1xyXG4gICAgcmlnaHQgOiAwO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgwLDAsMCwgMC4xNSk7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGRbaGlkZGVuXSB7XHJcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbnRhYmxle1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGx7XHJcbiAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDsgXHJcbiAgICBjb2xvcjogIzA0NGRhMTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBjdXJzb3I6IGNvbC1yZXNpemU7XHJcbiAgICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufVxyXG5cclxudHIubWF0LWhlYWRlci1yb3cge1xyXG4gICAgaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDsgXHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG50ci5tYXQtZm9vdGVyLXJvdywgdHIubWF0LXJvdyB7XHJcbiAgICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcclxufVxyXG4gIFxyXG4ubWF0LXRhYmxle1xyXG4gICAgYm9yZGVyLXNwYWNpbmc6IDA7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZjdmNGYwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsICBcclxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDEycHg7IFxyXG59XHJcblxyXG4uY2FyZC1oZWFkZXItdGl0bGV7XHJcbiAgICB3aWR0aDogNTUlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNzIsIDEzMCwgMTk3KTtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcclxuICAgIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMzsgXHJcbiAgICBwYWRkaW5nOiA0cHggNHB4IDBweCAxNHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG59XHJcbi5jYXJkLWZvcm0taGVhZGVyLXRpdGxlIHtcclxuXHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0ODgyYzU7XHJcbiAgICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XHJcbiAgICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7IFxyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiA4cHg7XHJcbn0iLCJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4uY2hpbGQtbWVudSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZmYwZjE7XG59XG5cbi5wYXJlbnQtbWVudSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmVhZTg7XG59XG5cbi5yb2xlLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogMjAwcHg7XG59XG5cbi5yb2xlLXRhYmxlLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWF4LWhlaWdodDogNDAwcHg7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuXG4ucm9sZS10YWJsZS1sb2FkaW5nLXNoYWRlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogNTZweDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG4gIHotaW5kZXg6IDE7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG5tYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG50YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubWF0LWhlYWRlci1jZWxsIHtcbiAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjMDQ0ZGExO1xuICBmb250LXdlaWdodDogNDAwO1xuICBjdXJzb3I6IGNvbC1yZXNpemU7XG4gIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICNlYWVlZWY7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG5cbnRyLm1hdC1oZWFkZXItcm93IHtcbiAgaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG50ci5tYXQtZm9vdGVyLXJvdywgdHIubWF0LXJvdyB7XG4gIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xufVxuXG4ubWF0LXRhYmxlIHtcbiAgYm9yZGVyLXNwYWNpbmc6IDA7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgYm9yZGVyLWNvbG9yOiAjZjdmNGYwO1xuICB3aWR0aDogMTAwJTtcbn1cblxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCxcbi5tYXQtY2VsbCwgLm1hdC1mb290ZXItY2VsbCB7XG4gIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICNlYWVlZWY7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5jYXJkLWhlYWRlci10aXRsZSB7XG4gIHdpZHRoOiA1NSU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbGluZS1oZWlnaHQ6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0ODgyYzU7XG4gIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcbiAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzO1xuICBwYWRkaW5nOiA0cHggNHB4IDBweCAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbi5jYXJkLWZvcm0taGVhZGVyLXRpdGxlIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcbiAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xuICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDhweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/user-management/ui-role/ui-role.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/user-management/ui-role/ui-role.component.ts ***!
  \**************************************************************/
/*! exports provided: UIRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UIRoleComponent", function() { return UIRoleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/validators/requiredmatch.validator */ "./src/app/validators/requiredmatch.validator.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var UIRoleComponent = /** @class */ (function () {
    function UIRoleComponent(formBuilder, rest, route, router, global, toastr) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.uiRoleMapStatus = 'create';
        this.displayedColumns = ['RoleId', 'Viewer', 'Maker', 'Checker', 'Edit', 'Export', 'Upload'];
        this.UIRoleMapDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](this.UIRoleMapData);
        this.dataSource = new rxjs__WEBPACK_IMPORTED_MODULE_8__["BehaviorSubject"]([]);
        this.uimenus = [];
        this.roles = [];
        this.isLoadingResults = false;
        this.rows = this.formBuilder.array([]);
        this.UIRoleConfigForm = this.formBuilder.group({
            RoleId: [''],
            Role: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_9__["RequireMatch"]]],
            'UIRoleMap': this.rows
        });
    }
    UIRoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userLoggedIn = JSON.parse(localStorage.getItem("userLoggedIn"));
        this.getMenuList();
        this.getRoleList();
        this.UIRoleMapData = [];
        this.UIRoleMapData.forEach(function (d) { return _this.addRow(d, false); });
        this.updateView();
        this.filteredRole = this.Role.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["map"])(function (value) {
            return value ? _this._filterRoles(value) : _this.roles.slice();
        }));
    };
    UIRoleComponent.prototype.emptyTable = function () {
        while (this.rows.length !== 0) {
            this.rows.removeAt(0);
        }
    };
    UIRoleComponent.prototype.addRow = function (d, noUpdate) {
        var uiTitle = this.uimenus.find(function (x) { return x.Id == d.UIId; });
        uiTitle = uiTitle ? uiTitle.Title : '';
        var row = this.formBuilder.group({
            'UIId': [d && d.UIId ? d.UIId : null, []],
            'UI': [d && uiTitle ? uiTitle : null, []],
            'ParentId': [d && d.ParentId ? d.ParentId : null, []],
            'Viewer': [d && d.Viewer ? d.Viewer : null, []],
            'Maker': [d && d.Maker ? d.Maker : null, []],
            'Checker': [d && d.Checker ? d.Checker : null, []],
            'Edit': [d && d.Edit ? d.Edit : null, []],
            'Export': [d && d.Export ? d.Export : null, []],
            'Upload': [d && d.Upload ? d.Upload : null, []],
        });
        this.rows.push(row);
        if (!noUpdate) {
            this.updateView();
        }
    };
    UIRoleComponent.prototype.updateView = function () {
        this.dataSource.next(this.rows.controls);
    };
    UIRoleComponent.prototype.clearView = function () {
        this.UIRoleMapData = [];
        this.dataSource.next(this.rows.controls);
    };
    Object.defineProperty(UIRoleComponent.prototype, "RoleId", {
        get: function () { return this.UIRoleConfigForm.get('RoleId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UIRoleComponent.prototype, "Role", {
        get: function () { return this.UIRoleConfigForm.get('Role'); },
        enumerable: true,
        configurable: true
    });
    UIRoleComponent.prototype.inputRole = function (role) {
        this.RoleId.setValue(null);
    };
    UIRoleComponent.prototype.onFocusRole = function (role) {
        var _this = this;
        this.filteredRole = this.Role.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["map"])(function (value) {
            return value ? _this._filterRoles(value) : _this.roles.slice();
        }));
    };
    UIRoleComponent.prototype.selectedRole = function (role) {
        this.RoleId.setValue(role.Id);
        this.loadUIRoleMapping(role.Id);
    };
    UIRoleComponent.prototype.clearRole = function () {
        this.RoleId.setValue(null);
        this.Role.setValue(null);
        this.emptyTable();
        this.updateView();
        this.UIRoleConfigForm.reset();
        this.form.resetForm();
        this.UIRoleConfigForm.markAsUntouched();
    };
    UIRoleComponent.prototype._filterRoles = function (value) {
        var filterValue = (value ? (value.Code ? value.Code.toLowerCase() : value.toLowerCase()) : '');
        return this.roles.filter(function (o) { return o.Code.toLowerCase().includes(filterValue); });
    };
    UIRoleComponent.prototype.displayWithRole = function (obj) {
        return obj ? obj.Code : undefined;
    };
    UIRoleComponent.prototype.getMenuList = function () {
        var _this = this;
        this.uimenus = [];
        this.rest.getAll(this.global.getapiendpoint() + 'menu/GetAllActiveMenu/').subscribe(function (data) {
            _this.uimenus = data.Data;
        });
    };
    UIRoleComponent.prototype.getRoleList = function () {
        var _this = this;
        this.roleList = [];
        this.rest.getAll(this.global.getapiendpoint() + 'role/GetAllActiveRole').subscribe(function (data) {
            _this.roleList = data.Data;
            _this.roles = data.Data;
        });
    };
    UIRoleComponent.prototype.saveUIRoleConfig = function () {
        var _this = this;
        var UIRoleMap = this.UIRoleConfigForm.get('UIRoleMap').value;
        var model = {
            RoleId: this.RoleId.value,
            UIRoleMap: JSON.stringify(UIRoleMap),
            UserId: this.userLoggedIn.Id,
            UserRoleId: this.userLoggedIn.DefaultRoleId
        };
        this.rest.create(this.global.getapiendpoint() + 'uirolemap/CreateUIRoleMap', model).subscribe(function (data) {
            _this.toastr.showNotification('top', 'right', 'Mapping Successfully Added', 'success');
            _this.emptyTable();
            _this.updateView();
            _this.UIRoleConfigForm.reset();
            _this.form.resetForm();
            _this.UIRoleConfigForm.markAsUntouched();
        });
    };
    UIRoleComponent.prototype.loadUIRoleMapping = function (roleId) {
        var _this = this;
        this.isLoadingResults = true;
        this.emptyTable();
        this.updateView();
        this.rest.getById(this.global.getapiendpoint() + 'uirolemap/GetUIRoleMap/', roleId).subscribe(function (data) {
            if (data.Success == true) {
                if (data.Data.length !== 0) {
                    _this.uiRoleMapStatus = 'update';
                    _this.UIRoleMapData = data.Data;
                    _this.uimenus.forEach(function (menu) {
                        var UIRoleMap = _this.UIRoleMapData.find(function (e) { return e.UIId === menu.Id; });
                        if (UIRoleMap) {
                            _this.addRow({
                                UIId: menu.Id,
                                UI: menu.Title,
                                ParentId: menu.ParentId,
                                Viewer: UIRoleMap.Viewer,
                                Maker: UIRoleMap.Maker,
                                Checker: UIRoleMap.Checker,
                                Edit: UIRoleMap.Edit,
                                Export: UIRoleMap.Export,
                                Upload: UIRoleMap.Upload
                            }, false);
                        }
                        else {
                            _this.addRow({
                                UIId: menu.Id,
                                UI: menu.Title,
                                ParentId: menu.ParentId,
                                Viewer: false,
                                Maker: false,
                                Checker: false,
                                Edit: false,
                                Export: false,
                                Upload: false,
                            }, false);
                        }
                    });
                    _this.updateView();
                    _this.isLoadingResults = false;
                }
                else {
                    _this.setDefaultData();
                    _this.updateView();
                    _this.uiRoleMapStatus = 'create';
                }
            }
            else {
                console.error(data);
            }
        });
    };
    UIRoleComponent.prototype.setDefaultData = function () {
        var _this = this;
        var menu_list = this.uimenus;
        menu_list.forEach(function (d) {
            _this.addRow({
                UIId: d.Id,
                UI: d.Title,
                ParentId: d.ParentId,
                Viewer: false,
                Maker: false,
                Checker: false,
                Edit: false,
                Export: false,
                Upload: false
            }, false);
        });
        this.updateView();
    };
    UIRoleComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_7__["KEY_CODE"].S_KEY) {
            this.saveUIRoleConfig();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], UIRoleComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], UIRoleComponent.prototype, "keyEvent", null);
    UIRoleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ui-role',
            template: __webpack_require__(/*! raw-loader!./ui-role.component.html */ "./node_modules/raw-loader/index.js!./src/app/user-management/ui-role/ui-role.component.html"),
            styles: [__webpack_require__(/*! ./ui-role.component.scss */ "./src/app/user-management/ui-role/ui-role.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"]])
    ], UIRoleComponent);
    return UIRoleComponent;
}());



/***/ }),

/***/ "./src/app/user-management/user-management.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/user-management/user-management.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #589092;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L0U6XFxEQVIgVUFUIEZpbGVzXFxBc3NldC1VSS9zcmNcXGFwcFxcdXNlci1tYW5hZ2VtZW50XFx1c2VyLW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3VzZXItbWFuYWdlbWVudC91c2VyLW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtBQ0NKOztBREVBO0VBQ0ksMEJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBQ0NKOztBREVBOztFQUVJLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L3VzZXItbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xyXG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG50YWJsZXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1jZWxse1xyXG4gICAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7IFxyXG4gICAgY29sb3I6ICM1ODkwOTI7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY3Vyc29yOiBjb2wtcmVzaXplO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItY29sb3I6ICNlYWVlZWY7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbn1cclxuXHJcbnRyLm1hdC1oZWFkZXItcm93IHtcclxuICAgIGhlaWdodDogNDBweCAhaW1wb3J0YW50O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7IFxyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG5cclxudHIubWF0LWZvb3Rlci1yb3csIHRyLm1hdC1yb3cge1xyXG4gICAgaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuICBcclxuLm1hdC10YWJsZXtcclxuICAgIGJvcmRlci1zcGFjaW5nOiAwO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIGJvcmRlci1jb2xvcjogI2Y3ZjRmMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG50ZC5tYXQtY2VsbCwgdGQubWF0LWZvb3Rlci1jZWxsLCAgXHJcbi5tYXQtY2VsbCwgLm1hdC1mb290ZXItY2VsbCB7XHJcbiAgICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcCAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAxMnB4OyBcclxufVxyXG5cclxuLmNhcmQtaGVhZGVyLXRpdGxle1xyXG4gICAgd2lkdGg6IDU1JTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDcyLCAxMzAsIDE5Nyk7XHJcbiAgICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XHJcbiAgICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7IFxyXG4gICAgcGFkZGluZzogNHB4IDRweCAwcHggMTRweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxufSIsIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtaGVhZGVyLWNlbGwge1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgY29sb3I6ICM1ODkwOTI7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGN1cnNvcjogY29sLXJlc2l6ZTtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxudHIubWF0LWhlYWRlci1yb3cge1xuICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcbiAgaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5tYXQtdGFibGUge1xuICBib3JkZXItc3BhY2luZzogMDtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG50ZC5tYXQtY2VsbCwgdGQubWF0LWZvb3Rlci1jZWxsLFxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmNhcmQtaGVhZGVyLXRpdGxlIHtcbiAgd2lkdGg6IDU1JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcbiAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xuICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7XG4gIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/user-management/user-management.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/user-management/user-management.component.ts ***!
  \**************************************************************/
/*! exports provided: UserManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserManagementComponent", function() { return UserManagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserManagementComponent = /** @class */ (function () {
    function UserManagementComponent() {
    }
    UserManagementComponent.prototype.ngOnInit = function () { };
    UserManagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! raw-loader!./user-management.component.html */ "./node_modules/raw-loader/index.js!./src/app/user-management/user-management.component.html"),
            styles: [__webpack_require__(/*! ./user-management.component.scss */ "./src/app/user-management/user-management.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UserManagementComponent);
    return UserManagementComponent;
}());



/***/ }),

/***/ "./src/app/user-management/user-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/user-management/user-routing.module.ts ***!
  \********************************************************/
/*! exports provided: UserRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRoutingModule", function() { return UserRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/common/auth.guard */ "./src/app/common/auth.guard.ts");
/* harmony import */ var _user_management_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-management.component */ "./src/app/user-management/user-management.component.ts");
/* harmony import */ var _role_role_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./role/role.component */ "./src/app/user-management/role/role.component.ts");
/* harmony import */ var _ui_role_ui_role_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ui-role/ui-role.component */ "./src/app/user-management/ui-role/ui-role.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user-management/user/user.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '', component: _user_management_component__WEBPACK_IMPORTED_MODULE_3__["UserManagementComponent"], children: [
            { path: 'role', component: _role_role_component__WEBPACK_IMPORTED_MODULE_4__["RoleComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
            { path: 'user', component: _user_user_component__WEBPACK_IMPORTED_MODULE_6__["UserComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
            { path: 'uiroleconfig', component: _ui_role_ui_role_component__WEBPACK_IMPORTED_MODULE_5__["UIRoleComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
        ]
    }
];
var UserRoutingModule = /** @class */ (function () {
    function UserRoutingModule() {
    }
    UserRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UserRoutingModule);
    return UserRoutingModule;
}());



/***/ }),

/***/ "./src/app/user-management/user.module.ts":
/*!************************************************!*\
  !*** ./src/app/user-management/user.module.ts ***!
  \************************************************/
/*! exports provided: UserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _user_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-routing.module */ "./src/app/user-management/user-routing.module.ts");
/* harmony import */ var app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _user_management_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-management.component */ "./src/app/user-management/user-management.component.ts");
/* harmony import */ var _role_role_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./role/role.component */ "./src/app/user-management/role/role.component.ts");
/* harmony import */ var _ui_role_ui_role_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ui-role/ui-role.component */ "./src/app/user-management/ui-role/ui-role.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user-management/user/user.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _user_routing_module__WEBPACK_IMPORTED_MODULE_2__["UserRoutingModule"],
                app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            declarations: [
                _user_management_component__WEBPACK_IMPORTED_MODULE_4__["UserManagementComponent"],
                _role_role_component__WEBPACK_IMPORTED_MODULE_5__["RoleComponent"],
                _ui_role_ui_role_component__WEBPACK_IMPORTED_MODULE_6__["UIRoleComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_7__["UserComponent"]
            ],
            exports: [
                app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            entryComponents: []
        })
    ], UserModule);
    return UserModule;
}());



/***/ }),

/***/ "./src/app/user-management/user/user.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/user-management/user/user.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #044da1;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n\n.card-form-header-title {\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  border-radius: 3px;\n  width: 100%;\n  padding: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L3VzZXIvRTpcXERBUiBVQVQgRmlsZXNcXEFzc2V0LVVJL3NyY1xcYXBwXFx1c2VyLW1hbmFnZW1lbnRcXHVzZXJcXHVzZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3VzZXItbWFuYWdlbWVudC91c2VyL3VzZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtBQ0NKOztBREVBO0VBQ0ksMEJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBQ0NKOztBREVBOztFQUVJLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBRUkscUJBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3Q0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3VzZXItbWFuYWdlbWVudC91c2VyL3VzZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxudGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItY2VsbHtcclxuICAgIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50OyBcclxuICAgIGNvbG9yOiAjMDQ0ZGExO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGN1cnNvcjogY29sLXJlc2l6ZTtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcblxyXG50ci5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50OyBcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcclxuICAgIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xyXG59XHJcbiAgXHJcbi5tYXQtdGFibGV7XHJcbiAgICBib3JkZXItc3BhY2luZzogMDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCwgIFxyXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xyXG4gICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItY29sb3I6ICNlYWVlZWY7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDsgXHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlci10aXRsZXtcclxuICAgIHdpZHRoOiA1NSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYig3MiwgMTMwLCAxOTcpO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xyXG4gICAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzOyBcclxuICAgIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbn1cclxuLmNhcmQtZm9ybS1oZWFkZXItdGl0bGUge1xyXG5cclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcclxuICAgIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMzsgXHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDhweDtcclxufSIsIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtaGVhZGVyLWNlbGwge1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgY29sb3I6ICMwNDRkYTE7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGN1cnNvcjogY29sLXJlc2l6ZTtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxudHIubWF0LWhlYWRlci1yb3cge1xuICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcbiAgaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5tYXQtdGFibGUge1xuICBib3JkZXItc3BhY2luZzogMDtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG50ZC5tYXQtY2VsbCwgdGQubWF0LWZvb3Rlci1jZWxsLFxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmNhcmQtaGVhZGVyLXRpdGxlIHtcbiAgd2lkdGg6IDU1JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcbiAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xuICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7XG4gIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuLmNhcmQtZm9ybS1oZWFkZXItdGl0bGUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogOHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/user-management/user/user.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user-management/user/user.component.ts ***!
  \********************************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/cdk/keycodes */ "./node_modules/@angular/cdk/esm5/keycodes.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var UserComponent = /** @class */ (function () {
    function UserComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.UserIndex = false;
        this.UserCreate = false;
        this.users = [];
        this.Readonly = true;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__["COMMA"]];
        this.lobs = [];
        this.allLobs = [];
        this.allLobsInit = [];
        this.entities = [];
        this.sublobs = [];
        this.allSubLobs = [];
        this.allSubLobsInit = [];
        this.roles = [];
        this.allRoles = [];
        this.allRolesInit = [];
        this.displayedColumns = [];
        this.columns = [
            { field: 'Id', width: 5 }, { field: 'LoginId', width: 30 }, { field: 'EmpCode', width: 60 },
            { field: 'EmpName', width: 5 }, { field: 'LOBCode', width: 5 }, { field: 'SubLOBCode', width: 5 }, { field: 'RoleCode', width: 5 },
            { field: 'IsActive', width: 5 }
        ];
        this.location = location;
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.IsEdit = this.UIObj.UIRoles[0].Edit;
        this.UserForm = this.formBuilder.group({
            Id: [''],
            LOBId: [''],
            SubLOBId: [''],
            RoleId: [''],
            ADUser: [''],
            SearchName: [''],
            LoginId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z0-9_.]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            EmpCode: [''],
            EmpName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z0-9\\s\\(\\)\\-&.\\\\,/_]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            IsActive: [''],
            EmailId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            EntityId: [''],
            Entity: [''],
            LOB: [''],
            SubLOB: [''],
            Activity: [''],
            Users: [''],
            Role: ['']
        });
        this.getAllUsers();
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.getAllEntity();
        this.getAllLOB();
        this.getAllRole();
        this.SearchName.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["debounceTime"])(500)).subscribe(function (text) {
            if (text != null && text != '') {
                _this.users = [];
                _this.getADUsers(text);
            }
            else {
                _this.users = [];
            }
        });
        this.filteredLobs = this.LOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allLobs) : _this.allLobs.slice(); }));
        this.filteredSubLobs = this.SubLOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allSubLobs) : _this.allSubLobs.slice(); }));
        this.filteredRoles = this.Role.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allRoles) : _this.allRoles.slice(); }));
    };
    Object.defineProperty(UserComponent.prototype, "Id", {
        get: function () { return this.UserForm.get('Id'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "LOBId", {
        get: function () { return this.UserForm.get('LOBId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "SubLOBId", {
        get: function () { return this.UserForm.get('SubLOBId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "RoleId", {
        get: function () { return this.UserForm.get('RoleId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "SubLOB", {
        get: function () { return this.UserForm.get('SubLOB'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "LOB", {
        get: function () { return this.UserForm.get('LOB'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "EntityId", {
        get: function () { return this.UserForm.get('EntityId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "Entity", {
        get: function () { return this.UserForm.get('Entity'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "Role", {
        get: function () { return this.UserForm.get('Role'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "IsActive", {
        get: function () { return this.UserForm.get('IsActive'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "SearchName", {
        get: function () { return this.UserForm.get('SearchName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "LoginId", {
        get: function () { return this.UserForm.get('LoginId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "EmpCode", {
        get: function () { return this.UserForm.get('EmpCode'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "EmpName", {
        get: function () { return this.UserForm.get('EmpName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "EmailId", {
        get: function () { return this.UserForm.get('EmailId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "Users", {
        get: function () { return this.UserForm.get('Users'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserComponent.prototype, "ADUser", {
        get: function () { return this.UserForm.get('ADUser'); },
        enumerable: true,
        configurable: true
    });
    UserComponent.prototype.getAllUsers = function () {
        var _this = this;
        this.rest.getAll(this.global.getapiendpoint() + 'user/GetAllUser').subscribe(function (data) {
            var tableData = [];
            data.Data.forEach(function (element) {
                var lobcode = '';
                element.UserLobs.forEach(function (lobelement) { lobcode += ", " + lobelement.LOB.Code; });
                lobcode = lobcode.substr(2, lobcode.length);
                var sublobcode = '';
                element.UserSublobs.forEach(function (sublobelement) { sublobcode += ", " + sublobelement.Sublob.Code; });
                sublobcode = sublobcode.substr(2, sublobcode.length);
                var rolecode = '';
                element.UserRoles.forEach(function (roleelement) { rolecode += ", " + roleelement.Role.Code; });
                rolecode = rolecode.substr(2, rolecode.length);
                var entity = element.Entity ? element.Entity.Code : '';
                tableData.push({
                    Id: element.Id, LoginId: element.LoginId, EmpCode: element.EmpCode,
                    EmpName: element.EmpName, EmailId: element.EmailId, Entity: entity, LOBCode: lobcode, SubLOBCode: sublobcode,
                    RoleCode: rolecode,
                    IsActive: element.IsActive ? "Active" : "Inactive"
                });
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](tableData);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
        this.UserCreate = false;
        this.UserIndex = true;
    };
    UserComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    UserComponent.prototype._filter = function (value, obj) {
        var filterValue = (value ? (value.Code ? value.Code.toLowerCase() : value.toLowerCase()) : "");
        return obj.filter(function (o) { return o.Code.toLowerCase().includes(filterValue); });
    };
    UserComponent.prototype.displayWith = function (obj) {
        return obj ? obj.Code : undefined;
    };
    UserComponent.prototype.inputEntity = function (entity) {
        this.EntityId.setValue('');
    };
    UserComponent.prototype.selectedEntity = function (entity) {
        this.EntityId.setValue(entity.Id);
    };
    UserComponent.prototype.getAllEntity = function () {
        var _this = this;
        this.entities = [];
        this.rest.getAll(this.global.getapiendpoint() + "entity/GetAllActiveEntity").subscribe(function (data) {
            _this.entities = data.Data;
            _this.filteredentities = _this.Entity.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.entities) : _this.entities.slice(); }));
        });
    };
    UserComponent.prototype.clearEntity = function () {
        this.EntityId.setValue(null);
        this.Entity.setValue(null);
    };
    UserComponent.prototype.getAllLOB = function () {
        var _this = this;
        this.allLobsInit = [];
        this.rest.getAll(this.global.getapiendpoint() + "lob/GetAllActiveLOB").subscribe(function (data) {
            _this.allLobsInit = data.Data;
        });
    };
    UserComponent.prototype.addLOB = function (event) {
        if (!this.autoLob.isOpen) {
            var input = event.input;
            if (input) {
                input.value = '';
            }
            this.LOB.setValue(null);
        }
        if (this.lobs.length == 0) {
            this.lobList.errorState = true;
        }
        else {
            this.lobList.errorState = false;
        }
    };
    UserComponent.prototype.removeLOB = function (lob) {
        var _this = this;
        var index = this.lobs.indexOf(lob);
        if (index >= 0) {
            this.lobs.splice(index, 1);
        }
        if (this.lobs.length == 0) {
            this.LOBId.setValue("");
        }
        this.allLobs.push(lob);
        this.filteredLobs = this.LOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allLobs) : _this.allLobs.slice(); }));
        if (this.lobs.length == 0) {
            this.lobList.errorState = true;
        }
        else {
            this.lobList.errorState = false;
            var lobId = [];
            this.lobs.forEach(function (element) { lobId.push(element.Id); });
            this.getAllSubLOB(lobId);
        }
    };
    UserComponent.prototype.selectedLOB = function (event) {
        var _this = this;
        this.lobs.push(event.option.value);
        this.lobInput.nativeElement.value = '';
        this.LOB.setValue(null);
        this.LOBId.setValue(this.lobs);
        var index = this.allLobs.indexOf(event.option.value);
        if (index >= 0) {
            this.allLobs.splice(index, 1);
        }
        this.filteredLobs = this.LOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allLobs) : _this.allLobs.slice(); }));
        if (this.lobs.length == 0) {
            this.lobList.errorState = true;
        }
        else {
            this.lobList.errorState = false;
            var lobId = [];
            this.lobs.forEach(function (element) { lobId.push(element.Id); });
            this.getAllSubLOB(lobId);
        }
    };
    UserComponent.prototype.getAllSubLOB = function (LOBId) {
        var _this = this;
        this.allSubLobsInit = [];
        this.rest.getAll(this.global.getapiendpoint() + "sublob/GetSubLOBByLOBId/".concat(LOBId)).subscribe(function (data) {
            _this.allSubLobsInit = data.Data;
            _this.sublobs = [];
            _this.allSubLobs = [];
            _this.allSubLobsInit.forEach(function (element) { _this.allSubLobs.push(element); });
            _this.SubLOB.setValue("");
            _this.SubLOBId.setValue("");
            _this.filteredSubLobs = _this.SubLOB.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allSubLobs) : _this.allSubLobs.slice(); }));
        });
    };
    UserComponent.prototype.getAllSubLOBEdit = function (LOBId, UserSubLOBs) {
        var _this = this;
        this.allSubLobsInit = [];
        this.rest.getAll(this.global.getapiendpoint() + "sublob/GetSubLOBByLOBId/".concat(LOBId)).subscribe(function (data) {
            _this.allSubLobsInit = data.Data;
            var sublobId = [];
            _this.allSubLobs = [];
            _this.allSubLobsInit.forEach(function (element) { _this.allSubLobs.push(element); });
            UserSubLOBs.forEach(function (element) {
                sublobId.push({ Id: element.SublobId, Code: element.Sublob.Code });
                var index = _this.allSubLobs.findIndex(function (o) { return o.Code == element.Sublob.Code; });
                if (index >= 0) {
                    _this.allSubLobs.splice(index, 1);
                }
            });
            _this.sublobs = sublobId;
            _this.SubLOBId.setValue(sublobId);
            _this.filteredSubLobs = _this.SubLOB.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allSubLobs) : _this.allSubLobs.slice(); }));
        });
    };
    UserComponent.prototype.addSubLOB = function (event) {
        if (!this.autoSublob.isOpen) {
            var input = event.input;
            if (input) {
                input.value = '';
            }
            this.SubLOB.setValue(null);
        }
        if (this.sublobs.length == 0) {
            this.sublobList.errorState = true;
        }
        else {
            this.sublobList.errorState = false;
        }
    };
    UserComponent.prototype.removeSUBLOB = function (sublob) {
        var _this = this;
        var index = this.sublobs.indexOf(sublob);
        if (index >= 0) {
            this.sublobs.splice(index, 1);
        }
        if (this.sublobs.length == 0) {
            this.SubLOBId.setValue("");
        }
        this.allSubLobs.push(sublob);
        this.filteredSubLobs = this.SubLOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allSubLobs) : _this.allSubLobs.slice(); }));
        if (this.sublobs.length == 0) {
            this.sublobList.errorState = true;
        }
        else {
            this.sublobList.errorState = false;
        }
    };
    UserComponent.prototype.selectedSubLOB = function (event) {
        var _this = this;
        this.sublobs.push(event.option.value);
        this.sublobInput.nativeElement.value = '';
        this.SubLOB.setValue(null);
        this.SubLOBId.setValue(this.sublobs);
        var index = this.allSubLobs.indexOf(event.option.value);
        if (index >= 0) {
            this.allSubLobs.splice(index, 1);
        }
        this.filteredSubLobs = this.SubLOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allSubLobs) : _this.allSubLobs.slice(); }));
        if (this.sublobs.length == 0) {
            this.sublobList.errorState = true;
        }
        else {
            this.sublobList.errorState = false;
        }
    };
    // Role dropdown 
    UserComponent.prototype.getAllRole = function () {
        var _this = this;
        this.allRolesInit = [];
        this.rest.getAll(this.global.getapiendpoint() + "role/GetAllActiveRole").subscribe(function (data) {
            _this.allRolesInit = data.Data;
        });
    };
    UserComponent.prototype.addRole = function (event) {
        if (!this.autoRole.isOpen) {
            var input = event.input;
            if (input) {
                input.value = '';
            }
            this.Role.setValue(null);
        }
        if (this.roles.length == 0) {
            this.roleList.errorState = true;
        }
        else {
            this.roleList.errorState = false;
        }
    };
    UserComponent.prototype.removeRole = function (role) {
        var _this = this;
        var index = this.roles.indexOf(role);
        if (index >= 0) {
            this.roles.splice(index, 1);
        }
        if (this.roles.length == 0) {
            this.RoleId.setValue("");
        }
        this.allRoles.push(role);
        this.filteredRoles = this.Role.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allRoles) : _this.allRoles.slice(); }));
        if (this.roles.length == 0) {
            this.roleList.errorState = true;
        }
        else {
            this.roleList.errorState = false;
        }
    };
    UserComponent.prototype.selectedRole = function (event) {
        var _this = this;
        this.roles.push(event.option.value);
        this.roleInput.nativeElement.value = '';
        this.Role.setValue(null);
        this.RoleId.setValue(this.roles);
        var index = this.allRoles.indexOf(event.option.value);
        if (index >= 0) {
            this.allRoles.splice(index, 1);
        }
        this.filteredRoles = this.Role.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allRoles) : _this.allRoles.slice(); }));
        if (this.roles.length == 0) {
            this.roleList.errorState = true;
        }
        else {
            this.roleList.errorState = false;
        }
    };
    UserComponent.prototype.getADUsers = function (Text) {
        var _this = this;
        this.rest.getAll(this.global.getapiendpoint() + "ad/FindUsers/".concat(Text)).subscribe(function (data) {
            _this.users = data.Data;
        });
    };
    UserComponent.prototype.onADUserChange = function (value) {
        if (!this.ADUser.value) {
            this.EmpCode.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("[a-zA-Z0-9 ]*"), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]);
            this.EmpCode.updateValueAndValidity();
            this.Readonly = false;
            this.SearchName.setValue('');
            this.EmpCode.setValue('');
        }
        else {
            this.EmpCode.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("[a-zA-Z0-9 ]*"), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]);
            this.EmpCode.updateValueAndValidity();
            this.EmpCode.markAsUntouched();
        }
    };
    UserComponent.prototype.getEmpCodeErrorMessage = function () {
        return this.EmpCode.hasError('required') ? 'Employee code is required' :
            this.EmpCode.hasError('pattern') ? 'Please enter a valid Employee code' :
                this.EmpCode.hasError('maxlength') ? 'This field should have less than 100 characters' :
                    '';
    };
    UserComponent.prototype.inputUser = function (user) {
        this.LoginId.setValue("");
    };
    UserComponent.prototype.selectedUser = function (user) {
        this.SearchName.setValue(user.cn);
        this.LoginId.setValue(user.sAMAccountName);
        this.EmpCode.setValue(user.description);
        this.EmpName.setValue(user.cn);
        this.EmailId.setValue(user.mail);
        var entity = this.entities.find(function (en) { return (en.Code ? en.Code.toLowerCase() : '') == (user.company ? user.company.toLowerCase() : ''); });
        if (entity) {
            this.EntityId.setValue(entity.Id);
            this.Entity.setValue(entity);
        }
        else {
            this.Entity.setValue('');
            this.EntityId.setValue(null);
        }
    };
    UserComponent.prototype.addUser = function () {
        var _this = this;
        this.form.resetForm();
        this.UserForm.markAsUntouched();
        this.Id.setValue('');
        this.LoginId.enable();
        this.SearchName.enable();
        this.IsActive.setValue(true);
        this.ADUser.setValue(true);
        this.onADUserChange(this.ADUser);
        //LOB dropdown
        this.LOBId.setValue("");
        this.LOBId.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]);
        this.LOBId.updateValueAndValidity();
        this.lobs = [];
        this.allLobs = [];
        this.allLobsInit.forEach(function (element) { _this.allLobs.push(element); });
        this.LOB.setValue("");
        this.filteredLobs = this.LOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allLobs) : _this.allLobs.slice(); }));
        this.lobList.errorState = true;
        //SubLOB dropdown
        this.SubLOBId.setValue("");
        this.SubLOBId.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]);
        this.SubLOBId.updateValueAndValidity();
        this.sublobs = [];
        this.allSubLobs = [];
        this.allSubLobsInit.forEach(function (element) { _this.allSubLobs.push(element); });
        this.SubLOB.setValue("");
        this.filteredSubLobs = this.SubLOB.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allSubLobs) : _this.allSubLobs.slice(); }));
        this.sublobList.errorState = true;
        //Role dropdown
        this.RoleId.setValue("");
        this.RoleId.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]);
        this.RoleId.updateValueAndValidity();
        this.roles = [];
        this.allRoles = [];
        this.allRolesInit.forEach(function (element) { _this.allRoles.push(element); });
        this.Role.setValue("");
        this.filteredRoles = this.Role.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allRoles) : _this.allRoles.slice(); }));
        this.roleList.errorState = true;
        this.UserCreate = true;
        this.UserIndex = false;
    };
    UserComponent.prototype.backUser = function () {
        this.UserCreate = false;
        this.UserIndex = true;
        this.topdiv.nativeElement.scrollIntoView({ behavior: 'smooth' });
    };
    UserComponent.prototype.updateUser = function (Id) {
        var _this = this;
        this.rest.getById(this.global.getapiendpoint() + 'user/GetUserById/', Id).subscribe(function (data) {
            _this.Id.setValue(data.Data.Id);
            _this.ADUser.setValue(data.Data.ADUser);
            _this.EmailId.setValue(data.Data.EmailId);
            _this.LoginId.setValue(data.Data.LoginId);
            _this.LoginId.disable();
            _this.SearchName.disable();
            _this.EmpCode.setValue(data.Data.EmpCode);
            _this.EmpName.setValue(data.Data.EmpName);
            _this.EntityId.setValue(data.Data.EntityId ? data.Data.EntityId : null);
            _this.Entity.setValue(data.Data.Entity ? data.Data.Entity : null);
            _this.IsActive.setValue(data.Data.IsActive);
            //LOB dropdown
            var lobId = [];
            _this.allLobs = [];
            _this.allLobsInit.forEach(function (element) { _this.allLobs.push(element); });
            data.Data.UserLobs.forEach(function (element) {
                lobId.push({ Id: element.LobId, Code: element.LOB.Code });
                var index = _this.allLobs.findIndex(function (o) { return o.Code == element.LOB.Code; });
                if (index >= 0) {
                    _this.allLobs.splice(index, 1);
                }
            });
            _this.lobs = lobId;
            _this.LOBId.setValue(lobId);
            _this.LOBId.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]);
            _this.LOBId.updateValueAndValidity();
            _this.filteredLobs = _this.LOB.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allLobs) : _this.allLobs.slice(); }));
            _this.lobList.errorState = false;
            //SUB LOB
            var lobId = [];
            _this.lobs.forEach(function (element) { lobId.push(element.Id); });
            _this.SubLOBId.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]);
            _this.SubLOBId.updateValueAndValidity();
            _this.getAllSubLOBEdit(lobId, data.Data.UserSublobs);
            _this.sublobList.errorState = false;
            //Role dropdown
            var roleId = [];
            _this.allRoles = [];
            _this.allRolesInit.forEach(function (element) { _this.allRoles.push(element); });
            data.Data.UserRoles.forEach(function (element) {
                roleId.push({ Id: element.RoleId, Code: element.Role.Code });
                var index = _this.allRoles.findIndex(function (o) { return o.Code == element.Role.Code; });
                if (index >= 0) {
                    _this.allRoles.splice(index, 1);
                }
            });
            _this.roles = roleId;
            _this.RoleId.setValue(roleId);
            _this.RoleId.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]);
            _this.RoleId.updateValueAndValidity();
            _this.filteredRoles = _this.Role.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.allRoles) : _this.allRoles.slice(); }));
            _this.roleList.errorState = false;
            _this.UserCreate = true;
            _this.UserIndex = false;
        });
    };
    UserComponent.prototype.exportUser = function () {
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'User');
        this.toastr.showNotification('top', 'right', 'Exported Successfully', 'success');
    };
    UserComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    UserComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    UserComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    UserComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].A_KEY) {
            this.addUser();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].B_KEY) {
            this.backUser();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].U_KEY) {
            // this.uploadUser();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].S_KEY) {
            this.saveUser();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].X_KEY) {
            this.exportUser();
        }
    };
    UserComponent.prototype.saveUser = function () {
        var _this = this;
        this.rest.checkDuplicate(this.global.getapiendpoint() + 'user/CheckDuplicateUser/', this.LoginId.value.toString().trim(), (this.Id.value !== '' ? this.Id.value : '0')).subscribe(function (data) {
            if (data.Data) {
                _this.toastr.showNotification('top', 'right', data.Message, 'danger');
            }
            else {
                var lobId = [];
                if (_this.LOBId.value) {
                    _this.LOBId.value.forEach(function (element) { lobId.push(element.Id); });
                }
                var sublobId = [];
                if (_this.SubLOBId.value) {
                    _this.SubLOBId.value.forEach(function (element) { sublobId.push(element.Id); });
                }
                var roleId = [];
                if (_this.RoleId.value) {
                    _this.RoleId.value.forEach(function (element) { roleId.push(element.Id); });
                }
                var model = {
                    Id: _this.Id.value,
                    ADUser: _this.ADUser.value,
                    EmpCode: _this.EmpCode.value,
                    EmpName: _this.EmpName.value,
                    EmailId: _this.EmailId.value,
                    LoginId: _this.LoginId.value,
                    EntityId: _this.EntityId.value,
                    DefaultRoleId: roleId ? roleId[0] : "",
                    IsActive: _this.IsActive.value,
                    LobId: lobId,
                    SublobId: sublobId,
                    RoleId: roleId,
                    UserId: _this.userLoggedIn.Id,
                    UserRoleId: _this.userLoggedIn.DefaultRoleId
                };
                var apiUrl = '';
                if (_this.Id.value == '') {
                    apiUrl = 'user/CreateUser';
                }
                else {
                    apiUrl = 'user/UpdateUser';
                }
                _this.rest.create(_this.global.getapiendpoint() + apiUrl, model).subscribe(function (data) {
                    if (data.Success) {
                        _this.toastr.showNotification('top', 'right', data.Message, 'success');
                    }
                    else {
                        _this.toastr.showNotification('top', 'right', data.Message, 'danger');
                    }
                    _this.getAllUsers();
                });
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UserComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('topdiv', { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UserComponent.prototype, "topdiv", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('lobInput', { static: true }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UserComponent.prototype, "lobInput", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('autoLob', { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocomplete"])
    ], UserComponent.prototype, "autoLob", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('lobList', { static: true }),
        __metadata("design:type", Object)
    ], UserComponent.prototype, "lobList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sublobInput', { static: true }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UserComponent.prototype, "sublobInput", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('autoSublob', { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocomplete"])
    ], UserComponent.prototype, "autoSublob", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sublobList', { static: true }),
        __metadata("design:type", Object)
    ], UserComponent.prototype, "sublobList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('roleInput', { static: true }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UserComponent.prototype, "roleInput", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('autoRole', { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocomplete"])
    ], UserComponent.prototype, "autoRole", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('roleList', { static: true }),
        __metadata("design:type", Object)
    ], UserComponent.prototype, "roleList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], UserComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], UserComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], UserComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"])
    ], UserComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], UserComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], UserComponent.prototype, "keyEvent", null);
    UserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! raw-loader!./user.component.html */ "./node_modules/raw-loader/index.js!./src/app/user-management/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.scss */ "./src/app/user-management/user/user.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExcelService"]])
    ], UserComponent);
    return UserComponent;
}());



/***/ })

}]);
//# sourceMappingURL=app-user-management-user-module.js.map