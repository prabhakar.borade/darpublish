(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-asset-asset-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/asset/approveregister/approveregister.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/asset/approveregister/approveregister.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #topdiv> </div>\r\n<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\" [hidden]=\"!ApproveRegisterList\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>Approve Entry</h4>\r\n                        </td>                        \r\n                        <td style=\"width: 50%; text-align: right;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                        <div class=\"table-responsive\">\r\n                            <table style=\"margin-top: -20px;\">\r\n                                <tr>\r\n                                    <td style=\"width: 10%;\">\r\n                                        <i *ngIf=\"IsExport\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; cursor: pointer !important; vertical-align: middle; \r\n                                            margin-left: 15px; color: #589092;\"\r\n                                            title=\"Export (alt+x)\" (click)=\"exportApproveRegister()\">play_for_work</i>\r\n                                    </td>\r\n                                    <td>\r\n                                        <mat-form-field>\r\n                                            <input matInput (keyup)=\"applyFilter($event.target.value)\"\r\n                                                placeholder=\"Filter\" autocomplete=\"off\">\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n                                <ng-container matColumnDef=\"Id\">\r\n                                    <th mat-header-cell *matHeaderCellDef (mousedown)=\"onResizeColumn($event, 0)\"\r\n                                        style=\"width:10%; text-align: center;\">\r\n                                        <span> Action </span>\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n\r\n                                        <button *ngIf=\"element.StatusId == 1\" mat-raised-button type=\"button\"\r\n                                            title=\"Verify\" (click)=\"approveRegister(element.Id,'Edit')\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #7ba964;\">done_outline</i>\r\n                                        </button>\r\n                                        <button *ngIf=\"element.StatusId > 1 || element.StatusId == -1\" mat-raised-button\r\n                                            type=\"button\" title=\"View\" (click)=\"approveRegister(element.Id, 'view')\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #589092;\">remove_red_eye</i>\r\n                                        </button>\r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Code\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width:5%; text-align: center;\">\r\n                                        Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Code}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Lob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width:10%; text-align: center;\"> Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Lob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Sublob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:10%\"> Sublob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Sublob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryType\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width:10%\"> InventoryType\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryType}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:10%\"> InventoryName\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Validity\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 6)\" style=\"width:10%\"> Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Validity}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"RaisedBy\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 7)\" style=\"width:10%\"> RaisedBy\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RaisedBy}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"RaisedDate\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 8)\" style=\"width:10%\"> RaisedDate\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RaisedDate}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Status\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 8)\" style=\"width:30%; text-align: center;\">\r\n                                        Status\r\n                                    </th>\r\n                                    <td style=\"color: #e65b32;\r\n                                    font-weight: 600;\" mat-cell *matCellDef=\"let element\">{{element.Status}}</td>\r\n                                </ng-container>\r\n\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"main-content\" [hidden]=\"!ApproveRegisterCreate\">\r\n    <div class=\"container-fluid\">\r\n        <div>\r\n            <div>\r\n                <div class=\"card\">\r\n                    <div class=\"card-form-header-title card-header-Grid\">\r\n                        <table style=\"margin-left: 3px;\">\r\n                            <tr>\r\n                                <td style=\"padding-left: 10px;\">\r\n                                    <h4 class=\"card-title\">Approve Entry</h4>\r\n                                </td>\r\n                                <td style=\"width: 50%; text-align: right;\">\r\n                                    <button mat-raised-button type=\"reset\" matTooltip=\"Close\"\r\n                                        class=\"btn-danger pull-right\" (click)=\"backApproveRegister()\">X\r\n                                    </button>\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"ApproveRegisterForm\">\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"Id\">\r\n                            </mat-form-field>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Entity\" type=\"text\" formControlName=\"Entity\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Lob\" type=\"text\" formControlName=\"Lob\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Sublob\" type=\"text\" formControlName=\"Sublob\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"InventoryType\" type=\"text\"\r\n                                            formControlName=\"InventoryType\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"InventoryName\" type=\"text\"\r\n                                            formControlName=\"InventoryName\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"IPAddress\" type=\"text\" formControlName=\"IPAddress\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Vendor\" type=\"text\" formControlName=\"Vendor\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"ValidityFrom\" type=\"text\"\r\n                                            formControlName=\"ValidityFrom\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"ValidityTo\" type=\"text\"\r\n                                            formControlName=\"ValidityTo\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Purpose\" type=\"text\"\r\n                                            formControlName=\"Purpose\"> </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"divBox\">\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <label class=\"divLabel\" placeholder=\"User / Consumer\" type=\"text\">User /\r\n                                            Consumer </label>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Employee Name\" type=\"text\"\r\n                                                formControlName=\"EmpName\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Email Id\" type=\"text\"\r\n                                                formControlName=\"EmailId\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Contact No\" type=\"text\"\r\n                                                formControlName=\"ContactNo\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"divBox\">\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <label class=\"divLabel\" placeholder=\"Tech Owner\" type=\"text\"> Tech Owner</label>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Employee Name\" type=\"text\"\r\n                                                formControlName=\"TechName\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Email Id\" type=\"text\"\r\n                                                formControlName=\"TechEmailId\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Contact No.\" type=\"text\"\r\n                                                formControlName=\"TechContactNo\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Dependency\" type=\"text\"\r\n                                            formControlName=\"Dependency\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Impact of failure\" type=\"text\"\r\n                                            formControlName=\"Impact\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Reason for changing validity\" type=\"text\"\r\n                                            formControlName=\"Reason\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Remarks\" type=\"text\"\r\n                                            formControlName=\"Remarks\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\" [hidden]=\"IsView\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Reason for Reject\" type=\"text\"\r\n                                            formControlName=\"RejectReason\" required></textarea>\r\n                                        <mat-error *ngIf=\"RejectReason.hasError('required')\">\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <button mat-raised-button type=\"reset\" matTooltip=\"Back (alt b)\"\r\n                                class=\"btn btn-warning pull-right\" (click)=\"backApproveRegister()\">Back</button>\r\n\r\n                            <button [hidden]=\"IsView\" mat-raised-button type=\"button\" matTooltip=\"Approve (alt a)\"\r\n                                class=\"btn btn-success pull-right\" (click)=\"saveApproveRegister(2)\">Approve</button>\r\n\r\n                            <button [hidden]=\"IsView\" mat-raised-button type=\"submit\" matTooltip=\"Reject (alt r)\"\r\n                                class=\"btn btn-danger pull-right\" (click)=\"saveApproveRegister(-1)\">Reject</button>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/asset/asset.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/asset/asset.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/asset/assetregister/assetregister.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/asset/assetregister/assetregister.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #topdiv> </div>\r\n<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\" [hidden]=\"!AssetRegisterList\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\"margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>Asset Entry</h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                        <div class=\"table-responsive\">\r\n                            <table style=\"margin-top: -20px;\">\r\n                                <tr>\r\n                                    <td style=\"width: 10%;\">\r\n                                        <i *ngIf=\"IsMaker\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; margin-left: 10px; vertical-align: middle; cursor: pointer !important; color: #589092;\"\r\n                                            title=\"Create (alt+c)\" (click)=\"createAssetRegister()\">add</i>\r\n                                        <i *ngIf=\"IsExport\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; cursor: pointer !important; vertical-align: middle; margin-left: 15px; color: #589092;\"\r\n                                            title=\"Export (alt+x)\" (click)=\"exportAssetRegister()\">play_for_work</i>\r\n                                    </td>\r\n                                    <td>\r\n                                        <mat-form-field>\r\n                                            <input matInput (keyup)=\"applyFilter($event.target.value)\"\r\n                                                placeholder=\"Filter\" autocomplete=\"off\">\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n                                <ng-container matColumnDef=\"Id\">\r\n                                    <th mat-header-cell *matHeaderCellDef (mousedown)=\"onResizeColumn($event, 0)\"\r\n                                        style=\"width:5%;text-align: center;\">\r\n                                        <span *ngIf=\"IsMaker\"> Action </span>\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                        <button *ngIf=\"IsMaker && element.StatusId == 0\" mat-raised-button type=\"button\"\r\n                                            title=\"Edit\" (click)=\"updateAssetRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #589092;\">edit</i>\r\n                                        </button>\r\n                                        <button *ngIf=\"element.StatusId > 0 || element.StatusId == -1\" mat-raised-button\r\n                                            type=\"button\" title=\"View\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #589092;\">remove_red_eye</i>\r\n                                        </button>\r\n\r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Code\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width:5%\"> Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Code}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Lob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width:10%;\"> Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Lob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Sublob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:10%\"> Sublob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Sublob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryType\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width:10%\"> InventoryType\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryType}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:10%\"> InventoryName\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Validity\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 6)\" style=\"width:10%\"> Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Validity}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"RaisedBy\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 7)\" style=\"width:10%\"> RaisedBy\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RaisedBy}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"RaisedDate\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 8)\" style=\"width:10%\"> RaisedDate\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RaisedDate}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Status\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 8)\" style=\"width:30%\"> Status\r\n                                    </th>\r\n                                    <td style=\"color: #e65b32;\r\n                                font-weight: 600;\" mat-cell *matCellDef=\"let element\">{{element.Status}}</td>\r\n                                </ng-container>\r\n\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"main-content\" [hidden]=\"!AssetRegisterCreate\">\r\n    <div class=\"container-fluid\">\r\n        <div>\r\n            <div>\r\n                <div class=\"card\">\r\n                    <div class=\"card-form-header-title card-header-Grid\" style=\"width:100%;\">\r\n                        <table style=\"margin-left: 3px;\">\r\n                            <tr>\r\n                                <td style=\"width: 50%; padding-left: 10px;\">\r\n                                    <h4 class=\"card-title\">Add Entry</h4>\r\n                                </td>\r\n                                <td style=\"width: 50%; text-align: right;\">\r\n                                    <button mat-raised-button type=\"reset\" matTooltip=\"Close\"\r\n                                        class=\"btn-danger pull-right\" (click)=\"backAssetRegister()\">X\r\n                                    </button>\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                    <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"AssetRegisterForm\">\r\n                        <div class=\"card-body\">\r\n\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"Id\">\r\n                                <input matInput type=\"text\" formControlName=\"EntityId\">\r\n                                <input matInput type=\"text\" formControlName=\"LobId\">\r\n                                <input matInput type=\"text\" formControlName=\"SublobId\">\r\n                                <input matInput type=\"text\" formControlName=\"VendorId\">\r\n                                <input matInput type=\"text\" formControlName=\"InventoryId\">\r\n                                <input matInput type=\"text\" formControlName=\"StatusId\">\r\n\r\n                            </mat-form-field>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Entity\" type=\"text\" formControlName=\"Entity\"\r\n                                            [matAutocomplete]=\"autoEntity\" (input)=\"inputEntity($event.target.value)\"\r\n                                            required>\r\n                                        <mat-autocomplete #autoEntity=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedEntity($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let entity of filteredEntity | async\" [value]=\"entity\">\r\n                                                {{ entity.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Entity.value && !AssetRegisterForm.value.Id\" matSuffix\r\n                                            mat-icon-button aria-label=\"Clear\" (click)=\"clearEntity()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Entity.hasError('required')\">\r\n                                            Entity is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Entity.hasError('incorrect')\">\r\n                                            Please select a valid Entity\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Lob\" type=\"text\" formControlName=\"Lob\"\r\n                                            [matAutocomplete]=\"autoLOB\" (input)=\"inputLOB($event.target.value)\"\r\n                                            required>\r\n                                        <mat-autocomplete #autoLOB=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedLOB($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let lob of filteredlobs | async\" [value]=\"lob\">\r\n                                                {{ lob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Lob.value && !AssetRegisterForm.value.Id\" matSuffix\r\n                                            mat-icon-button aria-label=\"Clear\" (click)=\"clearLOB()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Lob.hasError('required')\">\r\n                                            Lob is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Lob.hasError('incorrect')\">\r\n                                            Please select a valid Lob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Sublob\" type=\"text\" formControlName=\"Sublob\"\r\n                                            [matAutocomplete]=\"autoSublob\" (input)=\"inputSublob($event.target.value)\"\r\n                                            required>\r\n                                        <mat-autocomplete #autoSublob=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedSublob($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let sublob of filteredSublobs | async\" [value]=\"sublob\">\r\n                                                {{ sublob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Sublob.value && !AssetRegisterForm.value.Id\" matSuffix\r\n                                            mat-icon-button aria-label=\"Clear\" (click)=\"clearSublob()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Sublob.hasError('required')\">\r\n                                            Sublob is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Sublob.hasError('incorrect')\">\r\n                                            Please select a valid Sublob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"InventoryType\" type=\"text\"\r\n                                            formControlName=\"InventoryType\" [matAutocomplete]=\"autoInventoryType\"\r\n                                            (input)=\"inputInventoryType($event.target.value)\" required>\r\n                                        <mat-autocomplete #autoInventoryType=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedInventoryType($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let inventorytype of filteredInventorytype | async\"\r\n                                                [value]=\"inventorytype\">\r\n                                                {{ inventorytype.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"InventoryType.value && !AssetRegisterForm.value.Id\"\r\n                                            matSuffix mat-icon-button aria-label=\"Clear\" (click)=\"clearInventoryType()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"InventoryType.hasError('required')\">\r\n                                            InventoryType is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"InventoryType.hasError('incorrect')\">\r\n                                            Please select a valid InventoryType\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"InventoryName\" type=\"text\"\r\n                                            formControlName=\"InventoryName\" required>\r\n                                        <mat-error *ngIf=\"InventoryName.hasError('required')\">\r\n                                            Inventory Name is <strong>required</strong>\r\n                                        </mat-error>\r\n                                        <mat-error\r\n                                            *ngIf=\"InventoryName.hasError('pattern') && !InventoryName.hasError('required')\">\r\n                                            Please enter a valid Inventory Name\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"InventoryName.hasError('maxLength')\">\r\n                                            Inventory Name should have less than 100 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"IPAddress\" type=\"text\" formControlName=\"IPAddress\">\r\n                                        <mat-error *ngIf=\"IPAddress.hasError('pattern')\">\r\n                                            Please enter a valid IP Address\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"IPAddress.hasError('maxLength')\">\r\n                                            IP Address should have less than 100 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Vendor\" type=\"text\" formControlName=\"Vendor\"\r\n                                            [matAutocomplete]=\"autoVendor\" (input)=\"inputVendor($event.target.value)\">\r\n                                        <mat-autocomplete #autoVendor=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedVendor($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let vendor of filteredVendor | async\" [value]=\"vendor\">\r\n                                                {{ vendor.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n\r\n                                        <button mat-button *ngIf=\"Vendor.value && !AssetRegisterForm.value.Id\" matSuffix\r\n                                            mat-icon-button aria-label=\"Clear\" (click)=\"clearVendor()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Vendor.hasError('incorrect')\">\r\n                                            Please select a valid Vendor\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput [matDatepicker]=\"pickerFrom\" [readonly]=\"readonly\"\r\n                                            placeholder=\"Validity From Date\" [formControl]=\"ValidityFrom\"\r\n                                            autocomplete=\"off\">\r\n                                        <button mat-button *ngIf=\"ValidityFrom.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearFromDate()\">\r\n                                            <mat-icon>close</mat-icon>\r\n                                        </button>\r\n                                        <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\">\r\n                                        </mat-datepicker-toggle>\r\n                                        <mat-datepicker #pickerFrom></mat-datepicker>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput [matDatepicker]=\"pickerTo\" [readonly]=\"readonly\"\r\n                                            placeholder=\"Validity To Date\" [formControl]=\"ValidityTo\"\r\n                                            autocomplete=\"off\">\r\n                                        <button mat-button *ngIf=\"ValidityTo.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearToDate()\">\r\n                                            <mat-icon>close</mat-icon>\r\n                                        </button>\r\n                                        <mat-datepicker-toggle matSuffix [for]=\"pickerTo\">\r\n                                        </mat-datepicker-toggle>\r\n                                        <mat-datepicker #pickerTo></mat-datepicker>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Purpose\" type=\"text\"\r\n                                            formControlName=\"Purpose\"><textarea></textarea>\r\n                                        <mat-error *ngIf=\"Purpose.hasError('pattern')\">\r\n                                            Please enter a valid Purpose\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Purpose.hasError('maxLength')\">\r\n                                            Purpose should have less than 2000 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"divBox\">\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <label class=\"divLabel\" placeholder=\"User / Consumer\" type=\"text\">User /\r\n                                            Consumer </label>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-2\" [hidden]=\"IsView\">\r\n                                        <mat-checkbox class=\"example-full-width\" (change)=\"onADUserChange($event)\"\r\n                                            formControlName=\"IsADUser\"> AD User\r\n                                        </mat-checkbox>\r\n                                    </div>\r\n                                    <div class=\"col-md-8\">\r\n                                        <mat-form-field class=\"example-full-width\"\r\n                                            [hidden]=\"!AssetRegisterForm.value.IsADUser\">\r\n                                            <input matInput placeholder=\"Search Name\" type=\"text\"\r\n                                                formControlName=\"SearchName\" [matAutocomplete]=\"searchname\"\r\n                                                (input)=\"inputUser($event.target.value)\">\r\n                                            <mat-autocomplete #searchname=\"matAutocomplete\"\r\n                                                (optionSelected)=\"selectedUser($event.target.value)\">\r\n                                                <mat-option *ngFor=\"let user of users\" [value]=\"user\">\r\n                                                    {{ user.displayName }}\r\n                                                </mat-option>\r\n                                            </mat-autocomplete>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Employee Name\" type=\"text\"\r\n                                                formControlName=\"EmpName\">\r\n                                            <mat-error *ngIf=\"EmpName.hasError('pattern')\">\r\n                                                Please enter a valid Employee Name\r\n                                            </mat-error>\r\n                                            <mat-error *ngIf=\"EmpName.hasError('maxLength')\">\r\n                                                Employee Name should have less than 100 characters\r\n                                            </mat-error>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Email Id\" type=\"text\"\r\n                                                formControlName=\"EmailId\">\r\n                                            <mat-error *ngIf=\"EmailId.hasError('pattern')\">\r\n                                                Please enter a valid Email Id\r\n                                            </mat-error>\r\n                                            <mat-error *ngIf=\"EmailId.hasError('maxLength')\">\r\n                                                Email Id should have less than 100 characters\r\n                                            </mat-error>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Contact No.\" type=\"text\"\r\n                                                formControlName=\"ContactNo\">\r\n                                            <mat-error *ngIf=\"ContactNo.hasError('pattern')\">\r\n                                                Please enter a valid Contact No\r\n                                            </mat-error>\r\n                                            <mat-error *ngIf=\"ContactNo.hasError('maxLength')\">\r\n                                                Contact No should have less than 100 characters\r\n                                            </mat-error>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n\r\n                            <div class=\"divBox\">\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <label class=\"divLabel\" placeholder=\"Tech Owner\" type=\"text\"> Tech Owner</label>\r\n                                    </div>\r\n                                    <div class=\"col-md-2\" [hidden]=\"IsView\">\r\n                                        <mat-checkbox class=\"example-full-width\" (change)=\"onADTUserChange($event)\"\r\n                                            formControlName=\"IsTADUser\"> AD User\r\n                                        </mat-checkbox>\r\n                                    </div>\r\n                                    <div class=\"col-md-8\">\r\n                                        <mat-form-field class=\"example-full-width\"\r\n                                            [hidden]=\"!AssetRegisterForm.value.IsTADUser\">\r\n                                            <input matInput placeholder=\"Search Name\" type=\"text\"\r\n                                                formControlName=\"SearchTName\" [matAutocomplete]=\"searchtname\"\r\n                                                (input)=\"inputUser($event.target.value)\">\r\n                                            <mat-autocomplete #searchtname=\"matAutocomplete\"\r\n                                                (optionSelected)=\"selectedTUser($event.option.value)\">\r\n                                                <mat-option *ngFor=\"let user of users\" [value]=\"user\">\r\n                                                    {{ user.displayName }}\r\n                                                </mat-option>\r\n                                            </mat-autocomplete>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Employee Name\" type=\"text\"\r\n                                                formControlName=\"TechName\">\r\n                                            <mat-error *ngIf=\"TechName.hasError('pattern')\">\r\n                                                Please enter a valid Tech Name\r\n                                            </mat-error>\r\n                                            <mat-error *ngIf=\"TechName.hasError('maxLength')\">\r\n                                                Tech Name should have less than 100 characters\r\n                                            </mat-error>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Email Id\" type=\"text\"\r\n                                                formControlName=\"TechEmailId\">\r\n                                            <mat-error *ngIf=\"TechEmailId.hasError('pattern')\">\r\n                                                Please enter a valid Tech Email Id\r\n                                            </mat-error>\r\n                                            <mat-error *ngIf=\"TechEmailId.hasError('maxLength')\">\r\n                                                Tech Email Id should have less than 100 characters\r\n                                            </mat-error>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Contact No.\" type=\"text\"\r\n                                                formControlName=\"TechContactNo\">\r\n                                            <mat-error *ngIf=\"TechContactNo.hasError('pattern')\">\r\n                                                Please enter a valid Contact No\r\n                                            </mat-error>\r\n                                            <mat-error *ngIf=\"TechContactNo.hasError('maxLength')\">\r\n                                                Contact No should have less than 100 characters\r\n                                            </mat-error>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Dependency\" type=\"text\"\r\n                                            formControlName=\"Dependency\"><textarea></textarea>\r\n                                        <mat-error *ngIf=\"Dependency.hasError('pattern')\">\r\n                                            Please enter a valid Dependency\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Dependency.hasError('maxLength')\">\r\n                                            Dependency should have less than 2000 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Impact of failure\" type=\"text\"\r\n                                            formControlName=\"Impact\"><textarea></textarea>\r\n                                        <mat-error *ngIf=\"Impact.hasError('pattern')\">\r\n                                            Please enter a valid Impact of failure\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Impact.hasError('maxLength')\">\r\n                                            Impact of failure should have less than 2000 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Reason for changing validity\" type=\"text\"\r\n                                            formControlName=\"Reason\"><textarea></textarea>\r\n                                        <mat-error *ngIf=\"Reason.hasError('pattern')\">\r\n                                            Please enter a valid Reason\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Reason.hasError('maxLength')\">\r\n                                            Reason should have less than 2000 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Remarks\" type=\"text\"\r\n                                            formControlName=\"Remarks\"><textarea></textarea>\r\n                                        <mat-error *ngIf=\"Remarks.hasError('pattern')\">\r\n                                            Please enter a valid Remarks\r\n                                        </mat-error>\r\n                                        <mat-error *ngIf=\"Remarks.hasError('maxLength')\">\r\n                                            Remarks should have less than 2000 characters\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\" *ngIf=\"StatusId.value == -1\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Reason for Reject\" type=\"text\"\r\n                                            formControlName=\"RejectReason\"></textarea>\r\n                                       </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <button mat-raised-button type=\"reset\" matTooltip=\"Back (alt b)\"\r\n                                class=\"btn btn-warning pull-right\" (click)=\"backAssetRegister()\">Back</button>\r\n\r\n                            <button mat-raised-button [hidden]=\"IsView\" type=\"submit\" id=\"btnDraft\"\r\n                                matTooltip=\"Draft (alt d)\" class=\"btn btn-info pull-right\" *ngIf=\"StatusId.value == 0\"\r\n                                (click)=\"saveAssetRegister(0)\">Draft</button>\r\n\r\n                            <button mat-raised-button [hidden]=\"IsView\" type=\"submit\" id=\"btnSubmit\"\r\n                                matTooltip=\"Submit (alt s)\" class=\"btn btn-success pull-right\"\r\n                                (click)=\"saveAssetRegister(1)\">Submit</button>\r\n\r\n                            <button mat-raised-button [hidden]=\"!IsRenew\" type=\"submit\" id=\"btnActive\"\r\n                                matTooltip=\"No action required\" class=\"btn btn-info pull-right\"\r\n                                (click)=\"deactivateAssetRegister(2)\">Deactive</button>\r\n\r\n                        </div>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/asset/viewregister/viewregister.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/asset/viewregister/viewregister.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #topdiv> </div>\r\n<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\" [hidden]=\"!ViewRegisterList\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4> Edit / View </h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\"> \r\n                    <div class=\"card-body\">\r\n                        <div class=\"table-responsive\">\r\n                            <table style=\"margin-top: -20px;\">\r\n                                <tr>\r\n                                    <td style=\"width: 10%;\">\r\n                                        <i *ngIf=\"IsExport\" class=\"material-icons\"\r\n                                            style=\"font-size:24px; cursor: pointer !important; vertical-align: middle; margin-left: 15px; color: #589092;\"\r\n                                            title=\"Export (alt+x)\" (click)=\"exportViewRegister()\">play_for_work</i>\r\n                                    </td>\r\n                                    <td>\r\n                                        <mat-form-field>\r\n                                            <input matInput (keyup)=\"applyFilter($event.target.value)\"\r\n                                                placeholder=\"Filter\" autocomplete=\"off\">\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n                                <ng-container matColumnDef=\"Id\">\r\n                                    <th mat-header-cell *matHeaderCellDef (mousedown)=\"onResizeColumn($event, 0)\"\r\n                                        style=\"width:10%\">\r\n                                        <span> Action </span>\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                       \r\n                                        <button *ngIf=\"element.StatusId > 0 || element.StatusId == -1\" mat-raised-button type=\"button\"\r\n                                            title=\"View\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #589092;\">remove_red_eye</i>\r\n                                        </button>\r\n                                        <button *ngIf=\"element.IsRenew == 'true'\" mat-raised-button type=\"button\"\r\n                                            title=\"Clone & Inactive\" (click)=\"cloneRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: #589092;\">content_copy</i>\r\n                                        </button>\r\n\r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Code\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width: 5%\"> Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Code}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Lob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width: 10%;\"> Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Lob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Sublob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width: 10%\"> Sublob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Sublob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryType\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width: 10%\"> InventoryType\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryType}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width: 10%\"> InventoryName\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Validity\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 6)\" style=\"width:10%\"> Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Validity}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"RaisedBy\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 7)\" style=\"width:10%\"> RaisedBy\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RaisedBy}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"RaisedDate\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 8)\" style=\"width:15%\"> RaisedDate\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RaisedDate}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Status\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 8)\" style=\"width:30%\"> Status\r\n                                    </th>\r\n                                    <td style=\"color: #e65b32;\r\n                                    font-weight: 600;\" mat-cell *matCellDef=\"let element\">{{element.Status}}</td>\r\n                                </ng-container>\r\n\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"main-content\" [hidden]=\"!ViewRegisterCreate\">\r\n    <div class=\"container-fluid\">\r\n        <div>\r\n            <div>\r\n                <div class=\"card\">\r\n                    <div class=\"card-form-header-title card-header-Grid\">\r\n                        <table style=\"margin-left: 3px;\">\r\n                            <tr>\r\n                                <td style=\"padding-left:10px;\">\r\n                                    <h4 class=\"card-title\">Edit / View </h4>\r\n                                </td>\r\n                                <td style=\"width: 50%; text-align: right;\">\r\n                                    <button mat-raised-button type=\"reset\" matTooltip=\"Close\"\r\n                                        class=\"btn-danger pull-right\" (click)=\"backViewRegister()\">X\r\n                                    </button>\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"ViewRegisterForm\">\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"Id\">\r\n                                <input matInput type=\"text\" formControlName=\"StatusId\">\r\n                            </mat-form-field>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Entity\" type=\"text\" formControlName=\"Entity\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Lob\" type=\"text\" formControlName=\"Lob\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Sublob\" type=\"text\" formControlName=\"Sublob\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"InventoryType\" type=\"text\"\r\n                                            formControlName=\"InventoryType\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"InventoryName\" type=\"text\"\r\n                                            formControlName=\"InventoryName\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"IPAddress\" type=\"text\" formControlName=\"IPAddress\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Vendor\" type=\"text\" formControlName=\"Vendor\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"ValidityFrom\" type=\"text\"\r\n                                            formControlName=\"ValidityFrom\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"ValidityTo\" type=\"text\"\r\n                                            formControlName=\"ValidityTo\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Purpose\" type=\"text\"\r\n                                            formControlName=\"Purpose\"> </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"divBox\">\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <label class=\"divLabel\" placeholder=\"User / Consumer\" type=\"text\">User /\r\n                                            Consumer </label>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Employee Name\" type=\"text\"\r\n                                                formControlName=\"EmpName\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Email Id\" type=\"text\"\r\n                                                formControlName=\"EmailId\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Contact No\" type=\"text\"\r\n                                                formControlName=\"ContactNo\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"divBox\">\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <label class=\"divLabel\" placeholder=\"Tech Owner\" type=\"text\"> Tech Owner</label>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"row\" style=\"padding: 5px;\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Employee Name\" type=\"text\"\r\n                                                formControlName=\"TechName\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Email Id\" type=\"text\"\r\n                                                formControlName=\"TechEmailId\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                    <div class=\"col-md-4\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Contact No.\" type=\"text\"\r\n                                                formControlName=\"TechContactNo\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Dependency\" type=\"text\"\r\n                                            formControlName=\"Dependency\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Impact of failure\" type=\"text\"\r\n                                            formControlName=\"Impact\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Reason for changing validity\" type=\"text\"\r\n                                            formControlName=\"Reason\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Remarks\" type=\"text\"\r\n                                            formControlName=\"Remarks\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\" *ngIf=\"StatusId.value == -1\">\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <textarea matInput placeholder=\"Reason for Reject\" type=\"text\"\r\n                                            formControlName=\"RejectReason\">  </textarea>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <button mat-raised-button type=\"reset\" matTooltip=\"Back (alt b)\"\r\n                                class=\"btn btn-warning pull-right\" (click)=\"backViewRegister()\">Back\r\n                            </button>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/asset/approveregister/approveregister.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/asset/approveregister/approveregister.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #044da1;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n\n.divBox {\n  border-style: dashed;\n  border-width: 1px;\n  border-color: #babfbf;\n  margin-bottom: 10px;\n}\n\n.divLabel {\n  color: #4d6d98;\n  font-weight: 600;\n}\n\n.card-form-header-title {\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  border-radius: 0px;\n  width: 100%;\n}\n\n.mat-form-field-type-mat-native-select.mat-form-field-disabled .mat-form-field-infix::after,\n.mat-input-element:disabled {\n  color: #4d6d98;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXNzZXQvYXBwcm92ZXJlZ2lzdGVyL0U6XFxEQVIgVUFUIEZpbGVzXFxBc3NldC1VSS9zcmNcXGFwcFxcYXNzZXRcXGFwcHJvdmVyZWdpc3RlclxcYXBwcm92ZXJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hc3NldC9hcHByb3ZlcmVnaXN0ZXIvYXBwcm92ZXJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7QUNDSjs7QURFQTtFQUNJLDBCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtFQUNBLDZCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7QUNDSjs7QURFQTs7RUFFSSxtQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUNBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3Q0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURFQTtFQUNJLG9CQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FDQ0o7O0FEQ0E7RUFFSSxjQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURDQTtFQUVJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDSjs7QURFQTs7RUFFSSxjQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9hc3NldC9hcHByb3ZlcmVnaXN0ZXIvYXBwcm92ZXJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LWZvcm0tZmllbGRbaGlkZGVuXSB7XHJcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbnRhYmxle1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGx7XHJcbiAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDsgXHJcbiAgICBjb2xvcjogIzA0NGRhMTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBjdXJzb3I6IGNvbC1yZXNpemU7XHJcbiAgICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufVxyXG5cclxudHIubWF0LWhlYWRlci1yb3cge1xyXG4gICAgaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDsgXHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG50ci5tYXQtZm9vdGVyLXJvdywgdHIubWF0LXJvdyB7XHJcbiAgICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcclxufVxyXG4gIFxyXG4ubWF0LXRhYmxle1xyXG4gICAgYm9yZGVyLXNwYWNpbmc6IDA7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZjdmNGYwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsICBcclxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDEycHg7IFxyXG59XHJcblxyXG4uY2FyZC1oZWFkZXItdGl0bGV7XHJcbiAgICB3aWR0aDogNTUlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNzIsIDEzMCwgMTk3KTtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcclxuICAgIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMzsgXHJcbiAgICBwYWRkaW5nOiA0cHggNHB4IDBweCAxNHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG59XHJcblxyXG4uZGl2Qm94e1xyXG4gICAgYm9yZGVyLXN0eWxlOiBkYXNoZWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IDFweDtcclxuICAgIGJvcmRlci1jb2xvcjogI2JhYmZiZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuLmRpdkxhYmVsXHJcbntcclxuICAgIGNvbG9yOiAjNGQ2ZDk4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG4uY2FyZC1mb3JtLWhlYWRlci10aXRsZSB7XHJcblxyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xyXG4gICAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xyXG4gICAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzOyBcclxuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgIHdpZHRoOiAxMDAlOyBcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLXR5cGUtbWF0LW5hdGl2ZS1zZWxlY3QubWF0LWZvcm0tZmllbGQtZGlzYWJsZWQgLm1hdC1mb3JtLWZpZWxkLWluZml4OjphZnRlciwgXHJcbi5tYXQtaW5wdXQtZWxlbWVudDpkaXNhYmxlZCB7XHJcbiAgICBjb2xvcjogIzRkNmQ5ODtcclxufSIsIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtaGVhZGVyLWNlbGwge1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgY29sb3I6ICMwNDRkYTE7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGN1cnNvcjogY29sLXJlc2l6ZTtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxudHIubWF0LWhlYWRlci1yb3cge1xuICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcbiAgaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5tYXQtdGFibGUge1xuICBib3JkZXItc3BhY2luZzogMDtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG50ZC5tYXQtY2VsbCwgdGQubWF0LWZvb3Rlci1jZWxsLFxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLXdpZHRoOiB0aGluO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmNhcmQtaGVhZGVyLXRpdGxlIHtcbiAgd2lkdGg6IDU1JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcbiAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xuICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7XG4gIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuLmRpdkJveCB7XG4gIGJvcmRlci1zdHlsZTogZGFzaGVkO1xuICBib3JkZXItd2lkdGg6IDFweDtcbiAgYm9yZGVyLWNvbG9yOiAjYmFiZmJmO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uZGl2TGFiZWwge1xuICBjb2xvcjogIzRkNmQ5ODtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLmNhcmQtZm9ybS1oZWFkZXItdGl0bGUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLXR5cGUtbWF0LW5hdGl2ZS1zZWxlY3QubWF0LWZvcm0tZmllbGQtZGlzYWJsZWQgLm1hdC1mb3JtLWZpZWxkLWluZml4OjphZnRlcixcbi5tYXQtaW5wdXQtZWxlbWVudDpkaXNhYmxlZCB7XG4gIGNvbG9yOiAjNGQ2ZDk4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/asset/approveregister/approveregister.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/asset/approveregister/approveregister.component.ts ***!
  \********************************************************************/
/*! exports provided: ApproveRegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproveRegisterComponent", function() { return ApproveRegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
//#region Imports all classes 
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












//#endregion Imports all classes 
var ApproveRegisterComponent = /** @class */ (function () {
    function ApproveRegisterComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.ApproveRegisterList = false;
        this.ApproveRegisterCreate = false;
        this.userLobId = [];
        this.userSublobId = [];
        this.users = [];
        this.lobs = [];
        this.sublobs = [];
        this.inventorytype = [];
        this.entity = [];
        this.vendor = [];
        this.displayedColumns = [];
        //#endregion
        this.columns = [
            { field: 'Id', width: 5 }, { field: 'Code', width: 30 }, { field: 'Lob', width: 60 }, { field: 'Sublob', width: 5 }, { field: 'InventoryType', width: 5 },
            { field: 'InventoryName', width: 5 }, { field: 'Validity', width: 5 }, { field: 'RaisedBy', width: 5 }, { field: 'RaisedDate', width: 5 }, { field: 'Status', width: 5 }
        ];
        this.location = location;
    }
    ApproveRegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        if (path.indexOf(";") > -1) {
            path = path.substr(0, path.indexOf(";"));
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.ApproveRegisterForm = this.formBuilder.group({
            Id: [''], LobId: [''], Lob: [''], SublobId: [''], Sublob: [''], EntityId: [''], Entity: [''],
            InventoryId: [''], InventoryType: [''], InventoryName: [''], IPAddress: [''], Purpose: [''],
            ValidityFrom: [], ValidityTo: [], VendorId: [''], Vendor: [''], IsADUser: [], EmpName: [''], ContactNo: [''],
            TechName: [''], TechContactNo: [''], TechEmailId: [''], EmailId: [''], Reason: [''], Impact: [''],
            Remarks: [''], Dependency: [''], SearchName: [''], SearchTName: [''],
            RejectReason: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
        });
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.userLoggedIn.UserLobs.forEach(function (element) { _this.userLobId.push(element.LobId); });
        this.userLoggedIn.UserSublobs.forEach(function (element) { _this.userSublobId.push(element.SublobId); });
        this.getAllApproveRegister();
    };
    Object.defineProperty(ApproveRegisterComponent.prototype, "Id", {
        get: function () { return this.ApproveRegisterForm.get('Id'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Code", {
        get: function () { return this.ApproveRegisterForm.get('Code'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "EntityId", {
        get: function () { return this.ApproveRegisterForm.get('EntityId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "LobId", {
        get: function () { return this.ApproveRegisterForm.get('LobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "InventoryId", {
        get: function () { return this.ApproveRegisterForm.get('InventoryId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "SublobId", {
        get: function () { return this.ApproveRegisterForm.get('SublobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Sublob", {
        get: function () { return this.ApproveRegisterForm.get('Sublob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Entity", {
        get: function () { return this.ApproveRegisterForm.get('Entity'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Lob", {
        get: function () { return this.ApproveRegisterForm.get('Lob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "InventoryType", {
        get: function () { return this.ApproveRegisterForm.get('InventoryType'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "InventoryName", {
        get: function () { return this.ApproveRegisterForm.get('InventoryName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "IPAddress", {
        get: function () { return this.ApproveRegisterForm.get('IPAddress'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Purpose", {
        get: function () { return this.ApproveRegisterForm.get('Purpose'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "ValidityFrom", {
        get: function () { return this.ApproveRegisterForm.get('ValidityFrom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "ValidityTo", {
        get: function () { return this.ApproveRegisterForm.get('ValidityTo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "VendorId", {
        get: function () { return this.ApproveRegisterForm.get('VendorId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Vendor", {
        get: function () { return this.ApproveRegisterForm.get('Vendor'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "ContactNo", {
        get: function () { return this.ApproveRegisterForm.get('ContactNo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "EmailId", {
        get: function () { return this.ApproveRegisterForm.get('EmailId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "EmpName", {
        get: function () { return this.ApproveRegisterForm.get('EmpName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Consumer", {
        get: function () { return this.ApproveRegisterForm.get('Consumer'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "TechContactNo", {
        get: function () { return this.ApproveRegisterForm.get('TechContactNo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "TechEmailId", {
        get: function () { return this.ApproveRegisterForm.get('TechEmailId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "TechName", {
        get: function () { return this.ApproveRegisterForm.get('TechName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "TechOwner", {
        get: function () { return this.ApproveRegisterForm.get('TechOwner'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Reason", {
        get: function () { return this.ApproveRegisterForm.get('Reason'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Remarks", {
        get: function () { return this.ApproveRegisterForm.get('Remarks'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Impact", {
        get: function () { return this.ApproveRegisterForm.get('Impact'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "Dependency", {
        get: function () { return this.ApproveRegisterForm.get('Dependency'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "SearchName", {
        get: function () { return this.ApproveRegisterForm.get('SearchName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "SearchTName", {
        get: function () { return this.ApproveRegisterForm.get('SearchTName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApproveRegisterComponent.prototype, "RejectReason", {
        get: function () { return this.ApproveRegisterForm.get('RejectReason'); },
        enumerable: true,
        configurable: true
    });
    ApproveRegisterComponent.prototype.getAllApproveRegister = function () {
        var _this = this;
        var filter = localStorage.getItem('Type');
        var assetType = localStorage.getItem('TypeId');
        var group = localStorage.getItem('GroupId') != null ? localStorage.getItem('GroupId') : '0';
        this.rest.getAll(this.global.getapiendpoint() + 'asset/GetAllAsset/' + this.userLoggedIn.Id + '/' + filter + '/' + assetType + '/' + group + '/' + 'A').subscribe(function (data) {
            var tableData = [];
            data.Data.forEach(function (element) {
                tableData.push({
                    Id: element.Id, Code: element.Code, Lob: element.Lob,
                    Sublob: element.Sublob, InventoryType: element.InventoryType, InventoryName: element.InventoryName,
                    Validity: element.Validity != null ? moment__WEBPACK_IMPORTED_MODULE_7__(element.Validity).format("DD/MM/YYYY") : 'NA',
                    RaisedBy: element.RaisedBy,
                    RaisedDate: moment__WEBPACK_IMPORTED_MODULE_7__(element.RaisedDate).format("DD/MM/YYYY"),
                    Status: element.Status,
                    StatusId: element.StatusId
                });
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](tableData);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
        this.ApproveRegisterCreate = false;
        this.ApproveRegisterList = true;
    };
    ApproveRegisterComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    ApproveRegisterComponent.prototype.backApproveRegister = function () {
        this.ApproveRegisterCreate = false;
        this.ApproveRegisterList = true;
        this.topdiv.nativeElement.scrollIntoView({ behavior: 'smooth' });
    };
    ApproveRegisterComponent.prototype.approveRegister = function (Id, type) {
        var _this = this;
        this.rest.getById(this.global.getapiendpoint() + 'asset/GetAssetById/', Id).subscribe(function (data) {
            _this.Id.setValue(data.Data[0].Id);
            _this.Entity.setValue(data.Data[0].Entity);
            _this.Lob.setValue(data.Data[0].Lob);
            _this.Sublob.setValue(data.Data[0].Sublob);
            _this.InventoryType.setValue(data.Data[0].InventoryType);
            _this.InventoryName.setValue(data.Data[0].InventoryName);
            _this.IPAddress.setValue(data.Data[0].IPAddress);
            _this.Vendor.setValue(data.Data[0].Vendor);
            _this.ValidityFrom.setValue(data.Data[0].ValidityFrom != null ? data.Data[0].ValidityFrom : 'NA');
            _this.ValidityTo.setValue(data.Data[0].ValidityTo != null ? data.Data[0].ValidityTo : 'NA');
            _this.Purpose.setValue(data.Data[0].Purpose);
            _this.EmpName.setValue(data.Data[0].EmpName);
            _this.EmailId.setValue(data.Data[0].EmpEmailId);
            _this.ContactNo.setValue(data.Data[0].EmpContactNumber);
            _this.TechName.setValue(data.Data[0].TechName);
            _this.TechEmailId.setValue(data.Data[0].TechEmailId);
            _this.TechContactNo.setValue(data.Data[0].TechContactNumber);
            _this.Dependency.setValue(data.Data[0].Dependency);
            _this.Impact.setValue(data.Data[0].ImpactFailure);
            _this.Reason.setValue(data.Data[0].Reason);
            _this.Remarks.setValue(data.Data[0].Remarks);
            _this.ApproveRegisterCreate = true;
            _this.ApproveRegisterList = false;
        });
        this.view();
        if (type == "view")
            this.IsView = true;
        else
            this.IsView = false;
    };
    ApproveRegisterComponent.prototype.view = function () {
        this.Entity.disable();
        this.Lob.disable();
        this.Sublob.disable();
        this.InventoryType.disable();
        this.InventoryName.disable();
        this.IPAddress.disable();
        this.Vendor.disable();
        this.ValidityFrom.disable();
        this.ValidityTo.disable();
        this.Purpose.disable();
        this.EmpName.disable();
        this.EmailId.disable();
        this.ContactNo.disable();
        this.TechName.disable();
        this.TechEmailId.disable();
        this.TechContactNo.disable();
        this.Dependency.disable();
        this.Impact.disable();
        this.Reason.disable();
        this.Reason.setValue('');
        this.Remarks.disable();
    };
    ApproveRegisterComponent.prototype.exportApproveRegister = function () {
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'ApproveRegister');
        this.toastr.showNotification('top', 'right', 'Exported Successfully', 'success');
    };
    ApproveRegisterComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    ApproveRegisterComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    ApproveRegisterComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    ApproveRegisterComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_10__["KEY_CODE"].B_KEY) {
            this.backApproveRegister();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_10__["KEY_CODE"].A_KEY) {
            this.saveApproveRegister(2);
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_10__["KEY_CODE"].X_KEY) {
            this.exportApproveRegister();
        }
    };
    ApproveRegisterComponent.prototype.saveApproveRegister = function (action) {
        var _this = this;
        if (action == '-1' && this.RejectReason.value.toString().trim() == "") {
            this.toastr.showNotification('top', 'right', "Reason for Reject is required", 'danger');
        }
        else {
            this.rest.checkDuplicate(this.global.getapiendpoint() + 'asset/CheckApproveRegister/', this.Id.value.toString().trim(), (this.Id.value !== '' ? this.Id.value : '0')).subscribe(function (data) {
                if (data.Data) {
                    _this.toastr.showNotification('top', 'right', data.Message, 'danger');
                }
                else {
                    var model = {
                        Id: _this.Id.value,
                        Reason: _this.RejectReason.value.trim(),
                        UserId: _this.userLoggedIn.Id,
                        UserRoleId: _this.userLoggedIn.DefaultRoleId,
                        Action: action == '2' ? 2 : -1,
                        StatusId: action == '2' ? 2 : -1
                    };
                    var apiUrl = '';
                    apiUrl = 'asset/ApproveRegister';
                    _this.rest.create(_this.global.getapiendpoint() + apiUrl, model).subscribe(function (data) {
                        if (data.Success) {
                            _this.toastr.showNotification('top', 'right', data.Message, 'success');
                        }
                        else {
                            _this.toastr.showNotification('top', 'right', data.Message, 'danger');
                        }
                        _this.getAllApproveRegister();
                    });
                }
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('topdiv', { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ApproveRegisterComponent.prototype, "topdiv", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ApproveRegisterComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ApproveRegisterComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ApproveRegisterComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], ApproveRegisterComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_11__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_11__["FlexyColumnComponent"])
    ], ApproveRegisterComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ApproveRegisterComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], ApproveRegisterComponent.prototype, "keyEvent", null);
    ApproveRegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-approveregister',
            template: __webpack_require__(/*! raw-loader!./approveregister.component.html */ "./node_modules/raw-loader/index.js!./src/app/asset/approveregister/approveregister.component.html"),
            styles: [__webpack_require__(/*! ./approveregister.component.scss */ "./src/app/asset/approveregister/approveregister.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_9__["ExcelService"]])
    ], ApproveRegisterComponent);
    return ApproveRegisterComponent;
}());



/***/ }),

/***/ "./src/app/asset/asset-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/asset/asset-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AssetRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetRoutingModule", function() { return AssetRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/common/auth.guard */ "./src/app/common/auth.guard.ts");
/* harmony import */ var _asset_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./asset.component */ "./src/app/asset/asset.component.ts");
/* harmony import */ var _assetregister_assetregister_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assetregister/assetregister.component */ "./src/app/asset/assetregister/assetregister.component.ts");
/* harmony import */ var _viewregister_viewregister_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./viewregister/viewregister.component */ "./src/app/asset/viewregister/viewregister.component.ts");
/* harmony import */ var _approveregister_approveregister_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./approveregister/approveregister.component */ "./src/app/asset/approveregister/approveregister.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '', component: _asset_component__WEBPACK_IMPORTED_MODULE_3__["AssetComponent"], children: [
            { path: 'assetregister', component: _assetregister_assetregister_component__WEBPACK_IMPORTED_MODULE_4__["AssetRegisterComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
            { path: 'viewregister', component: _viewregister_viewregister_component__WEBPACK_IMPORTED_MODULE_5__["ViewRegisterComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
            { path: 'approveregister', component: _approveregister_approveregister_component__WEBPACK_IMPORTED_MODULE_6__["ApproveRegisterComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
        ]
    }
];
var AssetRoutingModule = /** @class */ (function () {
    function AssetRoutingModule() {
    }
    AssetRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AssetRoutingModule);
    return AssetRoutingModule;
}());



/***/ }),

/***/ "./src/app/asset/asset.component.scss":
/*!********************************************!*\
  !*** ./src/app/asset/asset.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Fzc2V0L2Fzc2V0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/asset/asset.component.ts":
/*!******************************************!*\
  !*** ./src/app/asset/asset.component.ts ***!
  \******************************************/
/*! exports provided: AssetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetComponent", function() { return AssetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AssetComponent = /** @class */ (function () {
    function AssetComponent() {
    }
    AssetComponent.prototype.ngOnInit = function () { };
    AssetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-asset',
            template: __webpack_require__(/*! raw-loader!./asset.component.html */ "./node_modules/raw-loader/index.js!./src/app/asset/asset.component.html"),
            styles: [__webpack_require__(/*! ./asset.component.scss */ "./src/app/asset/asset.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AssetComponent);
    return AssetComponent;
}());



/***/ }),

/***/ "./src/app/asset/asset.module.ts":
/*!***************************************!*\
  !*** ./src/app/asset/asset.module.ts ***!
  \***************************************/
/*! exports provided: AssetModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetModule", function() { return AssetModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _asset_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./asset-routing.module */ "./src/app/asset/asset-routing.module.ts");
/* harmony import */ var app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _assetregister_assetregister_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assetregister/assetregister.component */ "./src/app/asset/assetregister/assetregister.component.ts");
/* harmony import */ var _viewregister_viewregister_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./viewregister/viewregister.component */ "./src/app/asset/viewregister/viewregister.component.ts");
/* harmony import */ var _approveregister_approveregister_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./approveregister/approveregister.component */ "./src/app/asset/approveregister/approveregister.component.ts");
/* harmony import */ var _asset_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./asset.component */ "./src/app/asset/asset.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AssetModule = /** @class */ (function () {
    function AssetModule() {
    }
    AssetModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _asset_routing_module__WEBPACK_IMPORTED_MODULE_2__["AssetRoutingModule"],
                app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            declarations: [
                _asset_component__WEBPACK_IMPORTED_MODULE_7__["AssetComponent"],
                _assetregister_assetregister_component__WEBPACK_IMPORTED_MODULE_4__["AssetRegisterComponent"],
                _viewregister_viewregister_component__WEBPACK_IMPORTED_MODULE_5__["ViewRegisterComponent"],
                _approveregister_approveregister_component__WEBPACK_IMPORTED_MODULE_6__["ApproveRegisterComponent"]
            ],
            exports: [
                app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            entryComponents: []
        })
    ], AssetModule);
    return AssetModule;
}());



/***/ }),

/***/ "./src/app/asset/assetregister/assetregister.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/asset/assetregister/assetregister.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #044da1;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n\n.divBox {\n  border-style: dashed;\n  border-width: 1px;\n  border-color: #babfbf;\n  margin-bottom: 10px;\n}\n\n.divLabel {\n  color: #4d6d98;\n  font-weight: 600;\n}\n\n.mat-form-field-type-mat-native-select.mat-form-field-disabled .mat-form-field-infix::after,\n.mat-input-element:disabled {\n  color: #4d6d98;\n}\n\n.card-form-header-title {\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  border-radius: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXNzZXQvYXNzZXRyZWdpc3Rlci9FOlxcREFSIFVBVCBGaWxlc1xcQXNzZXQtVUkvc3JjXFxhcHBcXGFzc2V0XFxhc3NldHJlZ2lzdGVyXFxhc3NldHJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hc3NldC9hc3NldHJlZ2lzdGVyL2Fzc2V0cmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtBQ0NKOztBREVBO0VBQ0ksMEJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBQ0NKOztBREVBOztFQUVJLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksb0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QUNDSjs7QURDQTtFQUVJLGNBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBOztFQUdJLGNBQUE7QUNBSjs7QURHQTtFQUVJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQ0RKIiwiZmlsZSI6InNyYy9hcHAvYXNzZXQvYXNzZXRyZWdpc3Rlci9hc3NldHJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LWZvcm0tZmllbGRbaGlkZGVuXSB7XHJcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbnRhYmxle1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGx7XHJcbiAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDsgXHJcbiAgICBjb2xvcjogIzA0NGRhMTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBjdXJzb3I6IGNvbC1yZXNpemU7XHJcbiAgICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufVxyXG5cclxudHIubWF0LWhlYWRlci1yb3cge1xyXG4gICAgaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDsgXHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG50ci5tYXQtZm9vdGVyLXJvdywgdHIubWF0LXJvdyB7XHJcbiAgICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcclxufVxyXG4gIFxyXG4ubWF0LXRhYmxle1xyXG4gICAgYm9yZGVyLXNwYWNpbmc6IDA7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZjdmNGYwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsICBcclxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDEycHg7IFxyXG59XHJcblxyXG4uY2FyZC1oZWFkZXItdGl0bGV7XHJcbiAgICB3aWR0aDogNTUlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNzIsIDEzMCwgMTk3KTtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcclxuICAgIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMzsgXHJcbiAgICBwYWRkaW5nOiA0cHggNHB4IDBweCAxNHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG59XHJcblxyXG4uZGl2Qm94e1xyXG4gICAgYm9yZGVyLXN0eWxlOiBkYXNoZWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IDFweDtcclxuICAgIGJvcmRlci1jb2xvcjogI2JhYmZiZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuLmRpdkxhYmVsXHJcbntcclxuICAgIGNvbG9yOiAjNGQ2ZDk4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG4gXHJcbi5tYXQtZm9ybS1maWVsZC10eXBlLW1hdC1uYXRpdmUtc2VsZWN0Lm1hdC1mb3JtLWZpZWxkLWRpc2FibGVkIC5tYXQtZm9ybS1maWVsZC1pbmZpeDo6YWZ0ZXIsIFxyXG4ubWF0LWlucHV0LWVsZW1lbnQ6ZGlzYWJsZWQge1xyXG4gICAgIFxyXG4gICAgY29sb3I6ICM0ZDZkOTg7XHJcbn1cclxuXHJcbi5jYXJkLWZvcm0taGVhZGVyLXRpdGxlIHtcclxuXHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0ODgyYzU7XHJcbiAgICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XHJcbiAgICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7IFxyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG59IiwibWF0LWZvcm0tZmllbGRbaGlkZGVuXSB7XG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbn1cblxudGFibGUge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1oZWFkZXItY2VsbCB7XG4gIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzA0NGRhMTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY3Vyc29yOiBjb2wtcmVzaXplO1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufVxuXG50ci5tYXQtaGVhZGVyLXJvdyB7XG4gIGhlaWdodDogNDBweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxudHIubWF0LWZvb3Rlci1yb3csIHRyLm1hdC1yb3cge1xuICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcbn1cblxuLm1hdC10YWJsZSB7XG4gIGJvcmRlci1zcGFjaW5nOiAwO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIGJvcmRlci1jb2xvcjogI2Y3ZjRmMDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uY2FyZC1oZWFkZXItdGl0bGUge1xuICB3aWR0aDogNTUlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgcGFkZGluZzogNHB4IDRweCAwcHggMTRweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuXG4uZGl2Qm94IHtcbiAgYm9yZGVyLXN0eWxlOiBkYXNoZWQ7XG4gIGJvcmRlci13aWR0aDogMXB4O1xuICBib3JkZXItY29sb3I6ICNiYWJmYmY7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5kaXZMYWJlbCB7XG4gIGNvbG9yOiAjNGQ2ZDk4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWF0LWZvcm0tZmllbGQtdHlwZS1tYXQtbmF0aXZlLXNlbGVjdC5tYXQtZm9ybS1maWVsZC1kaXNhYmxlZCAubWF0LWZvcm0tZmllbGQtaW5maXg6OmFmdGVyLFxuLm1hdC1pbnB1dC1lbGVtZW50OmRpc2FibGVkIHtcbiAgY29sb3I6ICM0ZDZkOTg7XG59XG5cbi5jYXJkLWZvcm0taGVhZGVyLXRpdGxlIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcbiAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xuICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/asset/assetregister/assetregister.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/asset/assetregister/assetregister.component.ts ***!
  \****************************************************************/
/*! exports provided: AssetRegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetRegisterComponent", function() { return AssetRegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/validators/requiredmatch.validator */ "./src/app/validators/requiredmatch.validator.ts");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var AssetRegisterComponent = /** @class */ (function () {
    function AssetRegisterComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.AssetRegisterList = false;
        this.AssetRegisterCreate = false;
        this.userLobId = [];
        this.userSublobId = [];
        this.users = [];
        this.cloneFilter = "";
        this.lobs = [];
        this.sublobs = [];
        this.inventorytype = [];
        this.entity = [];
        this.vendor = [];
        this.readonly = true;
        this.displayedColumns = [];
        this.columns = [
            { field: 'Id', width: 5 }, { field: 'Code', width: 30 }, { field: 'Lob', width: 60 }, { field: 'Sublob', width: 5 }, { field: 'InventoryType', width: 5 },
            { field: 'InventoryName', width: 5 }, { field: 'Validity', width: 5 }, { field: 'RaisedBy', width: 5 }, { field: 'RaisedDate', width: 5 }, { field: 'Status', width: 5 }
        ];
        this.location = location;
    }
    AssetRegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.IsExpired = localStorage.getItem('Type');
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        if (path.indexOf(";") > -1) {
            path = path.substr(0, path.indexOf(";"));
        }
        this.UIObj = this.global.getUIObj(path);
        if (this.UIObj == null) {
            this.toastr.showNotification('top', 'right', "This is an unauthorised action by this user role", 'danger');
        }
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.AssetRegisterForm = this.formBuilder.group({
            Id: [''],
            Code: [],
            LobId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            Lob: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_10__["RequireMatch"]]],
            SublobId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            Sublob: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_10__["RequireMatch"]]],
            EntityId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            Entity: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_10__["RequireMatch"]]],
            InventoryId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            InventoryType: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_10__["RequireMatch"]]],
            InventoryName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            IPAddress: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9\\*\\:\\s\\.\\,]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            Purpose: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(2000)]],
            ValidityFrom: [moment__WEBPACK_IMPORTED_MODULE_8__()],
            ValidityTo: [moment__WEBPACK_IMPORTED_MODULE_8__()],
            VendorId: [''],
            Vendor: ['', [app_validators_requiredmatch_validator__WEBPACK_IMPORTED_MODULE_10__["RequireMatch"]]],
            IsADUser: [],
            EmpName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z0-9\\s\\(\\)\\-&.\\\\,/_]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            ContactNo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9\\,]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(12)]],
            TechName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z0-9\\s\\(\\)\\-&.\\\\,/_]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            TechContactNo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9\\,]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(12)]],
            TechEmailId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            EmailId: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(100)]],
            Reason: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(2000)]],
            Impact: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(2000)]],
            Remarks: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(2000)]],
            Dependency: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(2000)]],
            IsTADUser: [],
            SearchName: [],
            SearchTName: [],
            StatusId: [],
            RejectReason: [],
            ParentId: []
        });
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.userLoggedIn.UserLobs.forEach(function (element) { _this.userLobId.push(element.LobId); });
        this.userLoggedIn.UserSublobs.forEach(function (element) { _this.userSublobId.push(element.SublobId); });
        this.getAllAssetRegister("View");
        this.getAllEntity();
        this.filteredEntity = this.Entity.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (value) { return value ? _this._filter(value, _this.entity) : _this.entity.slice(); }));
        this.getAllLOB();
        this.filteredlobs = this.Lob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (value) { return value ? _this._filter(value, _this.lobs) : _this.lobs.slice(); }));
        this.getAllInventoryType();
        this.filteredInventorytype = this.InventoryType.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (value) { return value ? _this._filter(value, _this.inventorytype) : _this.inventorytype.slice(); }));
        this.getAllVendor();
        this.filteredVendor = this.Vendor.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (value) { return value ? _this._filter(value, _this.vendor) : _this.vendor.slice(); }));
        this.SearchName.valueChanges.pipe().subscribe(function (text) {
            if (text != null && text != '') {
                _this.users = [];
                _this.getADUsers(text);
            }
            else {
                _this.users = [];
            }
        });
        this.SearchTName.valueChanges.pipe().subscribe(function (text) {
            if (text != null && text != '') {
                _this.users = [];
                _this.getADUsers(text);
            }
            else {
                _this.users = [];
            }
        });
    };
    Object.defineProperty(AssetRegisterComponent.prototype, "Id", {
        get: function () { return this.AssetRegisterForm.get('Id'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Code", {
        get: function () { return this.AssetRegisterForm.get('Code'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "EntityId", {
        get: function () { return this.AssetRegisterForm.get('EntityId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "LobId", {
        get: function () { return this.AssetRegisterForm.get('LobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "InventoryId", {
        get: function () { return this.AssetRegisterForm.get('InventoryId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "SublobId", {
        get: function () { return this.AssetRegisterForm.get('SublobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Sublob", {
        get: function () { return this.AssetRegisterForm.get('Sublob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Entity", {
        get: function () { return this.AssetRegisterForm.get('Entity'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Lob", {
        get: function () { return this.AssetRegisterForm.get('Lob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "InventoryType", {
        get: function () { return this.AssetRegisterForm.get('InventoryType'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "InventoryName", {
        get: function () { return this.AssetRegisterForm.get('InventoryName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "IPAddress", {
        get: function () { return this.AssetRegisterForm.get('IPAddress'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Purpose", {
        get: function () { return this.AssetRegisterForm.get('Purpose'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "ValidityFrom", {
        get: function () { return this.AssetRegisterForm.get('ValidityFrom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "ValidityTo", {
        get: function () { return this.AssetRegisterForm.get('ValidityTo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "VendorId", {
        get: function () { return this.AssetRegisterForm.get('VendorId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Vendor", {
        get: function () { return this.AssetRegisterForm.get('Vendor'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "ContactNo", {
        get: function () { return this.AssetRegisterForm.get('ContactNo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "EmailId", {
        get: function () { return this.AssetRegisterForm.get('EmailId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "EmpName", {
        get: function () { return this.AssetRegisterForm.get('EmpName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Consumer", {
        get: function () { return this.AssetRegisterForm.get('Consumer'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "TechContactNo", {
        get: function () { return this.AssetRegisterForm.get('TechContactNo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "TechEmailId", {
        get: function () { return this.AssetRegisterForm.get('TechEmailId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "TechName", {
        get: function () { return this.AssetRegisterForm.get('TechName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "TechOwner", {
        get: function () { return this.AssetRegisterForm.get('TechOwner'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Reason", {
        get: function () { return this.AssetRegisterForm.get('Reason'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Remarks", {
        get: function () { return this.AssetRegisterForm.get('Remarks'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Impact", {
        get: function () { return this.AssetRegisterForm.get('Impact'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "Dependency", {
        get: function () { return this.AssetRegisterForm.get('Dependency'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "SearchName", {
        get: function () { return this.AssetRegisterForm.get('SearchName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "ADUser", {
        get: function () { return this.AssetRegisterForm.get('IsADUser'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "SearchTName", {
        get: function () { return this.AssetRegisterForm.get('SearchTName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "TADUser", {
        get: function () { return this.AssetRegisterForm.get('IsTADUser'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "StatusId", {
        get: function () { return this.AssetRegisterForm.get('StatusId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "RejectReason", {
        get: function () { return this.AssetRegisterForm.get('RejectReason'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetRegisterComponent.prototype, "ParentId", {
        get: function () { return this.AssetRegisterForm.get('ParentId'); },
        enumerable: true,
        configurable: true
    });
    AssetRegisterComponent.prototype.getAllAssetRegister = function (type) {
        var _this = this;
        var filter = localStorage.getItem('Type');
        var assetType = localStorage.getItem('TypeId');
        var group = localStorage.getItem('GroupId') != null ? localStorage.getItem('GroupId') : '0';
        if (type != "List")
            this.cloneFilter = localStorage.getItem('Clone') != null ? localStorage.getItem('Clone') : "";
        if (this.cloneFilter != "") {
            this.updateAssetRegister(this.cloneFilter);
            this.AssetRegisterCreate = true;
            this.AssetRegisterList = false;
        }
        else {
            this.rest.getAll(this.global.getapiendpoint() + 'asset/GetAllAsset/' + this.userLoggedIn.Id + '/' + filter + '/' +
                assetType + '/' + group + '/' + 'E').subscribe(function (data) {
                var tableData = [];
                data.Data.forEach(function (element) {
                    tableData.push({
                        Id: element.Id, Code: element.Code, Lob: element.Lob,
                        Sublob: element.Sublob, InventoryType: element.InventoryType, InventoryName: element.InventoryName,
                        Validity: element.Validity != null ? moment__WEBPACK_IMPORTED_MODULE_8__(element.Validity).format("DD/MM/YYYY") : 'NA',
                        RaisedBy: element.RaisedBy,
                        RaisedDate: element.RaisedDate != null ? moment__WEBPACK_IMPORTED_MODULE_8__(element.RaisedDate).format("DD/MM/YYYY") : 'NA',
                        Status: element.Status,
                        StatusId: element.StatusId
                    });
                });
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](tableData);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
            });
            this.AssetRegisterCreate = false;
            this.AssetRegisterList = true;
            this.IsView = false;
        }
    };
    AssetRegisterComponent.prototype.clearToDate = function () {
        this.ValidityTo.setValue(null);
    };
    AssetRegisterComponent.prototype.clearFromDate = function () {
        this.ValidityFrom.setValue(null);
    };
    AssetRegisterComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    AssetRegisterComponent.prototype.displayWith = function (obj) {
        return obj ? obj.Code : undefined;
    };
    AssetRegisterComponent.prototype.getAllLOB = function () {
        var _this = this;
        this.lobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "lob/GetAllActiveLOB").subscribe(function (data) {
            _this.lobs = data.Data;
        });
    };
    AssetRegisterComponent.prototype.inputLOB = function (lob) {
        var _this = this;
        this.LobId.setValue("");
        this.SublobId.setValue("");
        this.sublobs = [];
        this.filteredSublobs = this.Sublob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (value) { return value ? _this._filter(value, _this.sublobs) : _this.sublobs.slice(); }));
    };
    AssetRegisterComponent.prototype.selectedLOB = function (lob) {
        var _this = this;
        this.LobId.setValue(lob.Id);
        this.sublobs = [];
        this.getAllSublob(lob.Id);
        this.filteredSublobs = this.Sublob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (value) { return value ? _this._filter(value, _this.sublobs) : _this.sublobs.slice(); }));
    };
    AssetRegisterComponent.prototype.clearLOB = function () {
        var _this = this;
        this.LobId.setValue(null);
        this.Lob.setValue(null);
        this.clearSublob();
        this.sublobs = [];
        this.filteredSublobs = this.Sublob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (value) { return value ? _this._filter(value, _this.sublobs) : _this.sublobs.slice(); }));
    };
    AssetRegisterComponent.prototype.getAllEntity = function () {
        var _this = this;
        this.entity = [];
        this.rest.getAll(this.global.getapiendpoint() + "entity/GetAllActiveEntity").subscribe(function (data) {
            _this.entity = data.Data;
        });
    };
    AssetRegisterComponent.prototype.inputEntity = function (lob) {
        this.EntityId.setValue("");
    };
    AssetRegisterComponent.prototype.selectedEntity = function (entity) {
        this.EntityId.setValue(entity.Id);
    };
    AssetRegisterComponent.prototype.clearEntity = function () {
        this.EntityId.setValue(null);
        this.Entity.setValue(null);
    };
    AssetRegisterComponent.prototype.getAllVendor = function () {
        var _this = this;
        this.vendor = [];
        this.rest.getAll(this.global.getapiendpoint() + "vendor/GetAllActiveVendor").subscribe(function (data) {
            _this.vendor = data.Data;
        });
    };
    AssetRegisterComponent.prototype.inputVendor = function (vendor) {
        this.VendorId.setValue("");
    };
    AssetRegisterComponent.prototype.selectedVendor = function (vendor) {
        this.VendorId.setValue(vendor.Id);
    };
    AssetRegisterComponent.prototype.clearVendor = function () {
        this.VendorId.setValue(null);
        this.Vendor.setValue(null);
    };
    AssetRegisterComponent.prototype.getAllSublob = function (LOBId) {
        var _this = this;
        this.sublobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "sublob/GetSubLOBByLOBId/".concat(LOBId)).subscribe(function (data) {
            _this.sublobs = data.Data;
        });
    };
    AssetRegisterComponent.prototype.inputSublob = function (sublob) {
        this.SublobId.setValue("");
    };
    AssetRegisterComponent.prototype.selectedSublob = function (sublob) {
        this.SublobId.setValue(sublob.Id);
    };
    AssetRegisterComponent.prototype.clearSublob = function () {
        this.SublobId.setValue(null);
        this.Sublob.setValue(null);
    };
    AssetRegisterComponent.prototype.getAllInventoryType = function () {
        var _this = this;
        this.inventorytype = [];
        this.rest.getAll(this.global.getapiendpoint() + "inventorytype/GetAllActiveInventoryType").subscribe(function (data) {
            _this.inventorytype = data.Data;
        });
    };
    AssetRegisterComponent.prototype.inputInventoryType = function (lob) {
        this.InventoryId.setValue("");
    };
    AssetRegisterComponent.prototype.selectedInventoryType = function (Inventory) {
        this.InventoryId.setValue(Inventory.Id);
    };
    AssetRegisterComponent.prototype.clearInventoryType = function () {
        this.InventoryId.setValue(null);
        this.InventoryType.setValue(null);
    };
    AssetRegisterComponent.prototype.getADUsers = function (Text) {
        var _this = this;
        this.rest.getAll(this.global.getapiendpoint() + "ad/FindUsers/".concat(Text)).subscribe(function (data) {
            _this.users = data.Data;
        });
    };
    AssetRegisterComponent.prototype._filter = function (value, obj) {
        var filterValue = (value ? (value.Code ? value.Code.toLowerCase() : value.toLowerCase()) : "");
        return obj.filter(function (o) { return o.Code.toLowerCase().includes(filterValue); });
    };
    AssetRegisterComponent.prototype.inputUser = function (user) {
    };
    AssetRegisterComponent.prototype.selectedUser = function (user) {
        this.SearchName.setValue(user.cn);
        this.EmpName.setValue(user.cn);
        this.EmailId.setValue(user.mail);
    };
    AssetRegisterComponent.prototype.onADUserChange = function (value) {
        if (!this.ADUser.value) {
            this.SearchName.setValue('');
            this.EmpName.setValue('');
            this.EmailId.setValue('');
            this.ContactNo.setValue('');
        }
    };
    AssetRegisterComponent.prototype.selectedTUser = function (user) {
        this.SearchTName.setValue(user.cn);
        this.TechName.setValue(user.cn);
        this.TechEmailId.setValue(user.mail);
    };
    AssetRegisterComponent.prototype.onADTUserChange = function (value) {
        if (!this.TADUser.value) {
            this.SearchTName.setValue('');
            this.TechName.setValue('');
            this.TechEmailId.setValue('');
            this.TechContactNo.setValue('');
        }
    };
    AssetRegisterComponent.prototype.createAssetRegister = function () {
        this.IsView = false;
        this.form.resetForm();
        this.AssetRegisterForm.markAsUntouched();
        this.Id.setValue('');
        this.Entity.enable();
        this.Lob.enable();
        this.Sublob.enable();
        this.InventoryType.enable();
        this.InventoryName.enable();
        this.IPAddress.enable();
        this.Vendor.enable();
        this.ValidityFrom.enable();
        this.ValidityTo.enable();
        this.Purpose.enable();
        this.EmpName.enable();
        this.EmailId.enable();
        this.ContactNo.enable();
        this.TechName.enable();
        this.TechEmailId.enable();
        this.TechContactNo.enable();
        this.Dependency.enable();
        this.Impact.enable();
        this.Reason.enable();
        this.Remarks.enable();
        this.ADUser.enable();
        this.TADUser.enable();
        this.RejectReason.enable();
        this.AssetRegisterCreate = true;
        this.AssetRegisterList = false;
    };
    AssetRegisterComponent.prototype.backAssetRegister = function () {
        this.AssetRegisterCreate = false;
        this.AssetRegisterList = true;
        if (this.cloneFilter != "")
            this.getAssetRegisterList();
        else
            this.getAllAssetRegister("List");
        this.topdiv.nativeElement.scrollIntoView({ behavior: 'smooth' });
    };
    AssetRegisterComponent.prototype.updateAssetRegister = function (Id) {
        this.IsView = false;
        this.getdata(Id);
    };
    ;
    AssetRegisterComponent.prototype.getdata = function (Id) {
        var _this = this;
        this.rest.getById(this.global.getapiendpoint() + 'asset/GetAssetRegisterById/', Id).subscribe(function (data) {
            _this.Id.setValue(data.Data.Id);
            _this.Entity.setValue(data.Data.Entity);
            _this.Lob.setValue(data.Data.LOB);
            _this.Sublob.setValue(data.Data.Sublob);
            _this.InventoryType.setValue(data.Data.InventoryType);
            _this.EntityId.setValue(data.Data.EntityId);
            _this.LobId.setValue(data.Data.LobId);
            _this.SublobId.setValue(data.Data.SublobId);
            _this.InventoryId.setValue(data.Data.InventoryTypeId);
            _this.InventoryName.setValue(data.Data.InventoryName);
            _this.IPAddress.setValue(data.Data.IPAddress);
            _this.Vendor.setValue(data.Data.Vendor);
            _this.ValidityFrom.setValue(data.Data.ValidityFrom != null ? data.Data.ValidityFrom : null);
            _this.ValidityTo.setValue(data.Data.ValidityTo != null ? data.Data.ValidityTo : 'NA');
            _this.RejectReason.setValue(data.Data.AssetRegisterHistories.length > 0 ? data.Data.AssetRegisterHistories[0].Reason : '');
            _this.Purpose.setValue(data.Data.Purpose);
            _this.EmpName.setValue(data.Data.EmpName);
            _this.EmailId.setValue(data.Data.EmpEmailId);
            _this.ContactNo.setValue(data.Data.EmpContactNumber);
            _this.TechName.setValue(data.Data.TechName);
            _this.TechEmailId.setValue(data.Data.TechEmailId);
            _this.TechContactNo.setValue(data.Data.TechContactNumber);
            _this.Dependency.setValue(data.Data.Dependency);
            _this.Impact.setValue(data.Data.ImpactFailure);
            _this.Reason.setValue(_this.cloneFilter != null ? '' : data.Data.Reason);
            _this.Remarks.setValue(_this.cloneFilter != null ? '' : data.Data.Remarks);
            _this.StatusId.setValue(data.Data.StatusId);
            _this.ParentId.setValue(_this.cloneFilter != "" ? _this.cloneFilter : '0');
            _this.Id.setValue(_this.cloneFilter != "" ? 0 : data.Data.Id);
            if (_this.cloneFilter != "")
                if (data.Data.ValidityTo != null && data.Data.IsActive == true) {
                    var date1 = new Date().setHours(0, 0, 0, 0);
                    var date2 = new Date(data.Data.ValidityTo).setHours(0, 0, 0, 0);
                    if (_this.IsExpired == "expired")
                        _this.IsRenew = true;
                    else
                        _this.IsRenew = false;
                }
                else
                    _this.IsRenew = false;
            _this.AssetRegisterCreate = true;
            _this.AssetRegisterList = false;
        });
    };
    AssetRegisterComponent.prototype.viewRegister = function (Id) {
        this.IsView = true;
        this.getdata(Id);
        this.Entity.disable();
        this.Lob.disable();
        this.Sublob.disable();
        this.InventoryType.disable();
        this.InventoryName.disable();
        this.IPAddress.disable();
        this.Vendor.disable();
        this.ValidityFrom.disable();
        this.ValidityTo.disable();
        this.Purpose.disable();
        this.EmpName.disable();
        this.EmailId.disable();
        this.ContactNo.disable();
        this.TechName.disable();
        this.TechEmailId.disable();
        this.TechContactNo.disable();
        this.Dependency.disable();
        this.Impact.disable();
        this.Reason.disable();
        this.Remarks.disable();
        this.ADUser.disable();
        this.TADUser.disable();
        this.RejectReason.disable();
    };
    AssetRegisterComponent.prototype.exportAssetRegister = function () {
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'AssetRegister');
        this.toastr.showNotification('top', 'right', 'Exported Successfully', 'success');
    };
    AssetRegisterComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    AssetRegisterComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    AssetRegisterComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    AssetRegisterComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_12__["KEY_CODE"].A_KEY) {
            this.createAssetRegister();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_12__["KEY_CODE"].B_KEY) {
            this.backAssetRegister();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_12__["KEY_CODE"].S_KEY) {
            this.saveAssetRegister('s');
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_12__["KEY_CODE"].X_KEY) {
            this.exportAssetRegister();
        }
    };
    AssetRegisterComponent.prototype.saveAssetRegister = function (action) {
        var _this = this;
        if (this.EntityId.value == null || this.LobId.value == null || this.SublobId.value == null || this.InventoryId.value == null
            || this.InventoryName.value == null) {
            this.toastr.showNotification('top', 'right', "Please enter mandatory details", "danger");
        }
        else if (this.AssetRegisterForm.invalid || this.InventoryName.value.toString().trim() == "") {
            this.toastr.showNotification('top', 'right', "Please enter valid data", 'danger');
        }
        else {
            var model = {
                Id: this.Id.value,
                EntityId: this.EntityId.value,
                LobId: this.LobId.value,
                SublobId: this.SublobId.value,
                VendorId: this.VendorId.value != "" ? this.VendorId.value : null,
                InventoryId: this.InventoryId.value,
                InventoryName: this.InventoryName.value != null ? this.InventoryName.value.trim() : '',
                IPAddress: this.IPAddress.value != null ? this.IPAddress.value.trim() : '',
                ValidityFrom: this.ValidityFrom.value,
                ValidityTo: this.ValidityTo.value,
                Purpose: this.Purpose.value != null ? this.Purpose.value.trim() : '',
                EmpName: this.EmpName.value != null ? this.EmpName.value.trim() : '',
                EmailId: this.EmailId.value != null ? this.EmailId.value.trim() : '',
                ContactNo: this.ContactNo.value,
                TechName: this.TechName.value != null ? this.TechName.value.trim() : '',
                TechEmailId: this.TechEmailId.value != null ? this.TechEmailId.value.trim() : '',
                TechContactNo: this.TechContactNo.value,
                Dependency: this.Dependency.value != null ? this.Dependency.value.trim() : '',
                Impact: this.Impact.value != null ? this.Impact.value.trim() : '',
                Reason: this.Reason.value != null ? this.Reason.value.trim() : '',
                Remarks: this.Remarks.value != null ? this.Remarks.value.trim() : '',
                UserId: this.userLoggedIn.Id,
                UserRoleId: this.userLoggedIn.DefaultRoleId,
                Action: action == '1' ? 1 : 0,
                Status: action == '1' ? 1 : 0,
            };
            var apiUrl = '';
            if (this.Id.value == '') {
                apiUrl = 'asset/CreateAssetRegister';
            }
            else {
                apiUrl = 'asset/UpdateAssetRegister';
            }
            this.rest.create(this.global.getapiendpoint() + apiUrl, model).subscribe(function (data) {
                if (data.Success) {
                    _this.toastr.showNotification('top', 'right', data.Message, 'success');
                }
                else {
                    _this.toastr.showNotification('top', 'right', data.Message, 'danger');
                }
                if (_this.cloneFilter != "") {
                    _this.deactivateAssetRegister('2');
                    _this.getAssetRegisterList();
                }
                else
                    _this.getAllAssetRegister("List");
            });
        }
        this.topdiv.nativeElement.scrollIntoView({ behavior: 'smooth' });
    };
    AssetRegisterComponent.prototype.deactivateAssetRegister = function (action) {
        var _this = this;
        var model = {
            Id: localStorage.getItem('Clone') != null ? localStorage.getItem('Clone') : this.Id.value,
            Remarks: action == 1 ? 'Renewed Initiated' : 'Deactivated',
            UserId: this.userLoggedIn.Id,
            UserRoleId: this.userLoggedIn.DefaultRoleId,
            IsActive: false
        };
        this.rest.create(this.global.getapiendpoint() + 'asset/DeactivateAssetRegister', model).subscribe(function (data) {
            console.log("data recied", data, "data.succ", data.Success);
            if (_this.cloneFilter != "")
                if (data.Success == true) {
                    _this.toastr.showNotification('top', 'right', 'Deactivated', 'success');
                    _this.backAssetRegister();
                }
                else {
                    _this.toastr.showNotification('top', 'right', data.Message, 'danger');
                }
        });
        if (this.cloneFilter == "")
            this.getAllAssetRegister("List");
        this.topdiv.nativeElement.scrollIntoView({ behavior: 'smooth' });
    };
    AssetRegisterComponent.prototype.getAssetRegisterList = function () {
        var _this = this;
        this.rest.getAll(this.global.getapiendpoint() + 'asset/GetAllAssetList/' + this.userLoggedIn.Id + '/' + 'E').subscribe(function (data) {
            var tableData = [];
            data.Data.forEach(function (element) {
                tableData.push({
                    Id: element.Id, Code: element.Code, Lob: element.Lob,
                    Sublob: element.Sublob, InventoryType: element.InventoryType, InventoryName: element.InventoryName,
                    Validity: element.Validity != null ? moment__WEBPACK_IMPORTED_MODULE_8__(element.Validity).format("DD/MM/YYYY") : 'NA',
                    RaisedBy: element.RaisedBy,
                    RaisedDate: element.RaisedDate != null ? moment__WEBPACK_IMPORTED_MODULE_8__(element.RaisedDate).format("DD/MM/YYYY") : 'NA',
                    Status: element.Status,
                    StatusId: element.StatusId
                });
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](tableData);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
        this.AssetRegisterCreate = false;
        this.AssetRegisterList = true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], AssetRegisterComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('topdiv', { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], AssetRegisterComponent.prototype, "topdiv", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], AssetRegisterComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], AssetRegisterComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], AssetRegisterComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_13__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_13__["FlexyColumnComponent"])
    ], AssetRegisterComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AssetRegisterComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], AssetRegisterComponent.prototype, "keyEvent", null);
    AssetRegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-assetregister',
            template: __webpack_require__(/*! raw-loader!./assetregister.component.html */ "./node_modules/raw-loader/index.js!./src/app/asset/assetregister/assetregister.component.html"),
            styles: [__webpack_require__(/*! ./assetregister.component.scss */ "./src/app/asset/assetregister/assetregister.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_9__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_11__["ExcelService"]])
    ], AssetRegisterComponent);
    return AssetRegisterComponent;
}());



/***/ }),

/***/ "./src/app/asset/viewregister/viewregister.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/asset/viewregister/viewregister.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #044da1;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n\n.mat-form-field-type-mat-native-select.mat-form-field-disabled .mat-form-field-infix::after, .mat-input-element:disabled {\n  color: #4d6d98;\n}\n\n.divBox {\n  border-style: dashed;\n  border-width: 1px;\n  border-color: #babfbf;\n  margin-bottom: 10px;\n}\n\n.divLabel {\n  color: #4d6d98;\n  font-weight: 600;\n}\n\n.card-form-header-title {\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  border-radius: 0px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXNzZXQvdmlld3JlZ2lzdGVyL0U6XFxEQVIgVUFUIEZpbGVzXFxBc3NldC1VSS9zcmNcXGFwcFxcYXNzZXRcXHZpZXdyZWdpc3Rlclxcdmlld3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hc3NldC92aWV3cmVnaXN0ZXIvdmlld3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7QUNDSjs7QURFQTtFQUNJLDBCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtFQUNBLDZCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7QUNDSjs7QURFQTs7RUFFSSxtQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUNBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3Q0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDQTtFQUVJLGNBQUE7QUNDSjs7QURFQTtFQUNJLG9CQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FDQ0o7O0FEQ0E7RUFFSSxjQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURDQTtFQUVJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2Fzc2V0L3ZpZXdyZWdpc3Rlci92aWV3cmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxudGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItY2VsbHtcclxuICAgIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50OyBcclxuICAgIGNvbG9yOiAjMDQ0ZGExO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGN1cnNvcjogY29sLXJlc2l6ZTtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcblxyXG50ci5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50OyBcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcclxuICAgIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xyXG59XHJcbiAgXHJcbi5tYXQtdGFibGV7XHJcbiAgICBib3JkZXItc3BhY2luZzogMDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCwgIFxyXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xyXG4gICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItY29sb3I6ICNlYWVlZWY7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDsgXHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlci10aXRsZXtcclxuICAgIHdpZHRoOiA1NSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYig3MiwgMTMwLCAxOTcpO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xyXG4gICAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzOyBcclxuICAgIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbn1cclxuLm1hdC1mb3JtLWZpZWxkLXR5cGUtbWF0LW5hdGl2ZS1zZWxlY3QubWF0LWZvcm0tZmllbGQtZGlzYWJsZWQgLm1hdC1mb3JtLWZpZWxkLWluZml4OjphZnRlciwgLm1hdC1pbnB1dC1lbGVtZW50OmRpc2FibGVkIHtcclxuICAgICBcclxuICAgIGNvbG9yOiAjNGQ2ZDk4O1xyXG59IFxyXG5cclxuLmRpdkJveHtcclxuICAgIGJvcmRlci1zdHlsZTogZGFzaGVkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiAxcHg7XHJcbiAgICBib3JkZXItY29sb3I6ICNiYWJmYmY7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG59XHJcbi5kaXZMYWJlbFxyXG57XHJcbiAgICBjb2xvcjogIzRkNmQ5ODtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuLmNhcmQtZm9ybS1oZWFkZXItdGl0bGUge1xyXG5cclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzQ4ODJjNTtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcclxuICAgIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMzsgXHJcbiAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG59IiwibWF0LWZvcm0tZmllbGRbaGlkZGVuXSB7XG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbn1cblxudGFibGUge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1oZWFkZXItY2VsbCB7XG4gIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzA0NGRhMTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY3Vyc29yOiBjb2wtcmVzaXplO1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufVxuXG50ci5tYXQtaGVhZGVyLXJvdyB7XG4gIGhlaWdodDogNDBweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxudHIubWF0LWZvb3Rlci1yb3csIHRyLm1hdC1yb3cge1xuICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcbn1cblxuLm1hdC10YWJsZSB7XG4gIGJvcmRlci1zcGFjaW5nOiAwO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIGJvcmRlci1jb2xvcjogI2Y3ZjRmMDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uY2FyZC1oZWFkZXItdGl0bGUge1xuICB3aWR0aDogNTUlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgcGFkZGluZzogNHB4IDRweCAwcHggMTRweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQtdHlwZS1tYXQtbmF0aXZlLXNlbGVjdC5tYXQtZm9ybS1maWVsZC1kaXNhYmxlZCAubWF0LWZvcm0tZmllbGQtaW5maXg6OmFmdGVyLCAubWF0LWlucHV0LWVsZW1lbnQ6ZGlzYWJsZWQge1xuICBjb2xvcjogIzRkNmQ5ODtcbn1cblxuLmRpdkJveCB7XG4gIGJvcmRlci1zdHlsZTogZGFzaGVkO1xuICBib3JkZXItd2lkdGg6IDFweDtcbiAgYm9yZGVyLWNvbG9yOiAjYmFiZmJmO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uZGl2TGFiZWwge1xuICBjb2xvcjogIzRkNmQ5ODtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLmNhcmQtZm9ybS1oZWFkZXItdGl0bGUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/asset/viewregister/viewregister.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/asset/viewregister/viewregister.component.ts ***!
  \**************************************************************/
/*! exports provided: ViewRegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewRegisterComponent", function() { return ViewRegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var ViewRegisterComponent = /** @class */ (function () {
    function ViewRegisterComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.ViewRegisterList = false;
        this.ViewRegisterCreate = false;
        this.userLobId = [];
        this.userSublobId = [];
        this.users = [];
        this.lobs = [];
        this.sublobs = [];
        this.inventorytype = [];
        this.entity = [];
        this.vendor = [];
        this.displayedColumns = [];
        this.columns = [
            { field: 'Id', width: 5 }, { field: 'Code', width: 30 }, { field: 'Lob', width: 60 }, { field: 'Sublob', width: 5 }, { field: 'InventoryType', width: 5 },
            { field: 'InventoryName', width: 5 }, { field: 'Validity', width: 5 }, { field: 'RaisedBy', width: 5 }, { field: 'RaisedDate', width: 5 }, { field: 'Status', width: 5 }
        ];
        this.location = location;
    }
    ViewRegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        if (path.indexOf(";") > -1) {
            path = path.substr(0, path.indexOf(";"));
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.ViewRegisterForm = this.formBuilder.group({
            Id: [''],
            LobId: [''],
            Lob: [''],
            SublobId: [''],
            Sublob: [''],
            EntityId: [''],
            Entity: [''],
            InventoryId: [''],
            InventoryType: [''],
            InventoryName: [''],
            IPAddress: [''],
            Purpose: [''],
            ValidityFrom: [''],
            ValidityTo: [''],
            VendorId: [''],
            Vendor: [''],
            IsADUser: [],
            EmpName: [''],
            ContactNo: [''],
            TechName: [''],
            TechContactNo: [''],
            TechEmailId: [''],
            EmailId: [''],
            Reason: [''],
            Impact: [''],
            Remarks: [''],
            Dependency: [''],
            SearchName: [''],
            SearchTName: [''],
            IsTADUser: [],
            RejectReason: [''],
            StatusId: []
        });
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.userLoggedIn.UserLobs.forEach(function (element) { _this.userLobId.push(element.LobId); });
        this.userLoggedIn.UserSublobs.forEach(function (element) { _this.userSublobId.push(element.SublobId); });
        this.getAllViewRegister();
    };
    Object.defineProperty(ViewRegisterComponent.prototype, "Id", {
        get: function () { return this.ViewRegisterForm.get('Id'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Code", {
        get: function () { return this.ViewRegisterForm.get('Code'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "EntityId", {
        get: function () { return this.ViewRegisterForm.get('EntityId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "LobId", {
        get: function () { return this.ViewRegisterForm.get('LobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "InventoryId", {
        get: function () { return this.ViewRegisterForm.get('InventoryId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "SublobId", {
        get: function () { return this.ViewRegisterForm.get('SublobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Sublob", {
        get: function () { return this.ViewRegisterForm.get('Sublob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Entity", {
        get: function () { return this.ViewRegisterForm.get('Entity'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Lob", {
        get: function () { return this.ViewRegisterForm.get('Lob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "InventoryType", {
        get: function () { return this.ViewRegisterForm.get('InventoryType'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "InventoryName", {
        get: function () { return this.ViewRegisterForm.get('InventoryName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "IPAddress", {
        get: function () { return this.ViewRegisterForm.get('IPAddress'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Purpose", {
        get: function () { return this.ViewRegisterForm.get('Purpose'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "ValidityFrom", {
        get: function () { return this.ViewRegisterForm.get('ValidityFrom'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "ValidityTo", {
        get: function () { return this.ViewRegisterForm.get('ValidityTo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "VendorId", {
        get: function () { return this.ViewRegisterForm.get('VendorId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Vendor", {
        get: function () { return this.ViewRegisterForm.get('Vendor'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "ContactNo", {
        get: function () { return this.ViewRegisterForm.get('ContactNo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "EmailId", {
        get: function () { return this.ViewRegisterForm.get('EmailId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "EmpName", {
        get: function () { return this.ViewRegisterForm.get('EmpName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Consumer", {
        get: function () { return this.ViewRegisterForm.get('Consumer'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "TechContactNo", {
        get: function () { return this.ViewRegisterForm.get('TechContactNo'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "TechEmailId", {
        get: function () { return this.ViewRegisterForm.get('TechEmailId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "TechName", {
        get: function () { return this.ViewRegisterForm.get('TechName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "TechOwner", {
        get: function () { return this.ViewRegisterForm.get('TechOwner'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Reason", {
        get: function () { return this.ViewRegisterForm.get('Reason'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Remarks", {
        get: function () { return this.ViewRegisterForm.get('Remarks'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Impact", {
        get: function () { return this.ViewRegisterForm.get('Impact'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "Dependency", {
        get: function () { return this.ViewRegisterForm.get('Dependency'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "SearchName", {
        get: function () { return this.ViewRegisterForm.get('SearchName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "ADUser", {
        get: function () { return this.ViewRegisterForm.get('IsADUser'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "SearchTName", {
        get: function () { return this.ViewRegisterForm.get('SearchTName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "TADUser", {
        get: function () { return this.ViewRegisterForm.get('IsTADUser'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "RejectReason", {
        get: function () { return this.ViewRegisterForm.get('RejectReason'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewRegisterComponent.prototype, "StatusId", {
        get: function () { return this.ViewRegisterForm.get('StatusId'); },
        enumerable: true,
        configurable: true
    });
    ViewRegisterComponent.prototype.getAllViewRegister = function () {
        var _this = this;
        var filter = localStorage.getItem('Type');
        var assetType = localStorage.getItem('TypeId');
        var group = localStorage.getItem('GroupId') != null ? localStorage.getItem('GroupId') : '0';
        this.rest.getAll(this.global.getapiendpoint() + 'asset/GetAllAsset/' + this.userLoggedIn.Id + '/' + filter + '/' + assetType + '/' + group + '/' + 'V').subscribe(function (data) {
            var tableData = [];
            data.Data.forEach(function (element) {
                tableData.push({
                    Id: element.Id, Code: element.Code, Lob: element.Lob,
                    Sublob: element.Sublob, InventoryType: element.InventoryType, InventoryName: element.InventoryName,
                    Validity: element.Validity != null ? element.Validity : 'NA',
                    RaisedBy: element.RaisedBy,
                    RaisedDate: element.RaisedDate,
                    Status: element.Status,
                    StatusId: element.StatusId,
                    IsRenew: element.IsRenew
                });
            });
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](tableData);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
        this.ViewRegisterCreate = false;
        this.ViewRegisterList = true;
    };
    ViewRegisterComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    ViewRegisterComponent.prototype.backViewRegister = function () {
        this.ViewRegisterCreate = false;
        this.ViewRegisterList = true;
        this.topdiv.nativeElement.scrollIntoView({ behavior: 'smooth' });
    };
    ViewRegisterComponent.prototype.cloneRegister = function (Id) {
        var UIObj = this.global.getUIObj("/dar/assetregister");
        if (UIObj == null)
            this.toastr.showNotification('top', 'right', "User Role has no access of Menu to clone, please contact Administrator", 'danger');
        else {
            localStorage.setItem('Clone', Id);
            this.router.navigate(['/dar/assetregister']);
        }
    };
    ViewRegisterComponent.prototype.viewRegister = function (Id) {
        var _this = this;
        this.rest.getById(this.global.getapiendpoint() + 'asset/GetAssetById/', Id).subscribe(function (data) {
            _this.Id.setValue(data.Data[0].Id);
            _this.Entity.setValue(data.Data[0].Entity);
            _this.Lob.setValue(data.Data[0].Lob);
            _this.Sublob.setValue(data.Data[0].Sublob);
            _this.InventoryType.setValue(data.Data[0].InventoryType);
            _this.InventoryName.setValue(data.Data[0].InventoryName);
            _this.IPAddress.setValue(data.Data[0].IPAddress);
            _this.Vendor.setValue(data.Data[0].Vendor);
            _this.ValidityFrom.setValue(data.Data[0].ValidityFrom != null ? data.Data[0].ValidityFrom : 'NA');
            _this.ValidityTo.setValue(data.Data[0].ValidityTo != null ? data.Data[0].ValidityTo : 'NA');
            _this.Purpose.setValue(data.Data[0].Purpose);
            _this.EmpName.setValue(data.Data[0].EmpName);
            _this.EmailId.setValue(data.Data[0].EmpEmailId);
            _this.ContactNo.setValue(data.Data[0].EmpContactNumber);
            _this.TechName.setValue(data.Data[0].TechName);
            _this.TechEmailId.setValue(data.Data[0].TechEmailId);
            _this.TechContactNo.setValue(data.Data[0].TechContactNumber);
            _this.Dependency.setValue(data.Data[0].Dependency);
            _this.Impact.setValue(data.Data[0].ImpactFailure);
            _this.Reason.setValue(data.Data[0].Reason);
            _this.Remarks.setValue(data.Data[0].Remarks);
            _this.ADUser.setValue(data.Data[0].ADUser);
            _this.StatusId.setValue(data.Data[0].StatusId);
            _this.RejectReason.setValue(data.Data[0].RejectReason);
            _this.ViewRegisterCreate = true;
            _this.ViewRegisterList = false;
        });
        this.view();
    };
    ViewRegisterComponent.prototype.view = function () {
        this.Entity.disable();
        this.Lob.disable();
        this.Sublob.disable();
        this.InventoryType.disable();
        this.InventoryName.disable();
        this.IPAddress.disable();
        this.Vendor.disable();
        this.ValidityFrom.disable();
        this.ValidityTo.disable();
        this.Purpose.disable();
        this.EmpName.disable();
        this.EmailId.disable();
        this.ContactNo.disable();
        this.TechName.disable();
        this.TechEmailId.disable();
        this.TechContactNo.disable();
        this.Dependency.disable();
        this.Impact.disable();
        this.Reason.disable();
        this.Remarks.disable();
        this.ADUser.disable();
        this.RejectReason.disable();
        this.TADUser.disable();
    };
    ViewRegisterComponent.prototype.exportViewRegister = function () {
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'ViewRegister');
        this.toastr.showNotification('top', 'right', 'Exported Successfully', 'success');
    };
    ViewRegisterComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    ViewRegisterComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    ViewRegisterComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    ViewRegisterComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].B_KEY) {
            this.backViewRegister();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].X_KEY) {
            this.exportViewRegister();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ViewRegisterComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('topdiv', { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ViewRegisterComponent.prototype, "topdiv", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ViewRegisterComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ViewRegisterComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], ViewRegisterComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"])
    ], ViewRegisterComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ViewRegisterComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], ViewRegisterComponent.prototype, "keyEvent", null);
    ViewRegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-viewregister',
            template: __webpack_require__(/*! raw-loader!./viewregister.component.html */ "./node_modules/raw-loader/index.js!./src/app/asset/viewregister/viewregister.component.html"),
            styles: [__webpack_require__(/*! ./viewregister.component.scss */ "./src/app/asset/viewregister/viewregister.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExcelService"]])
    ], ViewRegisterComponent);
    return ViewRegisterComponent;
}());



/***/ })

}]);
//# sourceMappingURL=app-asset-asset-module.js.map