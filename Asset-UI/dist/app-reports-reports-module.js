(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-reports-reports-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/reports/MISReport/misReport.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reports/MISReport/misReport.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>MIS Report</h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                        <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"SearchForm\">\r\n\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"LobId\">\r\n                                <input matInput type=\"text\" formControlName=\"SublobId\">\r\n                            </mat-form-field>\r\n                            <div class=\"row\">\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Lob\" type=\"text\" formControlName=\"Lob\"\r\n                                            [matAutocomplete]=\"autoLOB\" (input)=\"inputLOB($event.target.value)\">\r\n                                        <mat-autocomplete #autoLOB=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedLOB($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let lob of filteredlobs | async\" [value]=\"lob\">\r\n                                                {{ lob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Lob.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearLOB()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Lob.hasError('incorrect')\">\r\n                                            Please select a valid Lob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Sublob\" type=\"text\" formControlName=\"Sublob\"\r\n                                            [matAutocomplete]=\"autoSublob\" (input)=\"inputSublob($event.target.value)\">\r\n                                        <mat-autocomplete #autoSublob=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedSublob($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let sublob of filteredSublobs | async\" [value]=\"sublob\">\r\n                                                {{ sublob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Sublob.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearSublob()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Sublob.hasError('incorrect')\">\r\n                                            Please select a valid Sublob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-6\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput [matDatepicker]=\"pickerFrom\" [readonly]=\"readonly\"\r\n                                                    placeholder=\"Validity From Date\" [formControl]=\"fromDate\"\r\n                                                    autocomplete=\"off\">\r\n                                                <button mat-button *ngIf=\"fromDate.value\" matSuffix mat-icon-button\r\n                                                    aria-label=\"Clear\" (click)=\"clearFromDate()\">\r\n                                                    <mat-icon>close</mat-icon>\r\n                                                </button>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\">\r\n                                                </mat-datepicker-toggle>\r\n                                                <mat-datepicker #pickerFrom></mat-datepicker>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-6\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput [matDatepicker]=\"pickerTo\" [readonly]=\"readonly\"\r\n                                                    placeholder=\"Validity To Date\" [formControl]=\"toDate\"\r\n                                                    autocomplete=\"off\">\r\n                                                <button mat-button *ngIf=\"toDate.value\" matSuffix mat-icon-button\r\n                                                    aria-label=\"Clear\" (click)=\"clearToDate()\">\r\n                                                    <mat-icon>close</mat-icon>\r\n                                                </button>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"pickerTo\">\r\n                                                </mat-datepicker-toggle>\r\n                                                <mat-datepicker #pickerTo></mat-datepicker>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\" style=\"margin-left: 5px; margin-top: 10px;\">\r\n                                <button mat-raised-button type=\"button\" matTooltip=\"Export (alt x)\"\r\n                                    class=\"btn btn-default pull-right\" (click)=\"downloadReport()\">Export</button>\r\n\r\n                                <button mat-raised-button type=\"submit\" matTooltip=\"View (alt v)\"\r\n                                    class=\"btn btn-success pull-right\" (click)=\"searchReport()\">View</button>\r\n\r\n                                <button mat-raised-button type=\"reset\" id=\"btnReset\" matTooltip=\"Reset (alt r)\"\r\n                                    class=\"btn btn-warning pull-right\" (click)=\"resetReport()\">Reset</button>\r\n\r\n                            </div>\r\n                        \r\n\r\n                        <div class=\"table-responsive\" [hidden]=\"ReportIndex\">\r\n\r\n                            <mat-form-field>\r\n                                <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\"\r\n                                    autocomplete=\"off\">\r\n                            </mat-form-field>\r\n\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n\r\n                                <ng-container matColumnDef=\"SLA\">\r\n                                    <th mat-header-cell *matHeaderCellDef style=\"width:20%;\"> SLA\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                        <button *ngIf=\"element.SLA == 'red'\" mat-raised-button type=\"button\"\r\n                                            title=\"Approval pending more than 3 days\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: red;\">lens</i>\r\n                                        </button>\r\n\r\n                                        <button *ngIf=\"element.SLA == 'green'\" mat-raised-button type=\"button\"\r\n                                            title=\"Approval pending more than 3 days\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: green;\">lens</i>\r\n                                        </button>\r\n                                    </td>\r\n                                </ng-container>\r\n\r\n                                <ng-container matColumnDef=\"Code\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width:20%;\"> Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Code}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Entity\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:30%;\"> Entity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Entity}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Lob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:30%;\"> Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Lob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"SubLob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width:20%;\"> Sub Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.SubLob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryType\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Inventory Type\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryType}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Inventory Name\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Validity\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                         <span *ngIf=\"element.SLA == 'red'\" style=\"color: red;\"> {{element.Validity}} </span>    \r\n                                         <span *ngIf=\"element.SLA == 'green'\"> {{element.Validity}} </span> \r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"IPAddress\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> IPAddress\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.IPAddress}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Purpose\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Purpose\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Purpose}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Vendor\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Vendor\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Vendor}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Consumer\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width:20%;\"> User / Consumer\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Consumer}}</td>\r\n                                </ng-container> \r\n                                <ng-container matColumnDef=\"EmailId\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> EmailId\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.EmailId}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"ContactNo\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Contact No\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.ContactNo}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Tech Owner\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechEmailId\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Email Id\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechEmailId}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechContactNo\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Contact No\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechContactNo}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Dependency\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Dependency\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Dependency}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Impact\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Impact of failure\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Impact}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Reason\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Reason for changing the Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Reason}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Remarks\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Remarks\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Remarks}}</td>\r\n                                </ng-container>\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"loading-shade\" *ngIf=\"isLoadingResults\">\r\n        <mat-spinner color=\"blue\" *ngIf=\"isLoadingResults\"></mat-spinner>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/reports/SLAReport/slaReport.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reports/SLAReport/slaReport.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>SLA Report</h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                        <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"SearchForm\">\r\n\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"LobId\">\r\n                                <input matInput type=\"text\" formControlName=\"SublobId\">\r\n                            </mat-form-field>\r\n                            <div class=\"row\">\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Lob\" type=\"text\" formControlName=\"Lob\"\r\n                                            [matAutocomplete]=\"autoLOB\" (input)=\"inputLOB($event.target.value)\">\r\n                                        <mat-autocomplete #autoLOB=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedLOB($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let lob of filteredlobs | async\" [value]=\"lob\">\r\n                                                {{ lob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Lob.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearLOB()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Lob.hasError('incorrect')\">\r\n                                            Please select a valid Lob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Sublob\" type=\"text\" formControlName=\"Sublob\"\r\n                                            [matAutocomplete]=\"autoSublob\" (input)=\"inputSublob($event.target.value)\">\r\n                                        <mat-autocomplete #autoSublob=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedSublob($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let sublob of filteredSublobs | async\" [value]=\"sublob\">\r\n                                                {{ sublob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Sublob.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearSublob()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Sublob.hasError('incorrect')\">\r\n                                            Please select a valid Sublob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-6\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput [matDatepicker]=\"pickerFrom\" [readonly]=\"readonly\"\r\n                                                    placeholder=\"Raised From Date\" [formControl]=\"fromDate\"\r\n                                                    autocomplete=\"off\">\r\n                                                <button mat-button *ngIf=\"fromDate.value\" matSuffix mat-icon-button\r\n                                                    aria-label=\"Clear\" (click)=\"clearFromDate()\">\r\n                                                    <mat-icon>close</mat-icon>\r\n                                                </button>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\">\r\n                                                </mat-datepicker-toggle>\r\n                                                <mat-datepicker #pickerFrom></mat-datepicker>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-6\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput [matDatepicker]=\"pickerTo\" [readonly]=\"readonly\"\r\n                                                    placeholder=\"Raised To Date\" [formControl]=\"toDate\"\r\n                                                    autocomplete=\"off\">\r\n                                                <button mat-button *ngIf=\"toDate.value\" matSuffix mat-icon-button\r\n                                                    aria-label=\"Clear\" (click)=\"clearToDate()\">\r\n                                                    <mat-icon>close</mat-icon>\r\n                                                </button>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"pickerTo\">\r\n                                                </mat-datepicker-toggle>\r\n                                                <mat-datepicker #pickerTo></mat-datepicker>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\" style=\"margin-left: 5px; margin-top: 10px;\">\r\n                                <button mat-raised-button type=\"button\" matTooltip=\"Export (alt x)\"\r\n                                    class=\"btn btn-default pull-right\" (click)=\"downloadReport()\">Export</button>\r\n\r\n                                <button mat-raised-button type=\"submit\" matTooltip=\"View (alt v)\"\r\n                                    class=\"btn btn-success pull-right\" (click)=\"searchReport()\">View</button>\r\n\r\n                                <button mat-raised-button type=\"reset\" id=\"btnReset\" matTooltip=\"Reset (alt r)\"\r\n                                    class=\"btn btn-warning pull-right\" (click)=\"resetReport()\">Reset</button>\r\n\r\n                            </div>\r\n                        \r\n\r\n                        <div class=\"table-responsive\" [hidden]=\"ReportIndex\">\r\n\r\n                            <mat-form-field>\r\n                                <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\"\r\n                                    autocomplete=\"off\">\r\n                            </mat-form-field>\r\n\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n\r\n                                <ng-container matColumnDef=\"SLA\">\r\n                                    <th mat-header-cell *matHeaderCellDef style=\"width:20%;\"> SLA\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                        <button *ngIf=\"element.SLA == 'red'\" mat-raised-button type=\"button\"\r\n                                            title=\"Approval pending more than 3 days\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: red;\">lens</i>\r\n                                        </button>\r\n\r\n                                        <button *ngIf=\"element.SLA == 'green'\" mat-raised-button type=\"button\"\r\n                                            title=\"Pending for approval\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: green;\">lens</i>\r\n                                        </button>\r\n                                    </td>\r\n                                </ng-container>\r\n\r\n                                <ng-container matColumnDef=\"Code\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width:20%;\"> Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Code}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Lob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:30%;\"> Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Lob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"SubLob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width:20%;\"> Sub Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.SubLob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryType\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Inventory Type\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryType}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Inventory Name\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Validity\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                        {{element.Validity}}\r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Consumer\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width:20%;\"> User / Consumer\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Consumer}}</td>\r\n                                </ng-container> \r\n                                <ng-container matColumnDef=\"EmailId\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> EmailId\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.EmailId}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"ContactNo\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Contact No\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.ContactNo}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Tech Owner\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechEmailId\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Email Id\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechEmailId}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechContactNo\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Contact No\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechContactNo}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Dependency\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Dependency\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Dependency}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Impact\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Impact of failure\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Impact}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Reason\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Reason for changing the Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Reason}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Remarks\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Remarks\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Remarks}}</td>\r\n                                </ng-container>\r\n                                \r\n                                <ng-container matColumnDef=\"RaisedDate\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> RaisedDate\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.RaisedDate}}</td>\r\n                                </ng-container>\r\n                                \r\n                                <ng-container matColumnDef=\"Status\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Status\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Status}}</td>\r\n                                </ng-container>\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"loading-shade\" *ngIf=\"isLoadingResults\">\r\n        <mat-spinner color=\"blue\" *ngIf=\"isLoadingResults\"></mat-spinner>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/reports/ValidityReport/validityReport.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reports/ValidityReport/validityReport.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-flexy-column></app-flexy-column>\r\n<div class=\"main-content\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <table style=\" margin-left: 3px;\">\r\n                    <tr>\r\n                        <td class=\"card-header-title\">\r\n                            <h4>Validity Expiry Report</h4>\r\n                        </td>\r\n                        <td style=\"width: 50%;\"></td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                        <form #form=\"ngForm\" autocomplete=\"off\" [formGroup]=\"SearchForm\">\r\n\r\n                            <mat-form-field class=\"example-full-width\" hidden>\r\n                                <input matInput type=\"text\" formControlName=\"LobId\">\r\n                                <input matInput type=\"text\" formControlName=\"SublobId\">\r\n                            </mat-form-field>\r\n                            <div class=\"row\">\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Lob\" type=\"text\" formControlName=\"Lob\"\r\n                                            [matAutocomplete]=\"autoLOB\" (input)=\"inputLOB($event.target.value)\">\r\n                                        <mat-autocomplete #autoLOB=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedLOB($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let lob of filteredlobs | async\" [value]=\"lob\">\r\n                                                {{ lob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Lob.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearLOB()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Lob.hasError('incorrect')\">\r\n                                            Please select a valid Lob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <mat-form-field class=\"example-full-width\">\r\n                                        <input matInput placeholder=\"Sublob\" type=\"text\" formControlName=\"Sublob\"\r\n                                            [matAutocomplete]=\"autoSublob\" (input)=\"inputSublob($event.target.value)\">\r\n                                        <mat-autocomplete #autoSublob=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selectedSublob($event.option.value)\"\r\n                                            [displayWith]=\"displayWith\">\r\n\r\n                                            <mat-option *ngFor=\"let sublob of filteredSublobs | async\" [value]=\"sublob\">\r\n                                                {{ sublob.Code }}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                        <button mat-button *ngIf=\"Sublob.value\" matSuffix mat-icon-button\r\n                                            aria-label=\"Clear\" (click)=\"clearSublob()\">\r\n                                            <mat-icon> close</mat-icon>\r\n                                        </button>\r\n                                        <mat-error *ngIf=\"Sublob.hasError('incorrect')\">\r\n                                            Please select a valid Sublob\r\n                                        </mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n\r\n                                <div class=\"col-md-4\">\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-6\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput [matDatepicker]=\"pickerFrom\" [readonly]=\"readonly\"\r\n                                                    placeholder=\"Validity From Date\" [formControl]=\"fromDate\"\r\n                                                    autocomplete=\"off\">\r\n                                                <button mat-button *ngIf=\"fromDate.value\" matSuffix mat-icon-button\r\n                                                    aria-label=\"Clear\" (click)=\"clearFromDate()\">\r\n                                                    <mat-icon>close</mat-icon>\r\n                                                </button>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\">\r\n                                                </mat-datepicker-toggle>\r\n                                                <mat-datepicker #pickerFrom></mat-datepicker>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-6\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput [matDatepicker]=\"pickerTo\" [readonly]=\"readonly\"\r\n                                                    placeholder=\"Validity To Date\" [formControl]=\"toDate\"\r\n                                                    autocomplete=\"off\">\r\n                                                <button mat-button *ngIf=\"toDate.value\" matSuffix mat-icon-button\r\n                                                    aria-label=\"Clear\" (click)=\"clearToDate()\">\r\n                                                    <mat-icon>close</mat-icon>\r\n                                                </button>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"pickerTo\">\r\n                                                </mat-datepicker-toggle>\r\n                                                <mat-datepicker #pickerTo></mat-datepicker>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"row\" style=\"margin-left: 5px; margin-top: 10px;\">\r\n                                <button mat-raised-button type=\"button\" matTooltip=\"Export (alt x)\"\r\n                                    class=\"btn btn-default pull-right\" (click)=\"downloadReport()\">Export</button>\r\n\r\n                                <button mat-raised-button type=\"submit\" matTooltip=\"View (alt v)\"\r\n                                    class=\"btn btn-success pull-right\" (click)=\"searchReport()\">View</button>\r\n\r\n                                <button mat-raised-button type=\"reset\" id=\"btnReset\" matTooltip=\"Reset (alt r)\"\r\n                                    class=\"btn btn-warning pull-right\" (click)=\"resetReport()\">Reset</button>\r\n\r\n                            </div>\r\n                        \r\n\r\n                        <div class=\"table-responsive\" [hidden]=\"ReportIndex\">\r\n\r\n                            <mat-form-field>\r\n                                <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\"\r\n                                    autocomplete=\"off\">\r\n                            </mat-form-field>\r\n\r\n                            <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-table\" cdkDropListGroup>\r\n\r\n                                <ng-container matColumnDef=\"SLA\">\r\n                                    <th mat-header-cell *matHeaderCellDef style=\"width:20%;\"> SLA\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                        <button *ngIf=\"element.SLA == 'red'\" mat-raised-button type=\"button\"\r\n                                            title=\"Approval pending more than 3 days\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: red;\">lens</i>\r\n                                        </button>\r\n\r\n                                        <button *ngIf=\"element.SLA == 'green'\" mat-raised-button type=\"button\"\r\n                                            title=\"Approval pending more than 3 days\" (click)=\"viewRegister(element.Id)\"\r\n                                            class=\"btn btn-primary btn-link btn-sm btn-just-icon\">\r\n                                            <i class=\"material-icons\" style=\"color: green;\">lens</i>\r\n                                        </button>\r\n                                    </td>\r\n                                </ng-container>\r\n\r\n                                <ng-container matColumnDef=\"Code\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 1)\" style=\"width:20%;\"> Code\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Code}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Lob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 3)\" style=\"width:30%;\"> Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Lob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"SubLob\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 4)\" style=\"width:20%;\"> Sub Lob\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.SubLob}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryType\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Inventory Type\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryType}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"InventoryName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Inventory Name\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.InventoryName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Validity\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">\r\n                                         <span *ngIf=\"element.SLA == 'red'\" style=\"color: red;\"> {{element.Validity}} </span>    \r\n                                         <span *ngIf=\"element.SLA == 'green'\"> {{element.Validity}} </span> \r\n                                    </td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Consumer\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 2)\" style=\"width:20%;\"> User / Consumer\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Consumer}}</td>\r\n                                </ng-container> \r\n                                <ng-container matColumnDef=\"EmailId\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> EmailId\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.EmailId}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"ContactNo\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Contact No\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.ContactNo}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechName\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Tech Owner\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechName}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechEmailId\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Email Id\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechEmailId}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"TechContactNo\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Contact No\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.TechContactNo}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Dependency\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Dependency\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Dependency}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Impact\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Impact of failure\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Impact}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Reason\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Reason for changing the Validity\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Reason}}</td>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"Remarks\">\r\n                                    <th mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                        (mousedown)=\"onResizeColumn($event, 5)\" style=\"width:20%;\"> Remarks\r\n                                    </th>\r\n                                    <td mat-cell *matCellDef=\"let element\">{{element.Remarks}}</td>\r\n                                </ng-container>\r\n                                <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                            </mat-table>\r\n                            <mat-paginator [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n                        </div>\r\n                    </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"loading-shade\" *ngIf=\"isLoadingResults\">\r\n        <mat-spinner color=\"blue\" *ngIf=\"isLoadingResults\"></mat-spinner>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/reports/reports.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reports/reports.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/reports/MISReport/MISReport.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/reports/MISReport/MISReport.component.ts ***!
  \**********************************************************/
/*! exports provided: MISReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MISReportComponent", function() { return MISReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MISReportComponent = /** @class */ (function () {
    function MISReportComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.ReportIndex = true;
        this.displayedColumns = [];
        this.columns = [
            { field: 'Code', width: 1 }, { field: 'Entity', width: 1 }, { field: 'Lob', width: 1 },
            { field: 'SubLob', width: 1 }, { field: 'InventoryType', width: 1 }, { field: 'InventoryName', width: 1 },
            { field: 'Validity', width: 1 }, { field: 'IPAddress', width: 1 }, { field: 'Purpose', width: 1 },
            { field: 'Vendor', width: 1 },
            { field: 'Consumer', width: 1 }, { field: 'EmailId', width: 1 },
            { field: 'ContactNo', width: 1 }, { field: 'TechName', width: 1 }, { field: 'TechEmailId', width: 1 },
            { field: 'TechContactNo', width: 1 }, { field: 'Dependency', width: 1 }, { field: 'Impact', width: 1 },
            { field: 'Reason', width: 1 }, { field: 'Remarks', width: 1 }
        ];
        this.lobs = [];
        this.sublobs = [];
        this.isLoadingResults = false;
        this.location = location;
    }
    MISReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.SearchForm = this.formBuilder.group({
            LobId: [],
            Lob: [''],
            SublobId: [],
            Sublob: [''],
            fromDate: [],
            toDate: []
        });
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.getAllLOB();
        this.filteredlobs = this.Lob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.lobs) : _this.lobs.slice(); }));
        this.getAllSublob();
        this.filteredlobs = this.Sublob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.sublobs) : _this.sublobs.slice(); }));
    };
    Object.defineProperty(MISReportComponent.prototype, "LobId", {
        get: function () { return this.SearchForm.get('LobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MISReportComponent.prototype, "SublobId", {
        get: function () { return this.SearchForm.get('SublobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MISReportComponent.prototype, "Lob", {
        get: function () { return this.SearchForm.get('Lob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MISReportComponent.prototype, "Sublob", {
        get: function () { return this.SearchForm.get('Sublob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MISReportComponent.prototype, "fromDate", {
        get: function () { return this.SearchForm.get('fromDate'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MISReportComponent.prototype, "toDate", {
        get: function () { return this.SearchForm.get('toDate'); },
        enumerable: true,
        configurable: true
    });
    MISReportComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    MISReportComponent.prototype.clearToDate = function () {
        this.toDate.setValue(null);
    };
    MISReportComponent.prototype.clearFromDate = function () {
        this.fromDate.setValue(null);
    };
    MISReportComponent.prototype.getAllMISReport = function (filter) {
        var _this = this;
        this.isLoadingResults = true;
        var LobId = this.LobId.value ? this.LobId.value : 0;
        var SubLOBId = this.SublobId.value ? this.SublobId.value : 0;
        var fromDate = this.fromDate.value ? this.fromDate.value : null;
        var toDate = this.toDate.value ? this.toDate.value : null;
        this.rest.getAll(this.global.getapiendpoint() + 'report/GetMISReport/' + this.userLoggedIn.Id + '/' + LobId + '/' + SubLOBId +
            '/' + fromDate + '/' + toDate).subscribe(function (data) {
            if (data.Data != null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](data.Data);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
                _this.isLoadingResults = false;
                if (filter == '1')
                    _this.ReportIndex = false;
            }
            _this.isLoadingResults = false;
        });
    };
    MISReportComponent.prototype.searchReport = function () {
        this.getAllMISReport("1");
    };
    MISReportComponent.prototype.downloadReport = function () {
        if (this.dataSource == null)
            this.getAllMISReport("2");
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'MISReport');
        this.toastr.showNotification('top', 'right', 'Report Downloaded Successfully', 'success');
    };
    MISReportComponent.prototype.displayWith = function (obj) {
        return obj ? obj.Code : undefined;
    };
    MISReportComponent.prototype.getAllLOB = function () {
        var _this = this;
        this.lobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "lob/GetAllActiveLOB").subscribe(function (data) {
            _this.lobs = data.Data;
        });
    };
    MISReportComponent.prototype.inputLOB = function (lob) {
        this.LobId.setValue("");
    };
    MISReportComponent.prototype.selectedLOB = function (lob) {
        this.LobId.setValue(lob.Id);
    };
    MISReportComponent.prototype.clearLOB = function () {
        this.LobId.setValue(null);
        this.Lob.setValue(null);
    };
    MISReportComponent.prototype.getAllSublob = function () {
        var _this = this;
        this.sublobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "sublob/GetAllActiveSublob").subscribe(function (data) {
            _this.sublobs = data.Data;
        });
    };
    MISReportComponent.prototype.inputSublob = function (sublob) {
        this.SublobId.setValue("");
    };
    MISReportComponent.prototype.selectedSublob = function (sublob) {
        this.SublobId.setValue(sublob.Id);
    };
    MISReportComponent.prototype.clearSublob = function () {
        this.SublobId.setValue(null);
        this.Sublob.setValue(null);
    };
    MISReportComponent.prototype._filter = function (value, obj) {
        var filterValue = (value ? (value.Code ? value.Code.toLowerCase() : value.toLowerCase()) : "");
        return obj.filter(function (o) { return o.Code.toLowerCase().includes(filterValue); });
    };
    MISReportComponent.prototype.resetReport = function () {
        this.form.resetForm();
        this.SearchForm.markAsUntouched();
        this.LobId.setValue("");
        this.SublobId.setValue("");
        this.ReportIndex = true;
        this.dataSource = null;
    };
    MISReportComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    MISReportComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    MISReportComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    MISReportComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].R_KEY) {
            this.resetReport();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].V_KEY) {
            this.searchReport();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].X_KEY) {
            this.downloadReport();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], MISReportComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"])
    ], MISReportComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], MISReportComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], MISReportComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], MISReportComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], MISReportComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], MISReportComponent.prototype, "keyEvent", null);
    MISReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-misReport.component',
            template: __webpack_require__(/*! raw-loader!./misReport.component.html */ "./node_modules/raw-loader/index.js!./src/app/reports/MISReport/misReport.component.html"),
            styles: [__webpack_require__(/*! ./misReport.component.scss */ "./src/app/reports/MISReport/misReport.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExcelService"]])
    ], MISReportComponent);
    return MISReportComponent;
}());



/***/ }),

/***/ "./src/app/reports/MISReport/misReport.component.scss":
/*!************************************************************!*\
  !*** ./src/app/reports/MISReport/misReport.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.loading-shade {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: -30%;\n  right: 0;\n  background: rgba(46, 42, 42, 0.15);\n  z-index: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #589092;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 0px;\n  width: 100%;\n  padding: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVwb3J0cy9NSVNSZXBvcnQvRTpcXERBUiBVQVQgRmlsZXNcXEFzc2V0LVVJL3NyY1xcYXBwXFxyZXBvcnRzXFxNSVNSZXBvcnRcXG1pc1JlcG9ydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcmVwb3J0cy9NSVNSZXBvcnQvbWlzUmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLGtDQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDQ0o7O0FERUE7RUFDSSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSw2QkFBQTtFQUNBLHNCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FDQ0o7O0FERUE7O0VBRUksbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1DQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3JlcG9ydHMvTUlTUmVwb3J0L21pc1JlcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xyXG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG50YWJsZXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubG9hZGluZy1zaGFkZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgYm90dG9tOiAtMzAlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoNDYsIDQyLCA0MiwgMC4xNSk7IFxyXG4gICAgei1pbmRleDogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7IFxyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyAgXHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGx7XHJcbiAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDsgXHJcbiAgICBjb2xvcjogIzU4OTA5MjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBjdXJzb3I6IGNvbC1yZXNpemU7XHJcbiAgICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufVxyXG5cclxudHIubWF0LWhlYWRlci1yb3cge1xyXG4gICAgaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDsgXHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG50ci5tYXQtZm9vdGVyLXJvdywgdHIubWF0LXJvdyB7XHJcbiAgICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcclxufVxyXG4gIFxyXG4ubWF0LXRhYmxle1xyXG4gICAgYm9yZGVyLXNwYWNpbmc6IDA7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZjdmNGYwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsICBcclxuLm1hdC1jZWxsLCAubWF0LWZvb3Rlci1jZWxsIHtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDEycHg7IFxyXG59XHJcblxyXG4uY2FyZC1oZWFkZXItdGl0bGV7XHJcbiAgICB3aWR0aDogNTUlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbGluZS1oZWlnaHQ6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNzIsIDEzMCwgMTk3KTtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1zdHlsZTogcmlkZ2U7XHJcbiAgICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcclxuICAgIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMzsgXHJcbiAgICBwYWRkaW5nOiA0cHggNHB4IDBweCAxNHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiA4cHg7XHJcbn0iLCJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG50YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubG9hZGluZy1zaGFkZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IC0zMCU7XG4gIHJpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDQ2LCA0MiwgNDIsIDAuMTUpO1xuICB6LWluZGV4OiAxO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLm1hdC1oZWFkZXItY2VsbCB7XG4gIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzU4OTA5MjtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY3Vyc29yOiBjb2wtcmVzaXplO1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufVxuXG50ci5tYXQtaGVhZGVyLXJvdyB7XG4gIGhlaWdodDogNDBweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxudHIubWF0LWZvb3Rlci1yb3csIHRyLm1hdC1yb3cge1xuICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcbn1cblxuLm1hdC10YWJsZSB7XG4gIGJvcmRlci1zcGFjaW5nOiAwO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIGJvcmRlci1jb2xvcjogI2Y3ZjRmMDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uY2FyZC1oZWFkZXItdGl0bGUge1xuICB3aWR0aDogNTUlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgcGFkZGluZzogNHB4IDRweCAwcHggMTRweDtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogOHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/reports/SLAReport/SLAReport.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/reports/SLAReport/SLAReport.component.ts ***!
  \**********************************************************/
/*! exports provided: SLAReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SLAReportComponent", function() { return SLAReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var SLAReportComponent = /** @class */ (function () {
    function SLAReportComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.ReportIndex = true;
        this.displayedColumns = [];
        this.columns = [
            { field: 'SLA', width: 1 }, { field: 'Code', width: 1 }, { field: 'Lob', width: 1 },
            { field: 'SubLob', width: 1 }, { field: 'InventoryType', width: 1 }, { field: 'InventoryName', width: 1 },
            { field: 'Validity', width: 1 }, { field: 'Consumer', width: 1 }, { field: 'EmailId', width: 1 },
            { field: 'ContactNo', width: 1 }, { field: 'TechName', width: 1 }, { field: 'TechEmailId', width: 1 },
            { field: 'TechContactNo', width: 1 }, { field: 'Dependency', width: 1 }, { field: 'Impact', width: 1 },
            { field: 'Reason', width: 1 }, { field: 'Remarks', width: 1 }, { field: 'RaisedDate', width: 1 }, { field: 'Status', width: 1 }
        ];
        this.lobs = [];
        this.sublobs = [];
        this.isLoadingResults = false;
        this.location = location;
    }
    SLAReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.SearchForm = this.formBuilder.group({
            LobId: [],
            Lob: [''],
            SublobId: [],
            Sublob: [''],
            fromDate: [],
            toDate: []
        });
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.getAllLOB();
        this.filteredlobs = this.Lob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.lobs) : _this.lobs.slice(); }));
        this.getAllSublob();
        this.filteredlobs = this.Sublob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.sublobs) : _this.sublobs.slice(); }));
    };
    Object.defineProperty(SLAReportComponent.prototype, "LobId", {
        get: function () { return this.SearchForm.get('LobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SLAReportComponent.prototype, "SublobId", {
        get: function () { return this.SearchForm.get('SublobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SLAReportComponent.prototype, "Lob", {
        get: function () { return this.SearchForm.get('Lob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SLAReportComponent.prototype, "Sublob", {
        get: function () { return this.SearchForm.get('Sublob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SLAReportComponent.prototype, "fromDate", {
        get: function () { return this.SearchForm.get('fromDate'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SLAReportComponent.prototype, "toDate", {
        get: function () { return this.SearchForm.get('toDate'); },
        enumerable: true,
        configurable: true
    });
    SLAReportComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    SLAReportComponent.prototype.clearToDate = function () {
        this.toDate.setValue(null);
    };
    SLAReportComponent.prototype.clearFromDate = function () {
        this.fromDate.setValue(null);
    };
    SLAReportComponent.prototype.getAllSLAReport = function (filter) {
        var _this = this;
        this.isLoadingResults = true;
        var LobId = this.LobId.value ? this.LobId.value : 0;
        var SubLOBId = this.SublobId.value ? this.SublobId.value : 0;
        var fromDate = this.fromDate.value ? this.fromDate.value : null;
        var toDate = this.toDate.value ? this.toDate.value : null;
        this.rest.getAll(this.global.getapiendpoint() + 'report/GetSLAReport/' + this.userLoggedIn.Id + '/' + LobId + '/' + SubLOBId +
            '/' + fromDate + '/' + toDate).subscribe(function (data) {
            if (data.Data != null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](data.Data);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
                _this.isLoadingResults = false;
                if (filter == '1')
                    _this.ReportIndex = false;
            }
            _this.isLoadingResults = false;
        });
    };
    SLAReportComponent.prototype.searchReport = function () {
        this.getAllSLAReport("1");
    };
    SLAReportComponent.prototype.downloadReport = function () {
        if (this.dataSource == null)
            this.getAllSLAReport("2");
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'SLAReport');
        this.toastr.showNotification('top', 'right', 'Report Downloaded Successfully', 'success');
    };
    SLAReportComponent.prototype.displayWith = function (obj) {
        return obj ? obj.Code : undefined;
    };
    SLAReportComponent.prototype.getAllLOB = function () {
        var _this = this;
        this.lobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "lob/GetAllActiveLOB").subscribe(function (data) {
            _this.lobs = data.Data;
        });
    };
    SLAReportComponent.prototype.inputLOB = function (lob) {
        this.LobId.setValue("");
    };
    SLAReportComponent.prototype.selectedLOB = function (lob) {
        this.LobId.setValue(lob.Id);
    };
    SLAReportComponent.prototype.clearLOB = function () {
        this.LobId.setValue(null);
        this.Lob.setValue(null);
    };
    SLAReportComponent.prototype.getAllSublob = function () {
        var _this = this;
        this.sublobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "sublob/GetAllActiveSublob").subscribe(function (data) {
            _this.sublobs = data.Data;
        });
    };
    SLAReportComponent.prototype.inputSublob = function (sublob) {
        this.SublobId.setValue("");
    };
    SLAReportComponent.prototype.selectedSublob = function (sublob) {
        this.SublobId.setValue(sublob.Id);
    };
    SLAReportComponent.prototype.clearSublob = function () {
        this.SublobId.setValue(null);
        this.Sublob.setValue(null);
    };
    SLAReportComponent.prototype._filter = function (value, obj) {
        var filterValue = (value ? (value.Code ? value.Code.toLowerCase() : value.toLowerCase()) : "");
        return obj.filter(function (o) { return o.Code.toLowerCase().includes(filterValue); });
    };
    SLAReportComponent.prototype.resetReport = function () {
        this.form.resetForm();
        this.SearchForm.markAsUntouched();
        this.LobId.setValue("");
        this.SublobId.setValue("");
        this.ReportIndex = true;
        this.dataSource = null;
    };
    SLAReportComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    SLAReportComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    SLAReportComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    SLAReportComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].R_KEY) {
            this.resetReport();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].V_KEY) {
            this.searchReport();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].X_KEY) {
            this.downloadReport();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SLAReportComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"])
    ], SLAReportComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], SLAReportComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], SLAReportComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], SLAReportComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], SLAReportComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], SLAReportComponent.prototype, "keyEvent", null);
    SLAReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-slaReport.component',
            template: __webpack_require__(/*! raw-loader!./slaReport.component.html */ "./node_modules/raw-loader/index.js!./src/app/reports/SLAReport/slaReport.component.html"),
            styles: [__webpack_require__(/*! ./slaReport.component.scss */ "./src/app/reports/SLAReport/slaReport.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExcelService"]])
    ], SLAReportComponent);
    return SLAReportComponent;
}());



/***/ }),

/***/ "./src/app/reports/SLAReport/slaReport.component.scss":
/*!************************************************************!*\
  !*** ./src/app/reports/SLAReport/slaReport.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.loading-shade {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: -30%;\n  right: 0;\n  background: rgba(46, 42, 42, 0.15);\n  z-index: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #589092;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 0px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVwb3J0cy9TTEFSZXBvcnQvRTpcXERBUiBVQVQgRmlsZXNcXEFzc2V0LVVJL3NyY1xcYXBwXFxyZXBvcnRzXFxTTEFSZXBvcnRcXHNsYVJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcmVwb3J0cy9TTEFSZXBvcnQvc2xhUmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLGtDQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDQ0o7O0FERUE7RUFDSSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSw2QkFBQTtFQUNBLHNCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FDQ0o7O0FERUE7O0VBRUksbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1DQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcmVwb3J0cy9TTEFSZXBvcnQvc2xhUmVwb3J0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LWZvcm0tZmllbGRbaGlkZGVuXSB7XHJcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbnRhYmxle1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5sb2FkaW5nLXNoYWRlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBib3R0b206IC0zMCU7XHJcbiAgICByaWdodDogMDtcclxuICAgIGJhY2tncm91bmQ6IHJnYig0NiwgNDIsIDQyLCAwLjE1KTsgXHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgZGlzcGxheTogZmxleDsgXHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7ICBcclxufVxyXG5cclxuLm1hdC1oZWFkZXItY2VsbHtcclxuICAgIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50OyBcclxuICAgIGNvbG9yOiAjNTg5MDkyO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGN1cnNvcjogY29sLXJlc2l6ZTtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcblxyXG50ci5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50OyBcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcclxuICAgIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xyXG59XHJcbiAgXHJcbi5tYXQtdGFibGV7XHJcbiAgICBib3JkZXItc3BhY2luZzogMDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCwgIFxyXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xyXG4gICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItY29sb3I6ICNlYWVlZWY7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDsgXHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlci10aXRsZXtcclxuICAgIHdpZHRoOiA1NSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYig3MiwgMTMwLCAxOTcpO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xyXG4gICAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzOyBcclxuICAgIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgICBcclxufSIsIm1hdC1mb3JtLWZpZWxkW2hpZGRlbl0ge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5sb2FkaW5nLXNoYWRlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogLTMwJTtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6IHJnYmEoNDYsIDQyLCA0MiwgMC4xNSk7XG4gIHotaW5kZXg6IDE7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubWF0LWhlYWRlci1jZWxsIHtcbiAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjNTg5MDkyO1xuICBmb250LXdlaWdodDogNDAwO1xuICBjdXJzb3I6IGNvbC1yZXNpemU7XG4gIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICNlYWVlZWY7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG5cbnRyLm1hdC1oZWFkZXItcm93IHtcbiAgaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG50ci5tYXQtZm9vdGVyLXJvdywgdHIubWF0LXJvdyB7XG4gIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xufVxuXG4ubWF0LXRhYmxlIHtcbiAgYm9yZGVyLXNwYWNpbmc6IDA7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgYm9yZGVyLWNvbG9yOiAjZjdmNGYwO1xuICB3aWR0aDogMTAwJTtcbn1cblxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCxcbi5tYXQtY2VsbCwgLm1hdC1mb290ZXItY2VsbCB7XG4gIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICNlYWVlZWY7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5jYXJkLWhlYWRlci10aXRsZSB7XG4gIHdpZHRoOiA1NSU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbGluZS1oZWlnaHQ6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0ODgyYzU7XG4gIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcbiAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzO1xuICBwYWRkaW5nOiA0cHggNHB4IDBweCAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG4gIHdpZHRoOiAxMDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/reports/ValidityReport/ValidityReport.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/reports/ValidityReport/ValidityReport.component.ts ***!
  \********************************************************************/
/*! exports provided: ValidityReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidityReportComponent", function() { return ValidityReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_common_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/common/toastr */ "./src/app/common/toastr.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/common/excel.service */ "./src/app/common/excel.service.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/flexy-column.component */ "./src/app/common/flexy-column.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var ValidityReportComponent = /** @class */ (function () {
    function ValidityReportComponent(location, formBuilder, rest, route, router, global, toastr, dialog, excelService) {
        this.formBuilder = formBuilder;
        this.rest = rest;
        this.route = route;
        this.router = router;
        this.global = global;
        this.toastr = toastr;
        this.dialog = dialog;
        this.excelService = excelService;
        this.ReportIndex = true;
        this.displayedColumns = [];
        this.columns = [
            { field: 'SLA', width: 1 }, { field: 'Code', width: 1 }, { field: 'Lob', width: 1 },
            { field: 'SubLob', width: 1 }, { field: 'InventoryType', width: 1 }, { field: 'InventoryName', width: 1 },
            { field: 'Validity', width: 1 }, { field: 'Consumer', width: 1 }, { field: 'EmailId', width: 1 },
            { field: 'ContactNo', width: 1 }, { field: 'TechName', width: 1 }, { field: 'TechEmailId', width: 1 },
            { field: 'TechContactNo', width: 1 }, { field: 'Dependency', width: 1 }, { field: 'Impact', width: 1 },
            { field: 'Reason', width: 1 }, { field: 'Remarks', width: 1 }
        ];
        this.lobs = [];
        this.sublobs = [];
        this.isLoadingResults = false;
        this.location = location;
    }
    ValidityReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.displayedColumns = this.flexyColumn.setDisplayedColumns(this.columns);
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.SearchForm = this.formBuilder.group({
            LobId: [],
            Lob: [''],
            SublobId: [],
            Sublob: [''],
            fromDate: [],
            toDate: []
        });
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.getAllLOB();
        this.filteredlobs = this.Lob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.lobs) : _this.lobs.slice(); }));
        this.getAllSublob();
        this.filteredlobs = this.Sublob.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["map"])(function (value) { return value ? _this._filter(value, _this.sublobs) : _this.sublobs.slice(); }));
    };
    Object.defineProperty(ValidityReportComponent.prototype, "LobId", {
        get: function () { return this.SearchForm.get('LobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ValidityReportComponent.prototype, "SublobId", {
        get: function () { return this.SearchForm.get('SublobId'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ValidityReportComponent.prototype, "Lob", {
        get: function () { return this.SearchForm.get('Lob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ValidityReportComponent.prototype, "Sublob", {
        get: function () { return this.SearchForm.get('Sublob'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ValidityReportComponent.prototype, "fromDate", {
        get: function () { return this.SearchForm.get('fromDate'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ValidityReportComponent.prototype, "toDate", {
        get: function () { return this.SearchForm.get('toDate'); },
        enumerable: true,
        configurable: true
    });
    ValidityReportComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    ValidityReportComponent.prototype.clearToDate = function () {
        this.toDate.setValue(null);
    };
    ValidityReportComponent.prototype.clearFromDate = function () {
        this.fromDate.setValue(null);
    };
    ValidityReportComponent.prototype.getAllValidityReport = function (filter) {
        var _this = this;
        this.isLoadingResults = true;
        var LobId = this.LobId.value ? this.LobId.value : 0;
        var SubLOBId = this.SublobId.value ? this.SublobId.value : 0;
        var fromDate = this.fromDate.value ? this.fromDate.value : null;
        var toDate = this.toDate.value ? this.toDate.value : null;
        this.rest.getAll(this.global.getapiendpoint() + 'report/GetValidityReport/' + this.userLoggedIn.Id + '/' + LobId + '/' + SubLOBId +
            '/' + fromDate + '/' + toDate).subscribe(function (data) {
            if (data.Data != null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](data.Data);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
                _this.isLoadingResults = false;
                if (filter == '1')
                    _this.ReportIndex = false;
            }
            _this.isLoadingResults = false;
        });
    };
    ValidityReportComponent.prototype.searchReport = function () {
        this.getAllValidityReport("1");
    };
    ValidityReportComponent.prototype.downloadReport = function () {
        if (this.dataSource == null)
            this.getAllValidityReport("2");
        this.excelService.exportASExcelFile(this.dataSource.filteredData, 'ValidityReport');
        this.toastr.showNotification('top', 'right', 'Report Downloaded Successfully', 'success');
    };
    ValidityReportComponent.prototype.displayWith = function (obj) {
        return obj ? obj.Code : undefined;
    };
    ValidityReportComponent.prototype.getAllLOB = function () {
        var _this = this;
        this.lobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "lob/GetAllActiveLOB").subscribe(function (data) {
            _this.lobs = data.Data;
        });
    };
    ValidityReportComponent.prototype.inputLOB = function (lob) {
        this.LobId.setValue("");
    };
    ValidityReportComponent.prototype.selectedLOB = function (lob) {
        this.LobId.setValue(lob.Id);
    };
    ValidityReportComponent.prototype.clearLOB = function () {
        this.LobId.setValue(null);
        this.Lob.setValue(null);
    };
    ValidityReportComponent.prototype.getAllSublob = function () {
        var _this = this;
        this.sublobs = [];
        this.rest.getAll(this.global.getapiendpoint() + "sublob/GetAllActiveSublob").subscribe(function (data) {
            _this.sublobs = data.Data;
        });
    };
    ValidityReportComponent.prototype.inputSublob = function (sublob) {
        this.SublobId.setValue("");
    };
    ValidityReportComponent.prototype.selectedSublob = function (sublob) {
        this.SublobId.setValue(sublob.Id);
    };
    ValidityReportComponent.prototype.clearSublob = function () {
        this.SublobId.setValue(null);
        this.Sublob.setValue(null);
    };
    ValidityReportComponent.prototype._filter = function (value, obj) {
        var filterValue = (value ? (value.Code ? value.Code.toLowerCase() : value.toLowerCase()) : "");
        return obj.filter(function (o) { return o.Code.toLowerCase().includes(filterValue); });
    };
    ValidityReportComponent.prototype.resetReport = function () {
        this.form.resetForm();
        this.SearchForm.markAsUntouched();
        this.LobId.setValue("");
        this.SublobId.setValue("");
        this.ReportIndex = true;
        this.dataSource = null;
    };
    ValidityReportComponent.prototype.ngAfterViewInit = function () {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    ValidityReportComponent.prototype.onResizeColumn = function (event, index) {
        this.flexyColumn.onResizeColumn(event, index, this.matTableRef);
    };
    ValidityReportComponent.prototype.onResize = function (event) {
        this.flexyColumn.setTableResize(this.matTableRef.nativeElement.clientWidth, this.columns);
    };
    ValidityReportComponent.prototype.keyEvent = function (event) {
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].R_KEY) {
            this.resetReport();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].V_KEY) {
            this.searchReport();
        }
        if (event.altKey && event.keyCode == app_common_constant__WEBPACK_IMPORTED_MODULE_9__["KEY_CODE"].X_KEY) {
            this.downloadReport();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTable"], { static: true, read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ValidityReportComponent.prototype, "matTableRef", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"], { static: true }),
        __metadata("design:type", app_common_flexy_column_component__WEBPACK_IMPORTED_MODULE_10__["FlexyColumnComponent"])
    ], ValidityReportComponent.prototype, "flexyColumn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ValidityReportComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"], { static: true }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ValidityReportComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form', { static: true }),
        __metadata("design:type", Object)
    ], ValidityReportComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ValidityReportComponent.prototype, "onResize", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], ValidityReportComponent.prototype, "keyEvent", null);
    ValidityReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-validityReport.component',
            template: __webpack_require__(/*! raw-loader!./validityReport.component.html */ "./node_modules/raw-loader/index.js!./src/app/reports/ValidityReport/validityReport.component.html"),
            styles: [__webpack_require__(/*! ./validityReport.component.scss */ "./src/app/reports/ValidityReport/validityReport.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], app_common_toastr__WEBPACK_IMPORTED_MODULE_6__["Toastr"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], app_common_excel_service__WEBPACK_IMPORTED_MODULE_8__["ExcelService"]])
    ], ValidityReportComponent);
    return ValidityReportComponent;
}());



/***/ }),

/***/ "./src/app/reports/ValidityReport/validityReport.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/reports/ValidityReport/validityReport.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.loading-shade {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: -30%;\n  right: 0;\n  background: rgba(46, 42, 42, 0.15);\n  z-index: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #589092;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 0px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVwb3J0cy9WYWxpZGl0eVJlcG9ydC9FOlxcREFSIFVBVCBGaWxlc1xcQXNzZXQtVUkvc3JjXFxhcHBcXHJlcG9ydHNcXFZhbGlkaXR5UmVwb3J0XFx2YWxpZGl0eVJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcmVwb3J0cy9WYWxpZGl0eVJlcG9ydC92YWxpZGl0eVJlcG9ydC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHdCQUFBO0FDQ0o7O0FERUE7RUFDSSxXQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFDQSxrQ0FBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksMEJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBQ0NKOztBREVBOztFQUVJLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQ0FBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLHdDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3JlcG9ydHMvVmFsaWRpdHlSZXBvcnQvdmFsaWRpdHlSZXBvcnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxudGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmxvYWRpbmctc2hhZGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGJvdHRvbTogLTMwJTtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDQ2LCA0MiwgNDIsIDAuMTUpOyBcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4OyBcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgIFxyXG59XHJcblxyXG4ubWF0LWhlYWRlci1jZWxse1xyXG4gICAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7IFxyXG4gICAgY29sb3I6ICM1ODkwOTI7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY3Vyc29yOiBjb2wtcmVzaXplO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItY29sb3I6ICNlYWVlZWY7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbn1cclxuXHJcbnRyLm1hdC1oZWFkZXItcm93IHtcclxuICAgIGhlaWdodDogNDBweCAhaW1wb3J0YW50O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7IFxyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG5cclxudHIubWF0LWZvb3Rlci1yb3csIHRyLm1hdC1yb3cge1xyXG4gICAgaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuICBcclxuLm1hdC10YWJsZXtcclxuICAgIGJvcmRlci1zcGFjaW5nOiAwO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIGJvcmRlci1jb2xvcjogI2Y3ZjRmMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG50ZC5tYXQtY2VsbCwgdGQubWF0LWZvb3Rlci1jZWxsLCAgXHJcbi5tYXQtY2VsbCwgLm1hdC1mb290ZXItY2VsbCB7XHJcbiAgICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1jb2xvcjogI2VhZWVlZjtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcCAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAxMnB4OyBcclxufVxyXG5cclxuLmNhcmQtaGVhZGVyLXRpdGxle1xyXG4gICAgd2lkdGg6IDU1JTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDcyLCAxMzAsIDE5Nyk7XHJcbiAgICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmlnaHQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XHJcbiAgICBib3JkZXItcmlnaHQtY29sb3I6ICNmZGFiMzM7IFxyXG4gICAgcGFkZGluZzogNHB4IDRweCAwcHggMTRweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbn0iLCJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG50YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubG9hZGluZy1zaGFkZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IC0zMCU7XG4gIHJpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDQ2LCA0MiwgNDIsIDAuMTUpO1xuICB6LWluZGV4OiAxO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLm1hdC1oZWFkZXItY2VsbCB7XG4gIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzU4OTA5MjtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY3Vyc29yOiBjb2wtcmVzaXplO1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufVxuXG50ci5tYXQtaGVhZGVyLXJvdyB7XG4gIGhlaWdodDogNDBweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxudHIubWF0LWZvb3Rlci1yb3csIHRyLm1hdC1yb3cge1xuICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcbn1cblxuLm1hdC10YWJsZSB7XG4gIGJvcmRlci1zcGFjaW5nOiAwO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIGJvcmRlci1jb2xvcjogI2Y3ZjRmMDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnRkLm1hdC1jZWxsLCB0ZC5tYXQtZm9vdGVyLWNlbGwsXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xuICBib3JkZXItcmlnaHQtd2lkdGg6IHRoaW4gIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItd2lkdGg6IHRoaW47XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uY2FyZC1oZWFkZXItdGl0bGUge1xuICB3aWR0aDogNTUlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg4MmM1O1xuICBmb250LWZhbWlseTogXCJUaXRpbGxpdW0gV2ViXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yaWdodC1zdHlsZTogcmlkZ2U7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICNmZGFiMzM7XG4gIGJvcmRlci1yaWdodC1jb2xvcjogI2ZkYWIzMztcbiAgcGFkZGluZzogNHB4IDRweCAwcHggMTRweDtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/reports/reports-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/reports/reports-routing.module.ts ***!
  \***************************************************/
/*! exports provided: ReportsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsRoutingModule", function() { return ReportsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/common/auth.guard */ "./src/app/common/auth.guard.ts");
/* harmony import */ var _reports_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reports.component */ "./src/app/reports/reports.component.ts");
/* harmony import */ var _SLAReport_SLAReport_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./SLAReport/SLAReport.component */ "./src/app/reports/SLAReport/SLAReport.component.ts");
/* harmony import */ var _ValidityReport_ValidityReport_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ValidityReport/ValidityReport.component */ "./src/app/reports/ValidityReport/ValidityReport.component.ts");
/* harmony import */ var _MISReport_MISReport_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./MISReport/MISReport.component */ "./src/app/reports/MISReport/MISReport.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '', component: _reports_component__WEBPACK_IMPORTED_MODULE_3__["ReportsComponent"], children: [
            { path: 'slareport', component: _SLAReport_SLAReport_component__WEBPACK_IMPORTED_MODULE_4__["SLAReportComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
            { path: 'validityreport', component: _ValidityReport_ValidityReport_component__WEBPACK_IMPORTED_MODULE_5__["ValidityReportComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
            { path: 'misreport', component: _MISReport_MISReport_component__WEBPACK_IMPORTED_MODULE_6__["MISReportComponent"], canActivate: [app_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
        ]
    }
];
var ReportsRoutingModule = /** @class */ (function () {
    function ReportsRoutingModule() {
    }
    ReportsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ReportsRoutingModule);
    return ReportsRoutingModule;
}());



/***/ }),

/***/ "./src/app/reports/reports.component.scss":
/*!************************************************!*\
  !*** ./src/app/reports/reports.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field[hidden] {\n  display: none !important;\n}\n\ntable {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12px !important;\n  color: #044da1;\n  font-weight: 400;\n  cursor: col-resize;\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n}\n\ntr.mat-header-row {\n  height: 40px !important;\n  text-align: center !important;\n  vertical-align: middle;\n}\n\ntr.mat-footer-row, tr.mat-row {\n  height: 30px !important;\n}\n\n.mat-table {\n  border-spacing: 0;\n  border-style: solid;\n  border-width: thin;\n  border-color: #f7f4f0;\n  width: 100%;\n}\n\ntd.mat-cell, td.mat-footer-cell,\n.mat-cell, .mat-footer-cell {\n  border-right-width: thin !important;\n  border-color: #eaeeef;\n  border-style: solid;\n  border-width: thin;\n  padding-left: 5px;\n  vertical-align: text-top !important;\n  font-size: 12px;\n}\n\n.card-header-title {\n  width: 55%;\n  display: inline-block;\n  line-height: 0;\n  background-color: #4882c5;\n  font-family: \"Titillium Web\", sans-serif;\n  font-size: 12px;\n  font-weight: 500;\n  color: #ffffff;\n  border-right-style: ridge;\n  border-left-style: ridge;\n  border-left-color: #fdab33;\n  border-right-color: #fdab33;\n  padding: 4px 4px 0px 14px;\n  border-radius: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVwb3J0cy9FOlxcREFSIFVBVCBGaWxlc1xcQXNzZXQtVUkvc3JjXFxhcHBcXHJlcG9ydHNcXHJlcG9ydHMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3JlcG9ydHMvcmVwb3J0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHdCQUFBO0FDQ0o7O0FERUE7RUFDSSxXQUFBO0FDQ0o7O0FERUE7RUFDSSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSw2QkFBQTtFQUNBLHNCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FDQ0o7O0FERUE7O0VBRUksbUNBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1DQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9yZXBvcnRzL3JlcG9ydHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxudGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItY2VsbHtcclxuICAgIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50OyBcclxuICAgIGNvbG9yOiAjMDQ0ZGExO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGN1cnNvcjogY29sLXJlc2l6ZTtcclxuICAgIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZWFlZWVmO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogdGhpbjtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcblxyXG50ci5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50OyBcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbnRyLm1hdC1mb290ZXItcm93LCB0ci5tYXQtcm93IHtcclxuICAgIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xyXG59XHJcbiAgXHJcbi5tYXQtdGFibGV7XHJcbiAgICBib3JkZXItc3BhY2luZzogMDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IHRoaW47XHJcbiAgICBib3JkZXItY29sb3I6ICNmN2Y0ZjA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCwgIFxyXG4ubWF0LWNlbGwsIC5tYXQtZm9vdGVyLWNlbGwge1xyXG4gICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiB0aGluICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItY29sb3I6ICNlYWVlZWY7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3AgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDsgXHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlci10aXRsZXtcclxuICAgIHdpZHRoOiA1NSU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBsaW5lLWhlaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYig3MiwgMTMwLCAxOTcpO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiVGl0aWxsaXVtIFdlYlwiLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LXN0eWxlOiByaWRnZTtcclxuICAgIGJvcmRlci1sZWZ0LWNvbG9yOiAjZmRhYjMzO1xyXG4gICAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzOyBcclxuICAgIHBhZGRpbmc6IDRweCA0cHggMHB4IDE0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbn0iLCJtYXQtZm9ybS1maWVsZFtoaWRkZW5dIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG50YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubWF0LWhlYWRlci1jZWxsIHtcbiAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjMDQ0ZGExO1xuICBmb250LXdlaWdodDogNDAwO1xuICBjdXJzb3I6IGNvbC1yZXNpemU7XG4gIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICNlYWVlZWY7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG5cbnRyLm1hdC1oZWFkZXItcm93IHtcbiAgaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG50ci5tYXQtZm9vdGVyLXJvdywgdHIubWF0LXJvdyB7XG4gIGhlaWdodDogMzBweCAhaW1wb3J0YW50O1xufVxuXG4ubWF0LXRhYmxlIHtcbiAgYm9yZGVyLXNwYWNpbmc6IDA7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgYm9yZGVyLWNvbG9yOiAjZjdmNGYwO1xuICB3aWR0aDogMTAwJTtcbn1cblxudGQubWF0LWNlbGwsIHRkLm1hdC1mb290ZXItY2VsbCxcbi5tYXQtY2VsbCwgLm1hdC1mb290ZXItY2VsbCB7XG4gIGJvcmRlci1yaWdodC13aWR0aDogdGhpbiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICNlYWVlZWY7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogdGhpbjtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5jYXJkLWhlYWRlci10aXRsZSB7XG4gIHdpZHRoOiA1NSU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbGluZS1oZWlnaHQ6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0ODgyYzU7XG4gIGZvbnQtZmFtaWx5OiBcIlRpdGlsbGl1bSBXZWJcIiwgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJpZ2h0LXN0eWxlOiByaWRnZTtcbiAgYm9yZGVyLWxlZnQtc3R5bGU6IHJpZGdlO1xuICBib3JkZXItbGVmdC1jb2xvcjogI2ZkYWIzMztcbiAgYm9yZGVyLXJpZ2h0LWNvbG9yOiAjZmRhYjMzO1xuICBwYWRkaW5nOiA0cHggNHB4IDBweCAxNHB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/reports/reports.component.ts":
/*!**********************************************!*\
  !*** ./src/app/reports/reports.component.ts ***!
  \**********************************************/
/*! exports provided: ReportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsComponent", function() { return ReportsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ReportsComponent = /** @class */ (function () {
    function ReportsComponent() {
    }
    ReportsComponent.prototype.ngOnInit = function () { };
    ReportsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reports',
            template: __webpack_require__(/*! raw-loader!./reports.component.html */ "./node_modules/raw-loader/index.js!./src/app/reports/reports.component.html"),
            styles: [__webpack_require__(/*! ./reports.component.scss */ "./src/app/reports/reports.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ReportsComponent);
    return ReportsComponent;
}());



/***/ }),

/***/ "./src/app/reports/reports.module.ts":
/*!*******************************************!*\
  !*** ./src/app/reports/reports.module.ts ***!
  \*******************************************/
/*! exports provided: ReportsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsModule", function() { return ReportsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _reports_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reports-routing.module */ "./src/app/reports/reports-routing.module.ts");
/* harmony import */ var app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _reports_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./reports.component */ "./src/app/reports/reports.component.ts");
/* harmony import */ var _SLAReport_SLAReport_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./SLAReport/SLAReport.component */ "./src/app/reports/SLAReport/SLAReport.component.ts");
/* harmony import */ var _ValidityReport_ValidityReport_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ValidityReport/ValidityReport.component */ "./src/app/reports/ValidityReport/ValidityReport.component.ts");
/* harmony import */ var _MISReport_MISReport_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./MISReport/MISReport.component */ "./src/app/reports/MISReport/MISReport.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var ReportsModule = /** @class */ (function () {
    function ReportsModule() {
    }
    ReportsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _reports_routing_module__WEBPACK_IMPORTED_MODULE_2__["ReportsRoutingModule"],
                app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            declarations: [
                _reports_component__WEBPACK_IMPORTED_MODULE_4__["ReportsComponent"],
                _SLAReport_SLAReport_component__WEBPACK_IMPORTED_MODULE_5__["SLAReportComponent"],
                _ValidityReport_ValidityReport_component__WEBPACK_IMPORTED_MODULE_6__["ValidityReportComponent"],
                _MISReport_MISReport_component__WEBPACK_IMPORTED_MODULE_7__["MISReportComponent"]
            ],
            exports: [
                app_shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
        })
    ], ReportsModule);
    return ReportsModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-reports-reports-module.js.map