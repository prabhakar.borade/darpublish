(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\" (document:mousemove)=\"onMouseMove($event)\" (document:keypress)=\"onKeyPress($event)\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-3 col-md-6 col-sm-6\" style=\"padding-left:15px !important; padding-right: 0px !important;\"\r\n                cdkDragLockAxis=\"x\" cdkDrag>\r\n                <div class=\"card card-stats\" style=\"background: linear-gradient(60deg, #FF9800, rgb(255, 152, 0));\">\r\n                    <div class=\"card header card-header-warning card-header-icon\"\r\n                        style=\"background-color: #FF9800;height: 120px;\">\r\n                        <table>\r\n                            <tr>\r\n                                <td>\r\n                                    <h2 class=\"card-box-header\" (click)=\"NearExpirySummary()\">\r\n                                        {{ nearexpiryCount }}\r\n                                    </h2>\r\n                                    <p class=\"card-box\">\r\n                                        {{ nearexpirytext }}\r\n                                    </p>\r\n                                </td>\r\n                                <td style=\"text-align: right;\">\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col-lg-3 col-md-6 col-sm-6\" style=\"padding-left:15px !important; padding-right: 0px !important;\"\r\n                cdkDragLockAxis=\"x\" cdkDrag>\r\n                <div class=\"card card-stats\" style=\"background: linear-gradient(60deg, #e54a3e, rgb(255, 145, 0));\">\r\n                    <div class=\"card header card-header-warning card-header-icon\"\r\n                        style=\" height: 120px; background-color: #e54a3e;\">\r\n                        <table>\r\n                            <tr>\r\n                                <td>\r\n                                    <h2 class=\"card-box-header\" (click)=\"VaidityexpiredSummary()\">\r\n                                        {{ validityexpiredCount }}\r\n                                    </h2>\r\n                                    <p class=\"card-box\">\r\n                                        {{ validityexpiredtext }}\r\n                                    </p>\r\n                                </td>\r\n                                <td style=\"text-align: right;\">\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col-lg-3 col-md-6 col-sm-6\" style=\"padding-left:15px !important; padding-right: 0px !important;\"\r\n                cdkDragLockAxis=\"x\" cdkDrag>\r\n                <div class=\"card card-stats\" style=\"background: linear-gradient(60deg,  #5d6b75, rgb(255, 152, 0));\">\r\n                    <div class=\"card header card-header-warning card-header-icon\"\r\n                        style=\"height: 120px; background-color: #318bd2;\">\r\n                        <table>\r\n                            <tr>\r\n                                <td>\r\n                                    <h2 class=\"card-box-header\" (click)=\"PendingSummary()\">\r\n                                        {{ pendingCount }}\r\n                                    </h2>\r\n                                    <p class=\"card-box\">\r\n                                        {{ pendingtext }}\r\n                                    </p>\r\n                                </td>\r\n                                <td style=\"text-align: right;\">\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col-lg-3 col-md-6 col-sm-6\" style=\"padding-left:15px !important; padding-right: 0px !important;\"\r\n                cdkDragLockAxis=\"x\" cdkDrag>\r\n                <div class=\"card card-stats\" style=\"background: linear-gradient(60deg, #8BC34A, rgb(255, 152, 0));\">\r\n                    <div class=\"card header card-header-warning card-header-icon\"\r\n                        style=\"background-color: #8BC34A; height: 120px;\">\r\n\r\n                        <table>\r\n                            <tr>\r\n                                <td>\r\n                                    <h2 class=\"card-box-header\" (click)=\"TotalAssetsSummary()\">\r\n                                        {{ totalassetsCount }}\r\n                                    </h2>\r\n                                    <p class=\"card-box\">\r\n                                        {{ totalassetstext }}\r\n                                    </p>\r\n                                </td>\r\n                                <td style=\"text-align: right;\">\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-6\" style=\"padding-left:15px !important; padding-right: 0px !important;\"\r\n                cdkDragLockAxis=\"x\" cdkDrag>\r\n\r\n                <div class=\"card card-chart\">\r\n                    <div class=\"chit-chat-heading\">\r\n                        <div> Nature of Assets\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <mat-accordion>\r\n                            <mat-expansion-panel [expanded]=\"true\" (opened)=\"0\" hideToggle>\r\n                                <mat-expansion-panel-header class=\"tr_group\">\r\n                                    <mat-panel-title>\r\n                                        Digital\r\n                                    </mat-panel-title>\r\n                                    <mat-panel-description>\r\n                                    </mat-panel-description>\r\n                                </mat-expansion-panel-header>\r\n\r\n                                <table style=\"width:100%;\">\r\n                                    <thead>\r\n                                        <tr class=\"tr_line\">\r\n                                            <th class=\"tr_box\"> Asset Type\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Count\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Near Expiry\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Expired\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Pending\r\n                                            </th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody>\r\n                                        <tr data-toggle=\"collapse\" *ngFor=\"let asset of NatureOfAssets_Digital\">\r\n                                            <td class=\"tr_box\">\r\n                                                {{ asset.AssetType }}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('total', asset)\">\r\n                                                {{asset.Count}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('expiry', asset)\">\r\n                                                {{asset.NearExpiry}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('expired', asset)\">\r\n                                                {{asset.Expired}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('pending', asset)\">\r\n                                                {{asset.ApprovalPending}}\r\n                                            </td>\r\n                                        </tr>\r\n\r\n                                        <tr class=\"tr_line\">\r\n                                            <td class=\"tr_box\">\r\n                                                Total\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('total', '1')\">\r\n                                                {{ DigitalCount}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('expiry', '1')\">\r\n                                                {{ DigitalNearExpiry}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('expired', '1')\">\r\n                                                {{ DigitalExpired}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('pending', '1')\">\r\n                                                {{ DigitalPending}}\r\n                                            </td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </mat-expansion-panel>\r\n\r\n                            <mat-expansion-panel>\r\n                                <mat-expansion-panel-header class=\"tr_group\">\r\n                                    <mat-panel-title>\r\n                                        Contracts\r\n                                    </mat-panel-title>\r\n                                    <mat-panel-description>\r\n                                    </mat-panel-description>\r\n                                </mat-expansion-panel-header>\r\n\r\n                                <table style=\"width:100%;\">\r\n                                    <thead>\r\n                                        <tr class=\"tr_line\">\r\n                                            <th class=\"tr_box\"> Asset Type\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Count\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Near Expiry\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Expired\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Pending\r\n                                            </th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody>\r\n                                        <tr data-toggle=\"collapse\" *ngFor=\"let asset of NatureOfAssets_Contracts\">\r\n                                            <td class=\"tr_box\">\r\n                                                {{ asset.AssetType }}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('total', asset)\">\r\n                                                {{asset.Count}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('expiry', asset)\">\r\n                                                {{asset.NearExpiry}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('expired', asset)\">\r\n                                                {{asset.Expired}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('pending', asset)\">\r\n                                                {{asset.ApprovalPending}}\r\n                                            </td>\r\n                                        </tr>\r\n\r\n                                        <tr class=\"tr_line\">\r\n                                            <td class=\"tr_box\">\r\n                                                Total\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('total', '2')\">\r\n                                                {{ ContractsCount}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('expiry', '2')\">\r\n                                                {{ ContractsNearExpiry}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('expired', '2')\">\r\n                                                {{ ContractsExpired}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('pending', '2')\">\r\n                                                {{ ContractsPending}}\r\n                                            </td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </mat-expansion-panel>\r\n\r\n                            <mat-expansion-panel>\r\n                                <mat-expansion-panel-header class=\"tr_group\">\r\n                                    <mat-panel-title>\r\n                                        Access Rights\r\n                                    </mat-panel-title>\r\n                                    <mat-panel-description>\r\n                                    </mat-panel-description>\r\n                                </mat-expansion-panel-header>\r\n\r\n                                <table style=\"width:100%;\">\r\n                                    <thead>\r\n                                        <tr class=\"tr_line\">\r\n                                            <th class=\"tr_box\"> Asset Type\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Count\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Near Expiry\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Expired\r\n                                            </th>\r\n                                            <th class=\"tr_box\"> Pending\r\n                                            </th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody>\r\n                                        <tr data-toggle=\"collapse\" *ngFor=\"let asset of NatureOfAssets_Assets\">\r\n                                            <td class=\"tr_box\">\r\n                                                {{ asset.AssetType }}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('total', asset)\">\r\n                                                {{asset.Count}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('expiry', asset)\">\r\n                                                {{asset.NearExpiry}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('expired', asset)\">\r\n                                                {{asset.Expired}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\"\r\n                                                (click)=\"close('pending', asset)\">\r\n                                                {{asset.ApprovalPending}}\r\n                                            </td>\r\n                                        </tr>\r\n\r\n                                        <tr class=\"tr_line\">\r\n                                            <td class=\"tr_box\">\r\n                                                Total\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('total', '3')\">\r\n                                                {{ AssetsCount}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('expiry', '3')\">\r\n                                                {{ AssetsNearExpiry}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('expired', '3')\">\r\n                                                {{ AssetsExpired}}\r\n                                            </td>\r\n                                            <td class=\"tr_box\"\r\n                                                style=\"text-align: center; font-weight: 600; cursor: pointer;\"\r\n                                                (click)=\"closeAll('pending', '3')\">\r\n                                                {{ AssetsPending}}\r\n                                            </td>\r\n\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </mat-expansion-panel>\r\n\r\n                        </mat-accordion>\r\n                    </div>\r\n                    <div class=\"card-footer\">\r\n                        <div class=\"stats\">\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"col-lg-6\" style=\"padding-left:15px !important; padding-right: 0px !important;\"\r\n                cdkDragLockAxis=\"x\" cdkDrag>\r\n\r\n                <div class=\"card card-chart\">\r\n                    <div class=\"chit-chat-heading\">\r\n                        <div> LOB Wise\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n\r\n                        <table style=\"width:100%;\">\r\n                            <thead>\r\n                                <tr class=\"tr_line\">\r\n                                    <th class=\"tr_box\"> LOB\r\n                                    </th>\r\n                                    <th class=\"tr_box\"> Count\r\n                                    </th>\r\n                                    <th class=\"tr_box\"> Near Expiry\r\n                                    </th>\r\n                                    <th class=\"tr_box\"> Expired\r\n                                    </th>\r\n                                    <th class=\"tr_box\"> Pending\r\n                                    </th>\r\n                                </tr>\r\n                            </thead>\r\n\r\n                            <tbody>\r\n                                <tr data-toggle=\"collapse\" *ngFor=\"let asset of LOBWise\">\r\n                                    <td class=\"tr_box\">\r\n                                        {{ asset.LOB }}\r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\" (click)=\"closeLOB('req', asset)\">                                  \r\n                                            {{asset.Count}}                                        \r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\" (click)=\"closeLOB('expiryl', asset)\">                                     \r\n                                            {{asset.NearExpiry}}                                        \r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center; cursor: pointer;\" (click)=\"closeLOB('expiredl', asset)\">                                        \r\n                                            {{asset.Expired}}                                        \r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center;cursor: pointer;\" (click)=\"closeLOB('pendingl', asset)\">                                        \r\n                                            {{asset.ApprovalPending}}                                        \r\n                                    </td>\r\n                                </tr>\r\n\r\n                                <tr class=\"tr_line\">\r\n                                    <td class=\"tr_box\">\r\n                                        Total\r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center; cursor: pointer; font-weight: 600;\" (click)=\"closeAllLOB('req')\">                                        \r\n                                            {{ totalCount}}\r\n                                        \r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center;  cursor: pointer; font-weight: 600;\" (click)=\"closeAllLOB('expiryl')\">                                        \r\n                                            {{ totalNearExpiry}}                                        \r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center; cursor: pointer; font-weight: 600;\" (click)=\"closeAllLOB('expiredl')\">                                        \r\n                                            {{ totalExpired}}                                        \r\n                                    </td>\r\n                                    <td class=\"tr_box\" style=\"text-align: center; cursor: pointer; font-weight: 600;\" (click)=\"closeAllLOB('pendingl')\">                                        \r\n                                            {{ totalPending}}                                        \r\n                                    </td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                    <div class=\"card-footer\">\r\n                        <div class=\"stats\">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/home.component.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/home.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <a [routerLink]=\"['/dar/viewregister']\"></a>\r\n\r\n <router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div mat-dialog-content>\r\n    <div class=\"card\">\r\n        <div class=\"card-header card-header-warning\" style=\"background: linear-gradient(60deg, #FF9800, #FF9800);\">\r\n\r\n            <h4 class=\"card-title\">\r\n                Near By Expiry\r\n                <button type=\"button\" style=\"Float:right; margin-top:-5px;\" (click)=\"close()\" mat-button mat-dailog-close> X </button>\r\n            </h4>\r\n        </div>\r\n\r\n        <div class=\"card-body table-responsive\">\r\n            <table class=\"table table-hover\" style=\"margin-top: 10px;\">\r\n                <tbody>\r\n                    <tr *ngFor=\"let item of activities\" (click)=\"close(item)\">\r\n                        <td>\r\n                            {{ item.Code }}\r\n                        </td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">                            \r\n                                {{ item.Count }} \r\n                        </td>\r\n                    </tr>\r\n                    <tr (click)=\"closeAll()\" *ngIf=\"activities.length > 0\">\r\n                        <td></td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">                            \r\n                                All                            \r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/pending-dialog/pending-dialog.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/pending-dialog/pending-dialog.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div mat-dialog-content>\r\n    <div class=\"card\">\r\n        <div class=\"card-header card-header-warning\" style=\"background: linear-gradient(60deg, #318bd2, #318bd2);\">\r\n\r\n            <h4 class=\"card-title\">\r\n                Pending for Approval\r\n                <button type=\"button\" style=\"Float:right; margin-top:-5px;\" (click)=\"close()\" mat-button\r\n                    mat-dailog-close> X </button>\r\n            </h4>\r\n        </div>\r\n\r\n        <div class=\"card-body table-responsive\">\r\n            <table class=\"table table-hover\" style=\"margin-top: 10px;\">\r\n                <tbody>\r\n                    <tr *ngFor=\"let item of activities\" (click)=\"close(item)\">\r\n                        <td>\r\n                            {{ item.Code }}\r\n                        </td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">\r\n                            {{ item.Count }}\r\n                        </td>\r\n                    </tr>\r\n                    <tr (click)=\"closeAll()\" *ngIf=\"activities.length > 0\">\r\n                        <td></td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">\r\n                            All\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div mat-dialog-content>\r\n    <div class=\"card\">\r\n        <div class=\"card-header card-header-warning\" style=\"background: linear-gradient(60deg, #8BC34A,#8BC34A);\">\r\n            <h4 class=\"card-title\">\r\n                Total Assets\r\n                <button type=\"button\" style=\"Float:right; margin-top:-5px;\" (click)=\"close()\" mat-button mat-dailog-close> X </button>\r\n            </h4>\r\n        </div>\r\n\r\n        <div class=\"card-body table-responsive\">\r\n            <table class=\"table table-hover\" style=\"margin-top: 10px;\">\r\n                <tbody>\r\n                    <tr *ngFor=\"let item of activities\" (click)=\"close(item)\">\r\n                        <td>\r\n                            {{ item.Code }}\r\n                        </td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">\r\n \r\n                                {{ item.Count }}\r\n                         </td>\r\n                    </tr>\r\n                    <tr (click)=\"closeAll()\" *ngIf=\"activities.length > 0\">\r\n                        <td></td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">\r\n                                All\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div mat-dialog-content>\r\n    <div class=\"card\">\r\n        <div class=\"card-header card-header-warning\" style=\"background: linear-gradient(60deg, #e54a3e, #e54a3e);\">\r\n\r\n            <h4 class=\"card-title\">\r\n                Validity Expired\r\n                <button type=\"button\" style=\"Float:right; margin-top:-5px;\" (click)=\"close()\" mat-button\r\n                    mat-dailog-close> X </button>\r\n            </h4>\r\n        </div>\r\n\r\n        <div class=\"card-body table-responsive\">\r\n            <table class=\"table table-hover\" style=\"margin-top: 10px;\">\r\n                <tbody>\r\n                    <tr *ngFor=\"let item of activities\" (click)=\"close(item)\">\r\n                        <td>\r\n                            {{ item.Code }}\r\n                        </td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">\r\n                            {{ item.Count }}\r\n                        </td>\r\n                    </tr>\r\n                    <tr (click)=\"closeAll()\" *ngIf=\"activities.length > 0\">\r\n                        <td></td>\r\n                        <td style=\"cursor: pointer; font-size: 15px; font-weight: bold;\">\r\n\r\n                            All\r\n\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/common/constant.ts":
/*!************************************!*\
  !*** ./src/app/common/constant.ts ***!
  \************************************/
/*! exports provided: PendingWithApprover, Cancelled, closed, Rejected, ExtensionType, KEY_CODE, Templates, Frequencys, Groups */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingWithApprover", function() { return PendingWithApprover; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cancelled", function() { return Cancelled; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "closed", function() { return closed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Rejected", function() { return Rejected; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionType", function() { return ExtensionType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KEY_CODE", function() { return KEY_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Templates", function() { return Templates; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Frequencys", function() { return Frequencys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Groups", function() { return Groups; });
var PendingWithApprover = 'Pending with approver';
var Cancelled = 'Cancelled';
var closed = 'Closed';
var Rejected = 'Rejected';
var ExtensionType = [
    { Id: 1, Code: 'CSV' },
    { Id: 2, Code: 'TXT' },
    { Id: 3, Code: 'XLS' },
    { Id: 4, Code: 'XLSX' }
];
var KEY_CODE;
(function (KEY_CODE) {
    KEY_CODE[KEY_CODE["A_KEY"] = 65] = "A_KEY";
    KEY_CODE[KEY_CODE["B_KEY"] = 66] = "B_KEY";
    KEY_CODE[KEY_CODE["U_KEY"] = 85] = "U_KEY";
    KEY_CODE[KEY_CODE["S_KEY"] = 83] = "S_KEY";
    KEY_CODE[KEY_CODE["X_KEY"] = 88] = "X_KEY";
    KEY_CODE[KEY_CODE["R_KEY"] = 82] = "R_KEY";
    KEY_CODE[KEY_CODE["V_KEY"] = 86] = "V_KEY";
    KEY_CODE[KEY_CODE["C_KEY"] = 67] = "C_KEY";
})(KEY_CODE || (KEY_CODE = {}));
;
var Templates = [
    { Id: 1, Code: 'Reminder' },
    { Id: 2, Code: 'NearExpiry' },
    { Id: 3, Code: 'ValidityExpired' },
];
var Frequencys = [
    { Id: 1, Code: 'Daily' },
    { Id: 2, Code: 'Weekly' },
    { Id: 3, Code: 'Monthly' },
    { Id: 4, Code: 'Quarterly' },
];
var Groups = [
    { Id: 1, Code: 'Digital' },
    { Id: 2, Code: 'Contracts' },
    { Id: 3, Code: 'AccessRights' }
];


/***/ }),

/***/ "./src/app/dashboard/dashboard-routing-module.ts":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/dashboard-routing-module.ts ***!
  \*******************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/auth.guard */ "./src/app/common/auth.guard.ts");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.component */ "./src/app/dashboard/home.component.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"], children: [
            { path: '', component: _dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"], canActivate: [_common_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]] },
        ]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-box {\r\n    width: 200px;\r\n    height: 200px;\r\n    border: solid 1px #ccc;\r\n    color: rgba(0, 0, 0, 0.87);\r\n    cursor: move;\r\n    display: inline-flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    text-align: center;\r\n    background: #fff;\r\n    border-radius: 4px;\r\n    margin-right: 25px;\r\n    position: relative;\r\n    z-index: 1;\r\n    transition: box-shadow 200ms cubic-bezier(0, 0, 0.2, 1);\r\n    box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),\r\n                0 2px 2px 0 rgba(0, 0, 0, 0.14),\r\n                0 1px 5px 0 rgba(0, 0, 0, 0.12);\r\n}\r\n\r\n.example-box:active {\r\n    box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\r\n                0 8px 10px 1px rgba(0, 0, 0, 0.14),\r\n                0 3px 14px 2px rgba(0, 0, 0, 0.12);\r\n}\r\n\r\n.card-box {\r\n    font-size: 1.1em;\r\n    color: #fff;\r\n    margin: 0.3em 1.5em;\r\n    font-family: 'Carrois Gothic', sans-serif;\r\n}\r\n\r\n.card-box-header {\r\n\r\n    color: #fff;\r\n    font-size: 2em;\r\n    font-family: 'Carrois Gothic', sans-serif;\r\n    text-align: left;\r\n    margin-left: 24px;\r\n    margin-top: 10px;\r\n    cursor: pointer;\r\n}\r\n\r\n.chit-chat-heading {\r\n    font-size: 1.13em;\r\n    padding-top: 10px;\r\n    margin-left: 20px;\r\n    font-weight: 600;\r\n    color: #5f5d5d;\r\n    font-family: sans-serif;\r\n}\r\n\r\n.tr_box {\r\n    padding-right: 10px;\r\n    vertical-align: baseline;\r\n    white-space: nowrap;\r\n    font-size: small;\r\n}\r\n\r\n.tr_group {\r\n    font-weight: 600;\r\n    height: 40px;\r\n    border-radius: initial;\r\n}\r\n\r\n.tr_line {\r\n    border-bottom-style: ridge;\r\n    border-bottom-color: #f3f1f1;\r\n    font-size: small;\r\n    margin-bottom: 10px;\r\n    padding-bottom: 10px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixvQkFBb0I7SUFDcEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVix1REFBdUQ7SUFDdkQ7OytDQUUyQztBQUMvQzs7QUFFQTtJQUNJOztrREFFOEM7QUFDbEQ7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLG1CQUFtQjtJQUNuQix5Q0FBeUM7QUFDN0M7O0FBRUE7O0lBRUksV0FBVztJQUNYLGNBQWM7SUFDZCx5Q0FBeUM7SUFDekMsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsdUJBQXVCO0FBQzNCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLHdCQUF3QjtJQUN4QixtQkFBbUI7SUFDbkIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSwwQkFBMEI7SUFDMUIsNEJBQTRCO0lBQzVCLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsb0JBQW9CO0FBQ3hCIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtYm94IHtcclxuICAgIHdpZHRoOiAyMDBweDtcclxuICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xyXG4gICAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44Nyk7XHJcbiAgICBjdXJzb3I6IG1vdmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDI1cHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAyMDBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcclxuICAgIGJveC1zaGFkb3c6IDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSxcclxuICAgICAgICAgICAgICAgIDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksXHJcbiAgICAgICAgICAgICAgICAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG59XHJcblxyXG4uZXhhbXBsZS1ib3g6YWN0aXZlIHtcclxuICAgIGJveC1zaGFkb3c6IDAgNXB4IDVweCAtM3B4IHJnYmEoMCwgMCwgMCwgMC4yKSxcclxuICAgICAgICAgICAgICAgIDAgOHB4IDEwcHggMXB4IHJnYmEoMCwgMCwgMCwgMC4xNCksXHJcbiAgICAgICAgICAgICAgICAwIDNweCAxNHB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG59XHJcblxyXG4uY2FyZC1ib3gge1xyXG4gICAgZm9udC1zaXplOiAxLjFlbTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgbWFyZ2luOiAwLjNlbSAxLjVlbTtcclxuICAgIGZvbnQtZmFtaWx5OiAnQ2Fycm9pcyBHb3RoaWMnLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG4uY2FyZC1ib3gtaGVhZGVyIHtcclxuXHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtc2l6ZTogMmVtO1xyXG4gICAgZm9udC1mYW1pbHk6ICdDYXJyb2lzIEdvdGhpYycsIHNhbnMtc2VyaWY7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDI0cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uY2hpdC1jaGF0LWhlYWRpbmcge1xyXG4gICAgZm9udC1zaXplOiAxLjEzZW07XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGNvbG9yOiAjNWY1ZDVkO1xyXG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi50cl9ib3gge1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBmb250LXNpemU6IHNtYWxsO1xyXG59XHJcblxyXG4udHJfZ3JvdXAge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IGluaXRpYWw7XHJcbn1cclxuXHJcbi50cl9saW5lIHtcclxuICAgIGJvcmRlci1ib3R0b20tc3R5bGU6IHJpZGdlO1xyXG4gICAgYm9yZGVyLWJvdHRvbS1jb2xvcjogI2YzZjFmMTtcclxuICAgIGZvbnQtc2l6ZTogc21hbGw7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_services_rest_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nearexpiry_dialog_nearexpiry_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./nearexpiry-dialog/nearexpiry-dialog.component */ "./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.ts");
/* harmony import */ var _pending_dialog_pending_dialog_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pending-dialog/pending-dialog.component */ "./src/app/dashboard/pending-dialog/pending-dialog.component.ts");
/* harmony import */ var _totalassets_dialog_totalassets_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./totalassets-dialog/totalassets-dialog.component */ "./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.ts");
/* harmony import */ var _validityexpired_dialog_validityexpired_dialog_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./validityexpired-dialog/validityexpired-dialog.component */ "./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.ts");
/* harmony import */ var app_common_constant__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/common/constant */ "./src/app/common/constant.ts");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(location, rest, global, router, dialog) {
        this.rest = rest;
        this.global = global;
        this.router = router;
        this.dialog = dialog;
        this.IsCentralAccess = false;
        this.IsLOBWiseAccess = false;
        this.idleTime = 1;
        this.groups = app_common_constant__WEBPACK_IMPORTED_MODULE_10__["Groups"];
        this.nearexpirytext = 'Near By Expiry';
        this.pendingtext = 'Pending For Approval';
        this.totalassetstext = 'Total Assets';
        this.validityexpiredtext = 'Validity Expired';
        this.nearexpiryCount = 0;
        this.validityexpiredCount = 0;
        this.totalassetsCount = 0;
        this.pendingCount = 0;
        this.NatureOfAssets_Digital = [];
        this.NatureOfAssets_Assets = [];
        this.NatureOfAssets_Contracts = [];
        this.panelOpenState = true;
        this.totalCount = 0;
        this.totalNearExpiry = 0;
        this.totalExpired = 0;
        this.totalPending = 0;
        this.DigitalCount = 0;
        this.DigitalNearExpiry = 0;
        this.DigitalExpired = 0;
        this.DigitalPending = 0;
        this.ContractsCount = 0;
        this.ContractsNearExpiry = 0;
        this.ContractsExpired = 0;
        this.ContractsPending = 0;
        this.AssetsCount = 0;
        this.AssetsNearExpiry = 0;
        this.AssetsExpired = 0;
        this.AssetsPending = 0;
        this.location = location;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var path = this.location.prepareExternalUrl(this.location.path());
        if (path.charAt(0) === '#') {
            path = path.slice(2);
        }
        this.UIObj = this.global.getUIObj(path);
        this.IsMaker = this.UIObj.UIRoles[0].Maker;
        this.IsChecker = this.UIObj.UIRoles[0].Checker;
        this.IsViewer = this.UIObj.UIRoles[0].Viewer;
        this.IsExport = this.UIObj.UIRoles[0].Export;
        this.IsCentralAccess = this.global.IsCentralAccess();
        this.IsLOBWiseAccess = this.global.IsLOBWiseAccess();
        this.userLoggedIn = JSON.parse(localStorage.getItem('userLoggedIn'));
        this.getTotalAssets();
        this.getPending();
        this.getNearExpiry();
        this.getValidityExpired();
        this.getNatureOfAssets();
        this.getLOBWise();
        this.idleInterval = setInterval(this.timerIncrement.bind(this), 10000);
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        if (this.idleInterval) {
            clearInterval(this.idleInterval);
        }
    };
    DashboardComponent.prototype.onMouseMove = function (e) {
    };
    DashboardComponent.prototype.onKeyPress = function (e) {
    };
    DashboardComponent.prototype.timerIncrement = function () {
        this.idleTime = this.idleTime + 1;
        if (this.idleTime > 5) {
            this.idleTime = 1;
            this.getTotalAssets();
            this.getPending();
            this.getNearExpiry();
            this.getValidityExpired();
            this.getNatureOfAssets();
            this.getLOBWise();
        }
    };
    DashboardComponent.prototype.close = function (item, asset) {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', item);
        localStorage.setItem('TypeId', asset.AssetTypeId);
        if (this.IsChecker && item == 'pending')
            this.router.navigate(['/dar/approveregister']);
        else
            this.router.navigate(['/dar/viewregister']);
    };
    DashboardComponent.prototype.closeAll = function (item, group) {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', item);
        localStorage.setItem('TypeId', '0');
        localStorage.setItem('GroupId', group);
        if (this.IsChecker && item == 'pending')
            this.router.navigate(['/dar/approveregister']);
        else
            this.router.navigate(['/dar/viewregister']);
    };
    DashboardComponent.prototype.closeLOB = function (item, asset) {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', item);
        localStorage.setItem('TypeId', asset.LobId);
        if (this.IsChecker && item == 'pendingl')
            this.router.navigate(['/dar/approveregister']);
        else
            this.router.navigate(['/dar/viewregister']);
    };
    DashboardComponent.prototype.closeAllLOB = function (item) {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', item);
        localStorage.setItem('TypeId', '0');
        if (this.IsChecker && item == 'pendingl')
            this.router.navigate(['/dar/approveregister']);
        else
            this.router.navigate(['/dar/viewregister']);
    };
    DashboardComponent.prototype.getTotalAssets = function () {
        var _this = this;
        var apiUrl = "";
        if (this.IsCentralAccess) {
            apiUrl = "dashboard/GetTotalAssets";
        }
        else {
            apiUrl = "dashboard/GetTotalAssetsByLOB/" + this.userLoggedIn.Id;
        }
        this.totalassetsCount = 0;
        this.rest.getAll(this.global.getapiendpoint() + apiUrl).subscribe(function (data) {
            data.Data.forEach(function (element) {
                _this.totalassetsCount += parseInt(element.Count);
            });
            _this.totalassetsData = data.Data;
        });
    };
    DashboardComponent.prototype.getPending = function () {
        var _this = this;
        var apiUrl = "";
        if (this.IsCentralAccess) {
            apiUrl = "dashboard/GetPending";
        }
        else {
            apiUrl = "dashboard/GetPendingByLOB/" + this.userLoggedIn.Id;
        }
        this.pendingCount = 0;
        this.rest.getAll(this.global.getapiendpoint() + apiUrl).subscribe(function (data) {
            data.Data.forEach(function (element) {
                _this.pendingCount += parseInt(element.Count);
            });
            _this.pendingData = data.Data;
        });
    };
    DashboardComponent.prototype.getNearExpiry = function () {
        var _this = this;
        var apiUrl = "";
        if (this.IsCentralAccess) {
            apiUrl = "dashboard/GetNearExpiry";
        }
        else {
            apiUrl = "dashboard/GetNearExpiryByLOB/" + this.userLoggedIn.Id;
        }
        this.nearexpiryCount = 0;
        this.rest.getAll(this.global.getapiendpoint() + apiUrl).subscribe(function (data) {
            data.Data.forEach(function (element) {
                _this.nearexpiryCount += parseInt(element.Count);
            });
            _this.nearexpiryData = data.Data;
        });
    };
    DashboardComponent.prototype.getValidityExpired = function () {
        var _this = this;
        var apiUrl = "";
        if (this.IsCentralAccess) {
            apiUrl = "dashboard/GetValidityExpired";
        }
        else {
            apiUrl = "dashboard/GetValidityExpiredByLOB/" + this.userLoggedIn.Id;
        }
        this.validityexpiredCount = 0;
        this.rest.getAll(this.global.getapiendpoint() + apiUrl).subscribe(function (data) {
            data.Data.forEach(function (element) {
                _this.validityexpiredCount += parseInt(element.Count);
            });
            _this.validityexpiredData = data.Data;
        });
    };
    DashboardComponent.prototype.getNatureOfAssets = function () {
        var _this = this;
        var apiUrl = "";
        if (this.IsCentralAccess) {
            apiUrl = "dashboard/GetNatureOfAssets";
        }
        else {
            apiUrl = "dashboard/GetNatureOfAssetsByLOB/" + this.userLoggedIn.Id;
        }
        this.rest.getAll(this.global.getapiendpoint() + apiUrl).subscribe(function (data) {
            _this.NatureOfAssets_Assets = [];
            _this.NatureOfAssets_Contracts = [];
            _this.NatureOfAssets_Digital = [];
            _this.DigitalCount = 0;
            _this.DigitalNearExpiry = 0;
            _this.DigitalPending = 0;
            _this.DigitalExpired = 0;
            _this.ContractsCount = 0;
            _this.ContractsExpired = 0;
            _this.ContractsNearExpiry = 0;
            _this.ContractsPending = 0;
            _this.AssetsCount = 0;
            _this.AssetsExpired = 0;
            _this.AssetsNearExpiry = 0;
            _this.AssetsPending = 0;
            data.Data.forEach(function (element) {
                if (element.Group == 'Digital') {
                    _this.NatureOfAssets_Digital.push(element);
                    _this.DigitalCount += parseInt(element.Count);
                    _this.DigitalNearExpiry += parseInt(element.NearExpiry);
                    _this.DigitalExpired += parseInt(element.Expired);
                    _this.DigitalPending += parseInt(element.ApprovalPending);
                }
                if (element.Group == 'Contracts') {
                    _this.NatureOfAssets_Contracts.push(element);
                    _this.ContractsCount += parseInt(element.Count);
                    _this.ContractsNearExpiry += parseInt(element.NearExpiry);
                    _this.ContractsExpired += parseInt(element.Expired);
                    _this.ContractsPending += parseInt(element.ApprovalPending);
                }
                if (element.Group == 'Access Rights') {
                    _this.NatureOfAssets_Assets.push(element);
                    _this.AssetsCount += parseInt(element.Count);
                    _this.AssetsNearExpiry += parseInt(element.NearExpiry);
                    _this.AssetsExpired += parseInt(element.Expired);
                    _this.AssetsPending += parseInt(element.ApprovalPending);
                }
            });
        });
    };
    DashboardComponent.prototype.getLOBWise = function () {
        var _this = this;
        var apiUrl = "";
        if (this.IsCentralAccess) {
            apiUrl = "dashboard/GetLOBWise";
        }
        else {
            apiUrl = "dashboard/GetLOBWiseByLOB/" + this.userLoggedIn.Id;
        }
        this.rest.getAll(this.global.getapiendpoint() + apiUrl).subscribe(function (data) {
            _this.LOBWise = data.Data;
            _this.totalCount = 0;
            _this.totalNearExpiry = 0;
            _this.totalPending = 0;
            _this.totalExpired = 0;
            data.Data.forEach(function (element) {
                _this.totalCount += parseInt(element.Count);
                _this.totalNearExpiry += parseInt(element.NearExpiry);
                _this.totalExpired += parseInt(element.Expired);
                _this.totalPending += parseInt(element.ApprovalPending);
            });
        });
    };
    DashboardComponent.prototype.NearExpirySummary = function () {
        var dialogRef = this.dialog.open(_nearexpiry_dialog_nearexpiry_dialog_component__WEBPACK_IMPORTED_MODULE_6__["NearExpiryDialogComponent"], {
            width: '40%',
            data: {
                activities: this.nearexpiryData, IsMaker: this.IsMaker, IsChecker: this.IsChecker, IsCentralAccess: this.IsCentralAccess, IsLOBWiseAccess: this.IsLOBWiseAccess
            }
        });
    };
    DashboardComponent.prototype.PendingSummary = function () {
        var dialogRef = this.dialog.open(_pending_dialog_pending_dialog_component__WEBPACK_IMPORTED_MODULE_7__["PendingDialogComponent"], {
            width: '40%',
            data: {
                activities: this.pendingData, IsMaker: this.IsMaker, IsChecker: this.IsChecker, IsCentralAccess: this.IsCentralAccess, IsLOBWiseAccess: this.IsLOBWiseAccess
            }
        });
    };
    DashboardComponent.prototype.TotalAssetsSummary = function () {
        var dialogRef = this.dialog.open(_totalassets_dialog_totalassets_dialog_component__WEBPACK_IMPORTED_MODULE_8__["TotalAssetsDialogComponent"], {
            width: '40%',
            data: {
                activities: this.totalassetsData, IsMaker: this.IsMaker, IsChecker: this.IsChecker, IsCentralAccess: this.IsCentralAccess, IsLOBWiseAccess: this.IsLOBWiseAccess
            }
        });
    };
    DashboardComponent.prototype.VaidityexpiredSummary = function () {
        var dialogRef = this.dialog.open(_validityexpired_dialog_validityexpired_dialog_component__WEBPACK_IMPORTED_MODULE_9__["ValidityExpiredDialogComponent"], {
            width: '40%',
            data: {
                activities: this.validityexpiredData, IsMaker: this.IsMaker, IsChecker: this.IsChecker, IsCentralAccess: this.IsCentralAccess, IsLOBWiseAccess: this.IsLOBWiseAccess
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_expansion__WEBPACK_IMPORTED_MODULE_11__["MatAccordion"], { static: true }),
        __metadata("design:type", _angular_material_expansion__WEBPACK_IMPORTED_MODULE_11__["MatAccordion"])
    ], DashboardComponent.prototype, "accordion", void 0);
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"], app_services_rest_service__WEBPACK_IMPORTED_MODULE_1__["RestService"], app_common_global__WEBPACK_IMPORTED_MODULE_2__["Global"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard-routing-module */ "./src/app/dashboard/dashboard-routing-module.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home.component */ "./src/app/dashboard/home.component.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var app_dashboard_nearexpiry_dialog_nearexpiry_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component */ "./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.ts");
/* harmony import */ var app_dashboard_pending_dialog_pending_dialog_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/dashboard/pending-dialog/pending-dialog.component */ "./src/app/dashboard/pending-dialog/pending-dialog.component.ts");
/* harmony import */ var app_dashboard_validityexpired_dialog_validityexpired_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/dashboard/validityexpired-dialog/validityexpired-dialog.component */ "./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.ts");
/* harmony import */ var app_dashboard_totalassets_dialog_totalassets_dialog_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/dashboard/totalassets-dialog/totalassets-dialog.component */ "./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__["DashboardRoutingModule"],
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ],
            declarations: [
                _home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"],
                _dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"],
                app_dashboard_nearexpiry_dialog_nearexpiry_dialog_component__WEBPACK_IMPORTED_MODULE_6__["NearExpiryDialogComponent"],
                app_dashboard_pending_dialog_pending_dialog_component__WEBPACK_IMPORTED_MODULE_7__["PendingDialogComponent"],
                app_dashboard_validityexpired_dialog_validityexpired_dialog_component__WEBPACK_IMPORTED_MODULE_8__["ValidityExpiredDialogComponent"],
                app_dashboard_totalassets_dialog_totalassets_dialog_component__WEBPACK_IMPORTED_MODULE_9__["TotalAssetsDialogComponent"]
            ],
            exports: [
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
                app_dashboard_nearexpiry_dialog_nearexpiry_dialog_component__WEBPACK_IMPORTED_MODULE_6__["NearExpiryDialogComponent"],
                app_dashboard_pending_dialog_pending_dialog_component__WEBPACK_IMPORTED_MODULE_7__["PendingDialogComponent"],
                app_dashboard_validityexpired_dialog_validityexpired_dialog_component__WEBPACK_IMPORTED_MODULE_8__["ValidityExpiredDialogComponent"],
                app_dashboard_totalassets_dialog_totalassets_dialog_component__WEBPACK_IMPORTED_MODULE_9__["TotalAssetsDialogComponent"]
            ],
            entryComponents: [
                app_dashboard_nearexpiry_dialog_nearexpiry_dialog_component__WEBPACK_IMPORTED_MODULE_6__["NearExpiryDialogComponent"],
                app_dashboard_pending_dialog_pending_dialog_component__WEBPACK_IMPORTED_MODULE_7__["PendingDialogComponent"],
                app_dashboard_validityexpired_dialog_validityexpired_dialog_component__WEBPACK_IMPORTED_MODULE_8__["ValidityExpiredDialogComponent"],
                app_dashboard_totalassets_dialog_totalassets_dialog_component__WEBPACK_IMPORTED_MODULE_9__["TotalAssetsDialogComponent"]
            ]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/home.component.scss":
/*!***********************************************!*\
  !*** ./src/app/dashboard/home.component.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9ob21lLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/dashboard/home.component.ts":
/*!*********************************************!*\
  !*** ./src/app/dashboard/home.component.ts ***!
  \*********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = /** @class */ (function () {
    function HomeComponent(router) {
        this.router = router;
        this.router.events.subscribe(function (e) {
            if (e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) { }
        });
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/dashboard/home.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12 !important;\n  color: #f1695d;\n  font-weight: 400;\n}\n\n.mat-slide-toggle-thumb {\n  background-color: green !important;\n}\n\n.mat-input-element:disabled {\n  color: #5c595d !important;\n}\n\n.mat-form-field-label {\n  color: #5c595d !important;\n}\n\n.card-header {\n  padding: 0.75rem 1.25rem !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL25lYXJleHBpcnktZGlhbG9nL0U6XFxEQVIgVUFUIEZpbGVzXFxBc3NldC1VSS9zcmNcXGFwcFxcZGFzaGJvYXJkXFxuZWFyZXhwaXJ5LWRpYWxvZ1xcbmVhcmV4cGlyeS1kaWFsb2cuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2Rhc2hib2FyZC9uZWFyZXhwaXJ5LWRpYWxvZy9uZWFyZXhwaXJ5LWRpYWxvZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUNDSjs7QURFQTtFQUNJLHdCQUFBO0VBQTBCLGNBQUE7RUFBZ0IsZ0JBQUE7QUNHOUM7O0FEQUE7RUFFSSxrQ0FBQTtBQ0VKOztBRENBO0VBRUkseUJBQUE7QUNDSjs7QURFQTtFQUVJLHlCQUFBO0FDQUo7O0FER0E7RUFDSSxtQ0FBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL25lYXJleHBpcnktZGlhbG9nL25lYXJleHBpcnktZGlhbG9nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGx7XHJcbiAgICBmb250LXNpemU6IDEyICFpbXBvcnRhbnQ7IGNvbG9yOiAjZjE2OTVkOyBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4ubWF0LXNsaWRlLXRvZ2dsZS10aHVtYlxyXG57XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWlucHV0LWVsZW1lbnQ6ZGlzYWJsZWRcclxue1xyXG4gICAgY29sb3I6ICM1YzU5NWQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWxhYmVsXHJcbntcclxuICAgIGNvbG9yOiAjNWM1OTVkICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlcntcclxuICAgIHBhZGRpbmc6IDAuNzVyZW0gMS4yNXJlbSAhaW1wb3J0YW50O1xyXG59IiwidGFibGUge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1oZWFkZXItY2VsbCB7XG4gIGZvbnQtc2l6ZTogMTIgIWltcG9ydGFudDtcbiAgY29sb3I6ICNmMTY5NWQ7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi5tYXQtc2xpZGUtdG9nZ2xlLXRodW1iIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW4gIWltcG9ydGFudDtcbn1cblxuLm1hdC1pbnB1dC1lbGVtZW50OmRpc2FibGVkIHtcbiAgY29sb3I6ICM1YzU5NWQgIWltcG9ydGFudDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWxhYmVsIHtcbiAgY29sb3I6ICM1YzU5NWQgIWltcG9ydGFudDtcbn1cblxuLmNhcmQtaGVhZGVyIHtcbiAgcGFkZGluZzogMC43NXJlbSAxLjI1cmVtICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.ts ***!
  \****************************************************************************/
/*! exports provided: NearExpiryDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NearExpiryDialogComponent", function() { return NearExpiryDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var NearExpiryDialogComponent = /** @class */ (function () {
    function NearExpiryDialogComponent(router, dialogRef, data) {
        this.router = router;
        this.dialogRef = dialogRef;
        this.data = data;
        this.activities = data.activities;
    }
    NearExpiryDialogComponent.prototype.ngOnInit = function () {
    };
    NearExpiryDialogComponent.prototype.close = function (item) {
        if (item != undefined) {
            localStorage.removeItem('Type');
            localStorage.removeItem('TypeId');
            localStorage.removeItem('GroupId');
            localStorage.setItem('Type', "expiry");
            localStorage.setItem('TypeId', item.AssetTypeId);
        }
        this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    NearExpiryDialogComponent.prototype.closeAll = function () {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', "expiry");
        localStorage.setItem('TypeId', '0');
        this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    NearExpiryDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nearexpiry-dialog',
            template: __webpack_require__(/*! raw-loader!./nearexpiry-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.html"),
            styles: [__webpack_require__(/*! ./nearexpiry-dialog.component.scss */ "./src/app/dashboard/nearexpiry-dialog/nearexpiry-dialog.component.scss")]
        }),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], NearExpiryDialogComponent);
    return NearExpiryDialogComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/pending-dialog/pending-dialog.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/dashboard/pending-dialog/pending-dialog.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12 !important;\n  color: #f1695d;\n  font-weight: 400;\n}\n\n.mat-slide-toggle-thumb {\n  background-color: green !important;\n}\n\n.mat-input-element:disabled {\n  color: #5c595d !important;\n}\n\n.mat-form-field-label {\n  color: #5c595d !important;\n}\n\n.card-header {\n  padding: 0.75rem 1.25rem !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3BlbmRpbmctZGlhbG9nL0U6XFxEQVIgVUFUIEZpbGVzXFxBc3NldC1VSS9zcmNcXGFwcFxcZGFzaGJvYXJkXFxwZW5kaW5nLWRpYWxvZ1xccGVuZGluZy1kaWFsb2cuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2Rhc2hib2FyZC9wZW5kaW5nLWRpYWxvZy9wZW5kaW5nLWRpYWxvZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUNDSjs7QURFQTtFQUNJLHdCQUFBO0VBQTBCLGNBQUE7RUFBZ0IsZ0JBQUE7QUNHOUM7O0FEQUE7RUFFSSxrQ0FBQTtBQ0VKOztBRENBO0VBRUkseUJBQUE7QUNDSjs7QURFQTtFQUVJLHlCQUFBO0FDQUo7O0FER0E7RUFDSSxtQ0FBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL3BlbmRpbmctZGlhbG9nL3BlbmRpbmctZGlhbG9nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGx7XHJcbiAgICBmb250LXNpemU6IDEyICFpbXBvcnRhbnQ7IGNvbG9yOiAjZjE2OTVkOyBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4ubWF0LXNsaWRlLXRvZ2dsZS10aHVtYlxyXG57XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWlucHV0LWVsZW1lbnQ6ZGlzYWJsZWRcclxue1xyXG4gICAgY29sb3I6ICM1YzU5NWQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWxhYmVsXHJcbntcclxuICAgIGNvbG9yOiAjNWM1OTVkICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlcntcclxuICAgIHBhZGRpbmc6IDAuNzVyZW0gMS4yNXJlbSAhaW1wb3J0YW50O1xyXG59IiwidGFibGUge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1oZWFkZXItY2VsbCB7XG4gIGZvbnQtc2l6ZTogMTIgIWltcG9ydGFudDtcbiAgY29sb3I6ICNmMTY5NWQ7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi5tYXQtc2xpZGUtdG9nZ2xlLXRodW1iIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW4gIWltcG9ydGFudDtcbn1cblxuLm1hdC1pbnB1dC1lbGVtZW50OmRpc2FibGVkIHtcbiAgY29sb3I6ICM1YzU5NWQgIWltcG9ydGFudDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWxhYmVsIHtcbiAgY29sb3I6ICM1YzU5NWQgIWltcG9ydGFudDtcbn1cblxuLmNhcmQtaGVhZGVyIHtcbiAgcGFkZGluZzogMC43NXJlbSAxLjI1cmVtICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/dashboard/pending-dialog/pending-dialog.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/dashboard/pending-dialog/pending-dialog.component.ts ***!
  \**********************************************************************/
/*! exports provided: PendingDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingDialogComponent", function() { return PendingDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var PendingDialogComponent = /** @class */ (function () {
    function PendingDialogComponent(router, global, dialogRef, data) {
        this.router = router;
        this.global = global;
        this.dialogRef = dialogRef;
        this.data = data;
        this.activities = data.activities;
    }
    PendingDialogComponent.prototype.ngOnInit = function () {
        var UIObj = this.global.getUIObj("/dar/approveregister");
        if (UIObj != null)
            this.isChecker = 1;
        else
            this.isChecker = 0;
    };
    PendingDialogComponent.prototype.close = function (item) {
        if (item != undefined) {
            localStorage.removeItem('Type');
            localStorage.removeItem('TypeId');
            localStorage.removeItem('GroupId');
            localStorage.setItem('Type', "pending");
            localStorage.setItem('TypeId', item.AssetTypeId);
        }
        if (this.isChecker == 1)
            this.router.navigate(['/dar/approveregister']);
        else
            this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    PendingDialogComponent.prototype.closeAll = function () {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', "pending");
        localStorage.setItem('TypeId', '0');
        if (this.isChecker == 1)
            this.router.navigate(['/dar/approveregister']);
        else
            this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    PendingDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pending-dialog',
            template: __webpack_require__(/*! raw-loader!./pending-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/pending-dialog/pending-dialog.component.html"),
            styles: [__webpack_require__(/*! ./pending-dialog.component.scss */ "./src/app/dashboard/pending-dialog/pending-dialog.component.scss")]
        }),
        __param(3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], PendingDialogComponent);
    return PendingDialogComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12 !important;\n  color: #f1695d;\n  font-weight: 400;\n}\n\n.mat-slide-toggle-thumb {\n  background-color: green !important;\n}\n\n.mat-input-element:disabled {\n  color: #5c595d !important;\n}\n\n.mat-form-field-label {\n  color: #5c595d !important;\n}\n\n.card-header {\n  padding: 0.75rem 1.25rem !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3RvdGFsYXNzZXRzLWRpYWxvZy9FOlxcREFSIFVBVCBGaWxlc1xcQXNzZXQtVUkvc3JjXFxhcHBcXGRhc2hib2FyZFxcdG90YWxhc3NldHMtZGlhbG9nXFx0b3RhbGFzc2V0cy1kaWFsb2cuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2Rhc2hib2FyZC90b3RhbGFzc2V0cy1kaWFsb2cvdG90YWxhc3NldHMtZGlhbG9nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQ0NKOztBREVBO0VBQ0ksd0JBQUE7RUFBMEIsY0FBQTtFQUFnQixnQkFBQTtBQ0c5Qzs7QURBQTtFQUVJLGtDQUFBO0FDRUo7O0FEQ0E7RUFFSSx5QkFBQTtBQ0NKOztBREVBO0VBRUkseUJBQUE7QUNBSjs7QURHQTtFQUNJLG1DQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvdG90YWxhc3NldHMtZGlhbG9nL3RvdGFsYXNzZXRzLWRpYWxvZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1jZWxse1xyXG4gICAgZm9udC1zaXplOiAxMiAhaW1wb3J0YW50OyBjb2xvcjogI2YxNjk1ZDsgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG5cclxuLm1hdC1zbGlkZS10b2dnbGUtdGh1bWJcclxue1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW4gIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1pbnB1dC1lbGVtZW50OmRpc2FibGVkXHJcbntcclxuICAgIGNvbG9yOiAjNWM1OTVkICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZC1sYWJlbFxyXG57XHJcbiAgICBjb2xvcjogIzVjNTk1ZCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY2FyZC1oZWFkZXJ7XHJcbiAgICBwYWRkaW5nOiAwLjc1cmVtIDEuMjVyZW0gIWltcG9ydGFudDtcclxufSIsInRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtaGVhZGVyLWNlbGwge1xuICBmb250LXNpemU6IDEyICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZjE2OTVkO1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4ubWF0LXNsaWRlLXRvZ2dsZS10aHVtYiB7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyZWVuICFpbXBvcnRhbnQ7XG59XG5cbi5tYXQtaW5wdXQtZWxlbWVudDpkaXNhYmxlZCB7XG4gIGNvbG9yOiAjNWM1OTVkICFpbXBvcnRhbnQ7XG59XG5cbi5tYXQtZm9ybS1maWVsZC1sYWJlbCB7XG4gIGNvbG9yOiAjNWM1OTVkICFpbXBvcnRhbnQ7XG59XG5cbi5jYXJkLWhlYWRlciB7XG4gIHBhZGRpbmc6IDAuNzVyZW0gMS4yNXJlbSAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.ts ***!
  \******************************************************************************/
/*! exports provided: TotalAssetsDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TotalAssetsDialogComponent", function() { return TotalAssetsDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var TotalAssetsDialogComponent = /** @class */ (function () {
    function TotalAssetsDialogComponent(router, global, dialogRef, data) {
        this.router = router;
        this.global = global;
        this.dialogRef = dialogRef;
        this.data = data;
        this.activities = data.activities;
    }
    TotalAssetsDialogComponent.prototype.ngOnInit = function () {
    };
    TotalAssetsDialogComponent.prototype.close = function (item) {
        if (item != undefined) {
            localStorage.removeItem('Type');
            localStorage.removeItem('TypeId');
            localStorage.removeItem('GroupId');
            localStorage.setItem('Type', "req");
            localStorage.setItem('TypeId', item.LobId);
        }
        this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    TotalAssetsDialogComponent.prototype.closeAll = function () {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', "req");
        localStorage.setItem('TypeId', '0');
        this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    TotalAssetsDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-totalassets-dialog',
            template: __webpack_require__(/*! raw-loader!./totalassets-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.html"),
            styles: [__webpack_require__(/*! ./totalassets-dialog.component.scss */ "./src/app/dashboard/totalassets-dialog/totalassets-dialog.component.scss")]
        }),
        __param(3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], TotalAssetsDialogComponent);
    return TotalAssetsDialogComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.scss ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%;\n}\n\n.mat-header-cell {\n  font-size: 12 !important;\n  color: #f1695d;\n  font-weight: 400;\n}\n\n.mat-slide-toggle-thumb {\n  background-color: green !important;\n}\n\n.mat-input-element:disabled {\n  color: #5c595d !important;\n}\n\n.mat-form-field-label {\n  color: #5c595d !important;\n}\n\n.card-header {\n  padding: 0.75rem 1.25rem !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3ZhbGlkaXR5ZXhwaXJlZC1kaWFsb2cvRTpcXERBUiBVQVQgRmlsZXNcXEFzc2V0LVVJL3NyY1xcYXBwXFxkYXNoYm9hcmRcXHZhbGlkaXR5ZXhwaXJlZC1kaWFsb2dcXHZhbGlkaXR5ZXhwaXJlZC1kaWFsb2cuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2Rhc2hib2FyZC92YWxpZGl0eWV4cGlyZWQtZGlhbG9nL3ZhbGlkaXR5ZXhwaXJlZC1kaWFsb2cuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0FDQ0o7O0FERUE7RUFDSSx3QkFBQTtFQUEwQixjQUFBO0VBQWdCLGdCQUFBO0FDRzlDOztBREFBO0VBRUksa0NBQUE7QUNFSjs7QURDQTtFQUVJLHlCQUFBO0FDQ0o7O0FERUE7RUFFSSx5QkFBQTtBQ0FKOztBREdBO0VBQ0ksbUNBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC92YWxpZGl0eWV4cGlyZWQtZGlhbG9nL3ZhbGlkaXR5ZXhwaXJlZC1kaWFsb2cuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItY2VsbHtcclxuICAgIGZvbnQtc2l6ZTogMTIgIWltcG9ydGFudDsgY29sb3I6ICNmMTY5NWQ7IGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbi5tYXQtc2xpZGUtdG9nZ2xlLXRodW1iXHJcbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGdyZWVuICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtaW5wdXQtZWxlbWVudDpkaXNhYmxlZFxyXG57XHJcbiAgICBjb2xvcjogIzVjNTk1ZCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQtbGFiZWxcclxue1xyXG4gICAgY29sb3I6ICM1YzU5NWQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNhcmQtaGVhZGVye1xyXG4gICAgcGFkZGluZzogMC43NXJlbSAxLjI1cmVtICFpbXBvcnRhbnQ7XHJcbn0iLCJ0YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubWF0LWhlYWRlci1jZWxsIHtcbiAgZm9udC1zaXplOiAxMiAhaW1wb3J0YW50O1xuICBjb2xvcjogI2YxNjk1ZDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cblxuLm1hdC1zbGlkZS10b2dnbGUtdGh1bWIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbiAhaW1wb3J0YW50O1xufVxuXG4ubWF0LWlucHV0LWVsZW1lbnQ6ZGlzYWJsZWQge1xuICBjb2xvcjogIzVjNTk1ZCAhaW1wb3J0YW50O1xufVxuXG4ubWF0LWZvcm0tZmllbGQtbGFiZWwge1xuICBjb2xvcjogIzVjNTk1ZCAhaW1wb3J0YW50O1xufVxuXG4uY2FyZC1oZWFkZXIge1xuICBwYWRkaW5nOiAwLjc1cmVtIDEuMjVyZW0gIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.ts ***!
  \**************************************************************************************/
/*! exports provided: ValidityExpiredDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidityExpiredDialogComponent", function() { return ValidityExpiredDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_common_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/common/global */ "./src/app/common/global.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ValidityExpiredDialogComponent = /** @class */ (function () {
    function ValidityExpiredDialogComponent(router, global, dialogRef, data) {
        this.router = router;
        this.global = global;
        this.dialogRef = dialogRef;
        this.data = data;
        this.activities = data.activities;
    }
    ValidityExpiredDialogComponent.prototype.ngOnInit = function () {
    };
    ValidityExpiredDialogComponent.prototype.close = function (item) {
        if (item != undefined) {
            localStorage.removeItem('Type');
            localStorage.removeItem('TypeId');
            localStorage.removeItem('GroupId');
            localStorage.setItem('Type', "expired");
            localStorage.setItem('TypeId', item.AssetTypeId);
        }
        this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    ValidityExpiredDialogComponent.prototype.closeAll = function () {
        localStorage.removeItem('Type');
        localStorage.removeItem('TypeId');
        localStorage.removeItem('GroupId');
        localStorage.setItem('Type', "expired");
        localStorage.setItem('TypeId', '0');
        this.router.navigate(['/dar/viewregister']);
        this.dialogRef.close();
    };
    ValidityExpiredDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-validityexpired-dialog',
            template: __webpack_require__(/*! raw-loader!./validityexpired-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.html"),
            styles: [__webpack_require__(/*! ./validityexpired-dialog.component.scss */ "./src/app/dashboard/validityexpired-dialog/validityexpired-dialog.component.scss")]
        }),
        __param(3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], app_common_global__WEBPACK_IMPORTED_MODULE_3__["Global"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ValidityExpiredDialogComponent);
    return ValidityExpiredDialogComponent;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map