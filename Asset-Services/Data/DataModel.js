var connect = require('./Connect');

var sequelize = connect.sequelize;
var Sequelize = connect.Sequelize;
const Model = connect.Sequelize.Model;

// Log Tables
class ErrorLog extends Model { }
class MailLog extends Model { }
class CronService extends Model { }

//Master tables
class UIMst extends Model { }
class EntityMst extends Model { }
class LOBMst extends Model { }
class SublobMst extends Model { }
class InventoryTypeMst extends Model { }
class VendorMst extends Model { }
class NotifyconfigMst extends Model { }
class NotifyLobMap extends Model { }
class NotifyRoleMap extends Model { }

//User Management
class UserMst extends Model { }
class UserLobMap extends Model { }
class UserSublobMap extends Model { }
class RoleMst extends Model { }
class UserRoleMap extends Model { }

//class UserADUserMap extends Model { }
class UIRoleMap extends Model { }

// Asset Register 
class AssetRegister extends Model { }
class AssetRegisterHistory extends Model { }

////#region Tables

module.exports.ErrorLog = function () {
    ErrorLog.init({
        Id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
        ServiceName: { type: Sequelize.STRING(100), allowNull: true },
        FunctionName: { type: Sequelize.STRING(100), allowNull: true },
        ErrorObject: { type: Sequelize.TEXT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
    },
        {
            sequelize,
            modelName: 'ErrorLog',
            tableName: 'ErrorLog'
        });
    return ErrorLog;
}

module.exports.MailLog = function () {
    MailLog.init({
        Id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
        RequestId: { type: Sequelize.STRING(100), allowNull: true },
        MailTo: { type: Sequelize.STRING(1000), allowNull: true },
        MailCC: { type: Sequelize.STRING(1000), allowNull: true },
        MailSubject: { type: Sequelize.STRING(1000), allowNull: true },
        MailStatus: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
    },
        {
            sequelize,
            modelName: 'MailLog',
            tableName: 'MailLog'
        });
    return MailLog;
}

module.exports.CronService = function () {
    CronService.init({
        Id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.STRING(100), allowNull: true },
        Desc: { type: Sequelize.STRING(2000), allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
    },
        {
            sequelize,
            modelName: 'CronService',
            tableName: 'CronService'
        });
    return CronService;
}

module.exports.UIMst = function () {
    UIMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        ParentId: { type: Sequelize.INTEGER, allowNull: true },
        Title: { type: Sequelize.STRING(100), allowNull: true },
        Path: { type: Sequelize.STRING(2000), allowNull: true },
        Icon: { type: Sequelize.STRING(2000), allowNull: true },
        CssClass: { type: Sequelize.STRING(2000), allowNull: true },
        Sequence: { type: Sequelize.INTEGER, allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        IsChild: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
    },
        {
            sequelize,
            modelName: 'UI',
            tableName: 'UIMst'
        });
    exports.UIRoleMap();
    UIMst.hasMany(UIRoleMap);
    UIMst.belongsTo(UIRoleMap, { foreignKey: 'Id', constraints: false, });

    return UIMst;
}

module.exports.EntityMst = function () {
    EntityMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.STRING(100), allowNull: true },
        Desc: { type: Sequelize.STRING(2000), allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'Entity',
            tableName: 'EntityMst'
        });

    return EntityMst;
}

module.exports.NotifyconfigMst = function () {
    NotifyconfigMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },         
        InventoryTypeId: { type: Sequelize.INTEGER, allowNull: true },
        FrequencyId: { type: Sequelize.INTEGER, allowNull: true },
        TriggerDays: { type: Sequelize.INTEGER, allowNull: true },
        TemplateId: { type: Sequelize.INTEGER, allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'Notifyconfig',
            tableName: 'NotifyconfigMst'
        });

    exports.NotifyLobMap();
    NotifyconfigMst.hasMany(NotifyLobMap);
    NotifyconfigMst.belongsTo(NotifyLobMap, { foreignKey: 'Id', constraints: false });

    exports.NotifyRoleMap();
    NotifyconfigMst.hasMany(NotifyRoleMap);
    NotifyconfigMst.belongsTo(NotifyRoleMap, { foreignKey: 'Id', constraints: false });
  
    exports.InventoryTypeMst();
    NotifyconfigMst.belongsTo(InventoryTypeMst, { foreignKey: 'InventoryTypeId' });

    return NotifyconfigMst;
}

module.exports.NotifyLobMap = function () {
    NotifyLobMap.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        NotifyconfigId: { type: Sequelize.INTEGER, allowNull: true },
        LobId: { type: Sequelize.INTEGER, allowNull: true }
    },
        {
            sequelize,
            modelName: 'NotifyLob',
            tableName: 'NotifyLobMap'
        });

    exports.LOBMst();
    NotifyLobMap.belongsTo(LOBMst, { foreignKey: 'LobId' });

    return NotifyLobMap;
}

module.exports.NotifyRoleMap = function () {
    NotifyRoleMap.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        NotifyconfigId: { type: Sequelize.INTEGER, allowNull: true },
        RoleId: { type: Sequelize.INTEGER, allowNull: true }
    },
        {
            sequelize,
            modelName: 'NotifyRole',
            tableName: 'NotifyRoleMap'
        });

    exports.RoleMst();
    NotifyRoleMap.belongsTo(RoleMst, { foreignKey: 'RoleId' });

    return NotifyRoleMap;
}

module.exports.VendorMst = function () {
    VendorMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.STRING(100), allowNull: true },
        Name: { type: Sequelize.STRING(100), allowNull: true },
        ContactPerson: { type: Sequelize.STRING(100), allowNull: true },
        ContactNumber: { type: Sequelize.STRING(100), allowNull: true },
        Address1: { type: Sequelize.STRING(2000), allowNull: true },
        Address2: { type: Sequelize.STRING(2000), allowNull: true },
        Address3: { type: Sequelize.STRING(2000), allowNull: true },
        State: { type: Sequelize.STRING(100), allowNull: true },
        City: { type: Sequelize.STRING(100), allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'Vendor',
            tableName: 'VendorMst'
        });

    return VendorMst;
}

module.exports.InventoryTypeMst = function () {
    InventoryTypeMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.STRING(100), allowNull: true },
        Desc: { type: Sequelize.STRING(2000), allowNull: true },
        GroupId: { type: Sequelize.INTEGER, allowNull: true},
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'InventoryType',
            tableName: 'InventoryTypeMst'
        });

    return InventoryTypeMst;
}

module.exports.LOBMst = function () {
    LOBMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.STRING(100), allowNull: true },
        Desc: { type: Sequelize.STRING(2000), allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'LOB',
            tableName: 'LOBMst'
        });

    return LOBMst;
}

module.exports.SublobMst = function () {
    SublobMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.STRING(100), allowNull: true },
        Desc: { type: Sequelize.STRING(2000), allowNull: true },
        LobId: { type: Sequelize.INTEGER, allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'Sublob',
            tableName: 'SublobMst'
        });

    exports.LOBMst();
    SublobMst.belongsTo(LOBMst, { foreignKey: 'LobId' });

    return SublobMst;
}

module.exports.UserMst = function () {
    UserMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        ADUser: { type: Sequelize.BOOLEAN, allowNull: true },
        LoginId: { type: Sequelize.STRING(100), allowNull: true },
        EmpCode: { type: Sequelize.STRING(100), allowNull: true },
        EmpName: { type: Sequelize.STRING(200), allowNull: true },
        EmailId: { type: Sequelize.STRING(100), allowNull: true },
        EntityId: { type: Sequelize.INTEGER, allowNull: true },
        DefaultRoleId: { type: Sequelize.INTEGER, allowNull: true },
        Password: { type: Sequelize.STRING(100), allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'User',
            tableName: 'UserMst'
        });

    exports.EntityMst();
    UserMst.belongsTo(EntityMst, { foreignKey: 'EntityId' });

    exports.UserLobMap();
    UserMst.hasMany(UserLobMap);
    UserMst.belongsTo(UserLobMap, { foreignKey: 'Id', constraints: false, });

    exports.UserSublobMap();
    UserMst.hasMany(UserSublobMap);
    UserMst.belongsTo(UserSublobMap, { foreignKey: 'Id', constraints: false, });

    exports.UserRoleMap();
    UserMst.hasMany(UserRoleMap);
    UserMst.belongsTo(UserRoleMap, { foreignKey: 'Id', constraints: false, });

    return UserMst;
}

module.exports.UserLobMap = function () {
    UserLobMap.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        UserId: { type: Sequelize.INTEGER, allowNull: true },
        LobId: { type: Sequelize.INTEGER, allowNull: true }
    },
        {
            sequelize,
            modelName: 'UserLob',
            tableName: 'UserLobMap'
        });

    exports.LOBMst();
    UserLobMap.belongsTo(LOBMst, { foreignKey: 'LobId' });

    return UserLobMap;
}

module.exports.UserSublobMap = function () {
    UserSublobMap.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        UserId: { type: Sequelize.INTEGER, allowNull: true },
        SublobId: { type: Sequelize.INTEGER, allowNull: true }
    },
        {
            sequelize,
            modelName: 'UserSublob',
            tableName: 'UserSublobMap'
        });

    exports.SublobMst();
    UserSublobMap.belongsTo(SublobMst, { foreignKey: 'SublobId' });

    return UserSublobMap;
}

module.exports.UserRoleMap = function () {
    UserRoleMap.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        UserId: { type: Sequelize.INTEGER, allowNull: true },
        RoleId: { type: Sequelize.INTEGER, allowNull: true }
    },
        {
            sequelize,
            modelName: 'UserRole',
            tableName: 'UserRoleMap'
        });

    exports.RoleMst();
    UserRoleMap.belongsTo(RoleMst, { foreignKey: 'RoleId' });

    return UserRoleMap;
}

module.exports.RoleMst = function () {
    RoleMst.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.STRING(100), allowNull: true },
        Desc: { type: Sequelize.STRING(2000), allowNull: true },
        IsCentralAccess: { type: Sequelize.BOOLEAN, allowNull: true },
        IsLobwiseAccess: { type: Sequelize.BOOLEAN, allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'Role',
            tableName: 'RoleMst'
        });

    return RoleMst;
}

module.exports.UIRoleMap = function () {
    UIRoleMap.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        UIId: { type: Sequelize.INTEGER, allowNull: true },
        RoleId: { type: Sequelize.INTEGER, allowNull: true },
        Viewer: { type: Sequelize.BOOLEAN, allowNull: true },
        Maker: { type: Sequelize.BOOLEAN, allowNull: true },
        Checker: { type: Sequelize.BOOLEAN, allowNull: true },
        Edit: { type: Sequelize.BOOLEAN, allowNull: true },
        Export: { type: Sequelize.BOOLEAN, allowNull: true },
        Upload: { type: Sequelize.BOOLEAN, allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'UIRole',
            tableName: 'UIRoleMap'
        });
    exports.RoleMst();
    UIRoleMap.belongsTo(RoleMst, { foreignKey: 'RoleId', });

    return UIRoleMap;
}

module.exports.AssetRegister = function () {
    AssetRegister.init({
        Id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        Code: { type: Sequelize.BIGINT, unique: true },
        EntityId: { type: Sequelize.INTEGER, allowNull: true },
        LobId: { type: Sequelize.INTEGER, allowNull: true },
        SublobId: { type: Sequelize.INTEGER, allowNull: true },
        InventoryTypeId: { type: Sequelize.INTEGER, allowNull: true },
        InventoryName: { type: Sequelize.STRING(100), allowNull: true },
        IPAddress: { type: Sequelize.STRING(100), allowNull: true },
        Purpose: { type: Sequelize.STRING(2000), allowNull: true },
        VendorId: { type: Sequelize.INTEGER, allowNull: true },
        ValidityFrom: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ValidityTo: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ConsumerName: { type: Sequelize.STRING(100), allowNull: true },
        ADUser: { type: Sequelize.INTEGER, allowNull: true },
        EmpName: { type: Sequelize.STRING(100), allowNull: true },
        EmpEmailId: { type: Sequelize.STRING(100), allowNull: true },
        EmpContactNumber: { type: Sequelize.STRING(100), allowNull: true },
        TechOwner: { type: Sequelize.STRING(100), allowNull: true },
        TechName: { type: Sequelize.STRING(100), allowNull: true },
        TechEmailId: { type: Sequelize.STRING(100), allowNull: true },
        TechContactNumber: { type: Sequelize.STRING(100), allowNull: true },
        Dependency: { type: Sequelize.STRING(2000), allowNull: true },
        ImpactFailure: { type: Sequelize.STRING(2000), allowNull: true },
        Reason: { type: Sequelize.STRING(2000), allowNull: true },
        Remarks: { type: Sequelize.STRING(2000), allowNull: true },
        FileName: { type: Sequelize.STRING(2000), allowNull: true },
        IsActive: { type: Sequelize.BOOLEAN, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        ModifiedBy: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        ModifiedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
        StatusId: { type: Sequelize.INTEGER, allowNull: true },
    },
        {
            sequelize,
            modelName: 'AssetRegister',
            tableName: 'AssetRegister'
        });

    exports.EntityMst();
    AssetRegister.belongsTo(EntityMst, { foreignKey: 'EntityId' });

    exports.LOBMst();
    AssetRegister.belongsTo(LOBMst, { foreignKey: 'LobId' });

    exports.SublobMst();
    AssetRegister.belongsTo(SublobMst, { foreignKey: 'SublobId' });

    exports.VendorMst();
    AssetRegister.belongsTo(VendorMst, { foreignKey: 'VendorId' , constraints: false });

    exports.InventoryTypeMst();
    AssetRegister.belongsTo(InventoryTypeMst, { foreignKey: 'InventoryTypeId' });

    exports.UserMst();
    AssetRegister.belongsTo(UserMst, { foreignKey: 'CreatedBy' });
    AssetRegister.belongsTo(UserMst, { as: 'ModifiedUser', foreignKey: 'ModifiedBy' });

    exports.RoleMst();
    AssetRegister.belongsTo(RoleMst, { foreignKey: 'CreatedByRoleId' });
    AssetRegister.belongsTo(RoleMst, { as: 'ModifiedUserRole', foreignKey: 'ModifiedByRoleId' });

    exports.AssetRegisterHistory();
    AssetRegister.hasMany(AssetRegisterHistory);
    AssetRegister.belongsTo(AssetRegisterHistory, { foreignKey: 'Id', constraints: false });

    return AssetRegister;
}

module.exports.AssetRegisterHistory = function () {

    AssetRegisterHistory.init({
        Id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
        AssetRegisterId: { type: Sequelize.BIGINT, allowNull: true },
        Reason: { type: Sequelize.STRING(2000), allowNull: true },
        StatusId: { type: Sequelize.INTEGER, allowNull: true },
        Action: { type: Sequelize.INTEGER, allowNull: true },
        CreatedBy: { type: Sequelize.BIGINT, allowNull: true },
        CreatedDate: { type: Sequelize.DATE, allowNull: true, defaultValue: Sequelize.NOW },
        CreatedByRoleId: { type: Sequelize.BIGINT, allowNull: true },
    },
        {
            sequelize,
            modelName: 'AssetRegisterHistory',
            tableName: 'AssetRegisterHistory'
        });

    exports.UserMst();
    AssetRegisterHistory.belongsTo(UserMst, { foreignKey: 'CreatedBy' });
    exports.RoleMst();
    AssetRegisterHistory.belongsTo(RoleMst, { foreignKey: 'CreatedByRoleId' })

    return AssetRegisterHistory;
}

////#endregion