var connect = require('./Connect');
var datamodel = require('./DataModel');
var dataaccess = require('./DataAccess');

/// CheckConnection function used to check a database connection using API
module.exports.CheckConnection = function (res) {
    connect.sequelize
        .authenticate()
        .then(function (result) {
            res.status(200).json({ Success: true, Message: 'Connection has been establised successfully', Data: null });
        }, function (err) {
            res.status(200).json({ Success: false, Message: 'Unable to connect to the database : ' + err, Data: null });
        }); 
}

/// CreateTable function used to create Database tables using API
module.exports.CreateTable = function (res) {

    // log tables
    datamodel.ErrorLog();
    datamodel.MailLog();
    datamodel.CronService();

    //Master tables
    datamodel.UIMst();
    datamodel.EntityMst();
    datamodel.LOBMst();
    datamodel.SublobMst();
    datamodel.InventoryTypeMst();
    datamodel.VendorMst();
    datamodel.NotifyconfigMst();
    datamodel.NotifyLobMap();
    datamodel.NotifyRoleMap();
    datamodel.AssetRegister();
    datamodel.AssetRegisterHistory();
    
    //User Management tables
    datamodel.UserMst();
    datamodel.UserLobMap();
    datamodel.UserSublobMap();
    datamodel.UserRoleMap();
    datamodel.RoleMst();
    datamodel.UIRoleMap();

    connect.sequelize.sync()
        .then(() => {
            res.status(200).json({ Success: true, Message: 'Tables updated', Data: null });
        })
}

/// errorlogger function used to insert error logs into Error log table
module.exports.errorlogger = function (servicename, functionname, errorobj) {

    var err = 'Message : ' + errorobj.message + '\n' + 'Stack : ' + errorobj.stack;
    
    var values = {
        ServiceName: servicename,
        FunctionName: functionname,
        ErrorObject: err
    };

    dataaccess.Create(datamodel.ErrorLog(), values)
        .then(function (result) {
            console.log(JSON.stringify(result));
        }, function (err) {
            console.log('Error: ' + JSON.stringify(err));
        });
}

/// mailerrorlogger function used to insert mail error logs into Maillog table
module.exports.mailerrorlogger = function (errorobj) {

    const MailLog = datamodel.MailLog();
    
    var RequestIds = [];
   
    var values = {
        RequestId: '', 
        MailTo: errorobj.TO,
        MailCC: errorobj.CC,
        MailSubject: errorobj.Subject,
        MailStatus: errorobj.MailStatus,
    };
    
    dataaccess.Create(MailLog, values)
        .then(function (result) {
            if (result == null)
                module.exports.errorlogger('Mailer', 'Mailerrorlogger', { message: 'No object found', stack: '' });
        }, function (err) {
            module.exports.errorlogger('Mailer', 'Mailerrorlogger', err); 
        });
}

/// GetAllCronService function used to fetch all cron job entries avaiable for scheduler  
module.exports.GetAllCronService = function () {
    const CronService = datamodel.CronService();
    var param = { attributes: ['Code', 'IsActive'] };
    return dataaccess.FindAll(CronService, param);
}