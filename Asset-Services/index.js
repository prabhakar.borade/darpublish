var dataconn = require('./Data/DataConnection');
var express = require('express');
var path = require('path');
var fs = require('fs');
var app = express();
var staticRoot = __dirname + '/../Asset-UI/dist/';
var publicPath = path.join(__dirname, 'public');
var config = require('./Config');
//var cron = require('./CronScheduler/RunScheduler');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

// CORS Middleware node.js package for connect express
app.use(function (req, res, next) {

    //enabling CORS
    var menthods = "GET, POST";
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", menthods);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType, Content-Type, Accept, Authorization");
    if (!menthods.includes(req.method.toUpperCase())) {
        return res.status(200).json({});
    };
    next();
});

app.use(express.static(staticRoot));

////#region Common Functions 

// Service checking method
app.get("/sample", function (req, res) {
    res.status(200).json({ Success: true, Message: "Welcome Hello ", Data: null });
});

// Connection checking method
app.get("/CheckConnection", function (req, res) {
    dataconn.CheckConnection(res);
});

//Table creation Method
app.get("/CreateTable", function (reg, res) {
    dataconn.CreateTable(res);
});

//Download User Manual 
app.get("", function (req, res) {
    res.sendFile(path.join(publicPath, 'UserManual.pdf'));
});

////#endregion

////#region AD service

var adService = require('./Service/AD/AdService')();

app.use("/ad", adService);

////#endregion

////#region Common Service
 
var dashboardService = require('./Service/Dashboard/DashboardService')();
app.use("/dashboard", dashboardService);

var loginService = require('./Service/Login/LoginService')();
app.use("/login", loginService);

var menuService = require('./Service/Login/MenuService')();
app.use("/menu", menuService);

var passwordService = require('./Service/Login/PasswordService')();
app.use("/password", passwordService);

var maillogService = require('./Service/Master/MailLogService')();
app.use("/maillog", maillogService);

var errorlogService = require('./Service/Master/ErrorLogService')();
app.use("/errorlog", errorlogService);

////#endregion

////#region User Management

var userService = require('./Service/UserManagement/UserService')();
app.use("/user", userService);

var roleService = require('./Service/UserManagement/RoleService')();
app.use("/role", roleService);

var uiroleService = require('./Service/UserManagement/UIRoleService')();
app.use("/uirolemap", uiroleService);

////#endregion

////#region Master Service

var entityService = require('./Service/Master/EntityService')();
app.use("/entity", entityService);

var lobService = require('./Service/Master/LOBService')();
app.use("/lob", lobService);

var sublobService = require('./Service/Master/SublobService')();
app.use("/sublob", sublobService);

var inventorytypeService = require('./Service/Master/InventoryTypeService')();
app.use("/inventorytype", inventorytypeService);

var vendorService = require('./Service/Master/VendorService')();
app.use("/vendor", vendorService);

var notifyconfigService = require('./Service/Master/NotifyconfigService')();
app.use("/notifyconfig", notifyconfigService);

////#endregion

var assetService = require('./Service/AssetRegister/AssetService')();
app.use("/asset", assetService);

////#region Reports

var reportService = require('./Service/Report/ReportService')();
app.use("/report", reportService);

////#endregion

// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(staticRoot, 'index.html'));
});

// Start server and listen on http://localhost:1339/
var server = app.listen(config.service_port, function()
{
    var host = server.address().address;
    var port = server.address().port;
    var datetime = new Date();
    var message = "Server :- " + host + " running on Port : - " + port +  " Started at :- " + datetime; 
    console.log(message);
});
