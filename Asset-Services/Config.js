var environmentConfig = {
local:{
    service_port: 1339,
    ui_url: 'http://localhost:4200/dar/',
    group_mail: '',
    emailConfig: {
        email_host:  '10.250.6.37',
        from_email: 'EGIA Repository - WM Corp - Edelweiss<EGIARepository@Edelweissfin.com>',
        },
        dbConn: {
            dbServer: 'cobranding-db.cwaupinwoyhl.ap-south-1.rds.amazonaws.com',
            dbName: 'DAR',
            dbUser: 'pravin',
            dbPassword: 'India-123',            
        }
},
sit:{
    service_port: 1337,
    ui_url: 'http://edemumnewuatvm4:1337/',
    group_mail: '',
    emailConfig: {
        email_host: '10.250.6.63',
        from_email: 'EGIA Repository - WM Corp - Edelweiss<EGIARepository@Edelweissfin.com>',
        },
        dbConn: {
            dbServer: '10.250.19.84',
            dbName: 'DAR',
            dbUser: 'daruser',
            dbPassword: 'daruser~',            
        }
},
uat:{
    service_port: 1338,
    ui_url: 'http://edemumnewuatvm4:1338/',
    group_mail: '',
    emailConfig: {
        email_host: '10.250.6.63',
        from_email: 'EGIA Repository - WM Corp - Edelweiss<EGIARepository@Edelweissfin.com>',
        },
        dbConn: {
            dbServer: '10.250.19.84',
            dbName: 'DAR_UAT',
            dbUser: 'daruser',
            dbPassword: 'daruser~',           
        }
},
live:{
    service_port: 1337,
    ui_url: 'http://edemumkalapp035:1337/',
    group_mail: '',
    emailConfig: {
        email_host: '10.250.6.63',
        from_email: 'EGIA Repository - WM Corp - Edelweiss<EGIARepository@Edelweissfin.com>',
        },
        dbConn: {
            dbServer: '10.250.0.237',
            dbName: 'DAR',
            dbUser: 'dar',
            dbPassword: 'dar~',            
        }
}
}

var environment = 'local'; 

const finalConfig = environmentConfig[environment];

module.exports.service_port = finalConfig.service_port;
module.exports.ui_url = finalConfig.ui_url;
module.exports.group_mail = finalConfig.group_mail;
module.exports.emailConfig = finalConfig.emailConfig;
module.exports.dbConn = finalConfig.dbConn;

