var express = require('express');
var router = express.Router();
var connect = require('../../Data/Connect');
var dataconn = require('../../Data/DataConnection');

var routes = function () {

    router.route("/GetNearExpiry")
    .get(function (req, res) {

        var queryText = 'SELECT "GetNearExpiry"(:p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetNearExpiry', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetNearExpiryByLOB/:UserId")
    .get(function (req, res) {

        var queryText = 'SELECT "GetNearExpiryByLOB"(:p_userid, :p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_userid: req.params.UserId, p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetNearExpiryByLOB', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetValidityExpired")
    .get(function (req, res) {

        var queryText = 'SELECT "GetValidityExpired"(:p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetValidityExpired', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetValidityExpiredByLOB/:UserId")
    .get(function (req, res) {

        var queryText = 'SELECT "GetValidityExpiredByLOB"(:p_userid, :p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_userid: req.params.UserId, p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetValidityExpiredByLOB', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetPending")
    .get(function (req, res) {

        var queryText = 'SELECT "GetPending"(:p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetPending', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetPendingByLOB/:UserId")
    .get(function (req, res) {

        var queryText = 'SELECT "GetPendingByLOB"(:p_userid, :p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_userid: req.params.UserId, p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetPendingByLOB', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetTotalAssets")
    .get(function (req, res) {

        var queryText = 'SELECT "GetTotalAssets"(:p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetTotalAssets', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetTotalAssetsByLOB/:UserId")
    .get(function (req, res) {

        var queryText = 'SELECT "GetTotalAssetsByLOB"(:p_userid, :p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_userid: req.params.UserId, p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetTotalAssetsByLOB', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetNatureOfAssets")
    .get(function (req, res) {

        var queryText = 'SELECT "GetNatureOfAssets"(:p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetNatureOfAssets', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetNatureOfAssetsByLOB/:UserId")
    .get(function (req, res) {

        var queryText = 'SELECT "GetNatureOfAssetsByLOB"(:p_userid, :p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_userid: req.params.UserId, p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetNatureOfAssetsByLOB', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });         
        });
    });

    router.route("/GetLOBWise")
    .get(function (req, res) {

        var queryText = 'SELECT "GetLOBWise"(:p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetLOBWise', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });        

        });
    });

    router.route("/GetLOBWiseByLOB/:UserId")
    .get(function (req, res) {

        var queryText = 'SELECT "GetLOBWiseByLOB"(:p_userid, :p_ref); FETCH ALL IN "Assets";';
        var param = { replacements: { p_userid: req.params.UserId, p_ref: 'Assets' }, type: connect.sequelize.QueryTypes.SELECT }

        connect.sequelize
        .query(queryText, param)
        .then(function (result) {

            result.shift();
            res.status(200).json({ Success: true, Message: "Assets access", Data: result });
      
        }, function (err) {
            dataconn.errorlogger('DashboardService', 'GetLOBWiseByLOB', err);
            res.status(200).json({ Success: false, Message: "User has no access of Assets", Data: null });         
        });
    });

    return router;
};

module.exports = routes;
