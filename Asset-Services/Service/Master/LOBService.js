var express = require('express');
var router = express.Router();
var connect = require('../../Data/Connect');
var datamodel = require('../../Data/DataModel');
var dataaccess = require('../../Data/DataAccess');
var dataconn = require('../../Data/DataConnection');
var mailer = require('../../Common/Mailer');
var commonfunc = require('../../Common/CommonFunctions');
var async = require('async');
var promise = connect.Sequelize.Promise;

var routes = function () {

    router.route('/GetAllLOB')
        .get(function (req, res) {

            const LOBMst = datamodel.LOBMst();
            var param = { attributes: ['Id', 'Code', 'Desc', 'IsActive'], order: ['Code'] };

            dataaccess.FindAll(LOBMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'LOB Access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('LOBService', 'GetAllLOB', err);
                    res.status(200).json({ Success: false, Message: 'user has no access of LOB', Data: null });
                });
        });

    router.route('/GetAllActiveLOB')
        .get(function (req, res) {

            const LOBMst = datamodel.LOBMst();
            var param = { where: { IsActive: true }, attributes: ['Id', 'Code'] };

            dataaccess.FindAll(LOBMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'LOB access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('LOBService', 'GetAllActiveLOB', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                });
        });

    router.route('/GetLOBById/:Id')
        .get(function (req, res) {

            const LOBMst = datamodel.LOBMst();
            var param = { where: { Id: req.params.Id } };

            dataaccess.FindOne(LOBMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'LOB access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('LOBService', 'GetLOBById', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                });

        });

    router.route('/CheckDuplicateLOB/:Value/:Id')
        .get(function (req, res) {

            const LOBMst = datamodel.LOBMst();
            var param = {
                where: { Code: req.params.Value, Id: { [connect.Op.ne]: req.params.Id } },
                attributes: [[connect.sequelize.fn('count', connect.sequelize.col('Code')), 'Count']]
            };

            dataaccess.FindAll(LOBMst, param)
                .then(function (result) {
                    if (result != null && result.length > 0 && result[0].dataValues.Count != null && result[0].dataValues.Count > 0) {
                        res.status(200).json({ Success: true, Message: 'LOB already exists', Data: true });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'LOB does not exists', Data: false });
                    }
                },
                    function (err) {
                        dataconn.errorlogger('LOBService', 'CheckDuplicateLOB', err);
                        res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                    });
        });

    router.route('/CreateLOB')
        .post(function (req, res) {

            const LOBMst = datamodel.LOBMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                IsActive: req.body.IsActive,
                CreatedBy: req.body.UserId,
                CreatedByRoleId: req.body.UserRoleId,
            };
            dataaccess.Create(LOBMst, values)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'LOB saved successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('LOBService', 'CreateLOB', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('LOBService', 'CreateLOB', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                });
        });

    router.route('/UpdateLOB')
        .post(function (req, res) {

            const LOBMst = datamodel.LOBMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                IsActive: req.body.IsActive,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW'),
            };
            var param = { Id: req.body.Id };
            dataaccess.Update(LOBMst, values, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'LOB updated successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('LOBService', 'UpdateLOB', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('LOBService', 'UpdateLOB', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });

    router.route('/BulkUpload')
        .post(function (req, res) {

            var bulkdata = req.body.data;
            var UserId = req.body.UserId;
            var UserRoleId = req.body.UserRoleId;

            if (bulkdata) {
                var bulkRequest = new Promise((resolve, reject) => {
                    async.eachOfSeries(bulkdata, (bd, next) => {

                        const LOBMst = datamodel.LOBMst();

                        dataaccess.FindAll(LOBMst)
                            .then(function (result) {

                                var values = {
                                    Code: bd.Code.toString().trim(),
                                    Desc: bd.Desc,
                                    IsActive: bd.Status.toLowerCase().trim() == 'active' ? true : false,
                                    CreatedBy: UserId,
                                    CreatedByRoleId: UserRoleId,
                                    ModifiedBy: UserId,
                                    ModifiedByRoleId: UserRoleId,
                                };

                                dataaccess.Create(LOBMst, values)
                                    .then(function (results) {
                                        if (results != null) {
                                            next();
                                        }
                                        else {
                                            next({ Success: false, Message: 'Error occurred while saving records', Data: null });
                                        }
                                    }, function (err) {
                                        dataconn.errorlogger('LOBService', 'BulkUpload', err);
                                        next(err);

                                    });
                            }, function (err) {
                                dataconn.errorlogger('LOBService', 'BulkUpload', err);
                                next(err);

                            });
                    },
                        err => {
                            if (err) reject(err);
                            else resolve(bulkdata);
                        });
                });

                bulkRequest.then((data) => {
                    res.status(200).json({ Success: true, Message: 'All lobs saved successfully', Data: data });
                }).catch((err) => {
                    dataconn.errorlogger('LOBService', 'BulkUpload', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });

            } else {
                res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
            }
        });

    router.route('/GetAllLOBList')
        .get(function (req, res) {

            const LOBMst = datamodel.LOBMst();
            var param = { attributes: ['Code'] };

            dataaccess.FindAll(LOBMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'LOB access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('LOBService', 'GetAllLOBList', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
                });
        });

    router.route('/CheckActiveLOB/:Id')
        .get(function (req, res) {
          
            const UserLobMap = datamodel.UserLobMap();

            var param = { where: { LobId: req.params.Id } };

            Promise.all([
                dataaccess.FindAndCountAll(UserLobMap, param)
            ]).then(function (Users) {
                if (Users != null && Users.count > 0) {
                    res.status(200).json({ Success: true, Message: 'Can not deactivate this LOB, its already used in user master', Data: true });
                }
                else {
                    res.status(200).json({ Success: false, Message: 'Can deactivate, LOB is not used', Data: false });
                }
            }).catch(err => {
                dataconn.errorlogger('LOBService', 'CheckActiveLOB', err);
                res.status(200).json({ Success: false, Message: 'User has no access of LOB', Data: null });
            });
        });

    return router;;
};

module.exports = routes;