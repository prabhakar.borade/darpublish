var express = require('express');
var router = express.Router();
var connect = require('../../Data/Connect');
var datamodel = require('../../Data/DataModel');
var dataaccess = require('../../Data/DataAccess');
var dataconn = require('../../Data/DataConnection');
var mailer = require('../../Common/Mailer');
var commonfunc = require('../../Common/CommonFunctions');
var async = require('async');
var promise = connect.Sequelize.Promise;

var routes = function () {

    router.route('/GetAllVendor')
        .get(function (req, res) {

            const VendorMst = datamodel.VendorMst();
            var param = { attributes: ['Id', 'Code', 'Name', 'ContactPerson', 'ContactNumber', 'Address1', 'Address2', 'Address3', 'State', 'City', 'IsActive'], order: [['CreatedDate']] };

            dataaccess.FindAll(VendorMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Vendor Access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of vendor', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('VendorService', 'GetAllVendor', err);
                    res.status(200).json({ Success: false, Message: 'user has no access of Vendor', Data: null });
                });
        });

    router.route('/GetAllActiveVendor')
        .get(function (req, res) {

            const VendorMst = datamodel.VendorMst();
            var param = { where: { IsActive: true }, attributes: ['Id', 'Code'] };

            dataaccess.FindAll(VendorMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Vendor access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Vendor', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('VendorService', 'GetAllActiveVendor', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Vendor', Data: null });
                });
        });

    router.route('/GetVendorById/:Id')
        .get(function (req, res) {

            const VendorMst = datamodel.VendorMst();
            var param = { where: { Id: req.params.Id } };

            dataaccess.FindOne(VendorMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Vendor access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Vendor', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('VendorService', 'GetVendorById', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Vendor', Data: null });
                });

        });

    router.route('/CheckDuplicateVendor/:Value/:Id')
        .get(function (req, res) {

            const VendorMst = datamodel.VendorMst();
            var param = {
                where: { Name: req.params.Value, Id: { [connect.Op.ne]: req.params.Id } },
                attributes: [[connect.sequelize.fn('count', connect.sequelize.col('Name')), 'Count']]
            };

            dataaccess.FindAll(VendorMst, param)
                .then(function (result) {
                    if (result != null && result.length > 0 && result[0].dataValues.Count != null && result[0].dataValues.Count > 0) {
                        res.status(200).json({ Success: true, Message: 'Vendor already exists', Data: true });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'Vendor does not exists', Data: false });
                    }
                },
                    function (err) {
                        dataconn.errorlogger('VendorService', 'CheckDuplicateVendor', err);
                        res.status(200).json({ Success: false, Message: 'User has no access of Vendor', Data: null });
                    });
        });

    router.route('/CreateVendor')
        .post(function (req, res) {

            const VendorMst = datamodel.VendorMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Name: req.body.Name,
                ContactPerson: req.body.ContactPerson,
                ContactNumber: req.body.ContactNumber,
                Address1: req.body.Address1,
                Address2: req.body.Address2,
                Address3: req.body.Address3,
                State: req.body.State,
                City: req.body.City,
                IsActive: req.body.IsActive,
                CreatedBy: req.body.UserId,
                CreatedByRoleId: req.body.UserRoleId
            };
            dataaccess.Create(VendorMst, values)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Vendor saved successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('VendorService', 'CreateVendor', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('VendorService', 'CreateVendor', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                });
        });

    router.route('/UpdateVendor')
        .post(function (req, res) {

            const VendorMst = datamodel.VendorMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Name: req.body.Name,
                ContactPerson: req.body.ContactPerson,
                ContactNumber: req.body.ContactNumber,
                Address1: req.body.Address1,
                Address2: req.body.Address2,
                Address3: req.body.Address3,
                State: req.body.State,
                City: req.body.City,
                IsActive: req.body.IsActive,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW'),
            };
            var param = { Id: req.body.Id };
            dataaccess.Update(VendorMst, values, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Vendor updated successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('VendorService', 'UpdateVendor', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('VendorService', 'UpdateVendor', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });

    router.route('/BulkUpload')
        .post(function (req, res) {

            var bulkdata = req.body.data;
            var UserId = req.body.UserId;
            var UserRoleId = req.body.UserRoleId;

            if (bulkdata) {
                var bulkRequest = new Promise((resolve, reject) => {
                    async.eachOfSeries(bulkdata, (bd, next) => {

                        const VendorMst = datamodel.VendorMst();

                        dataaccess.FindAll(VendorMst)
                            .then(function (result) {

                                var values = {
                                    Code: bd.Code.toString().trim(),
                                    Name: bd.Name,
                                    ContactPerson: bd.ContactPerson,
                                    ContactNumber: bd.ContactNumber,
                                    Address1: bd.Address1,
                                    Address2: bd.Address2,
                                    Address3: bd.Address3,
                                    State: bd.State,
                                    City: bd.City,
                                    IsActive: bd.Status.toLowerCase().trim() == 'active' ? true : false,
                                    CreatedBy: UserId,
                                    CreatedByRoleId: UserRoleId,
                                    ModifiedBy: UserId,
                                    ModifiedByRoleId: UserRoleId,
                                };

                                dataaccess.Create(VendorMst, values)
                                    .then(function (results) {
                                        if (results != null) {
                                            next();
                                        }
                                        else {
                                            next({ Success: false, Message: 'Error occurred while saving records', Data: null });
                                        }
                                    }, function (err) {
                                        dataconn.errorlogger('VendorService', 'BulkUpload', err);
                                        next(err);

                                    });
                            }, function (err) {
                                dataconn.errorlogger('VendorService', 'BulkUpload', err);
                                next(err);

                            });
                    },
                        err => {
                            if (err) reject(err);
                            else resolve(bulkdata);
                        });
                });

                bulkRequest.then((data) => {
                    res.status(200).json({ Success: true, Message: 'All vendors saved successfully', Data: data });
                }).catch((err) => {
                    dataconn.errorlogger('VendorService', 'BulkUpload', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });

            } else {
                res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
            }
        });

    router.route('/GetAllVendorList')
        .get(function (req, res) {

            const VendorMst = datamodel.VendorMst();
            var param = { attributes: ['Code'] };

            dataaccess.FindAll(VendorMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Vendor access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Vendor', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('VendorService', 'GetAllVendorList', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Vendor', Data: null });
                });
        });


    return router;;
};

module.exports = routes;