var express = require('express');
var router = express.Router();
var connect = require('../../Data/Connect');
var datamodel = require('../../Data/DataModel');
var dataaccess = require('../../Data/DataAccess');
var dataconn = require('../../Data/DataConnection');
var mailer = require('../../Common/Mailer');
var commonfunc = require('../../Common/CommonFunctions');
var async = require('async');
var promise = connect.Sequelize.Promise;

var routes = function () {

    router.route('/GetAllInventoryType')
        .get(function (req, res) {

            const InventoryTypeMst = datamodel.InventoryTypeMst();
            var param = { attributes: ['Id', 'Code', 'Desc', 'GroupId', 'IsActive'], order: ['Code'] };

            dataaccess.FindAll(InventoryTypeMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'InventoryType Access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of inventory type', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('InventoryTypeService', 'GetAllInventoryType', err);
                    res.status(200).json({ Success: false, Message: 'user has no access of Inventory Type', Data: null });
                });
        });

    router.route('/GetAllActiveInventoryType')
        .get(function (req, res) {

            const InventoryTypeMst = datamodel.InventoryTypeMst();
            var param = { where: { IsActive: true }, attributes: ['Id', 'Code'] };

            dataaccess.FindAll(InventoryTypeMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Inventory Type access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Inventory Type', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('InventoryTypeService', 'GetAllActiveInventoryType', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Inventory Type', Data: null });
                });
        });

    router.route('/GetInventoryTypeById/:Id')
        .get(function (req, res) {

            const InventoryTypeMst = datamodel.InventoryTypeMst();
            var param = { where: { Id: req.params.Id } };

            dataaccess.FindOne(InventoryTypeMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Inventory Type access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Inventory Type', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('InventoryTypeService', 'GetInventoryTypeById', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Inventory Type', Data: null });
                });

        });

    router.route('/CheckDuplicateInventoryType/:Value/:Id')
        .get(function (req, res) {

            const InventoryTypeMst = datamodel.InventoryTypeMst();
            var param = {
                where: { Code: req.params.Value, Id: { [connect.Op.ne]: req.params.Id } },
                attributes: [[connect.sequelize.fn('count', connect.sequelize.col('Code')), 'Count']]
            };

            dataaccess.FindAll(InventoryTypeMst, param)
                .then(function (result) {
                    if (result != null && result.length > 0 && result[0].dataValues.Count != null && result[0].dataValues.Count > 0) {
                        res.status(200).json({ Success: true, Message: 'InventoryType already exists', Data: true });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'InventoryType does not exists', Data: false });
                    }
                },
                    function (err) {
                        dataconn.errorlogger('InventoryTypeService', 'CheckDuplicateInventoryType', err);
                        res.status(200).json({ Success: false, Message: 'User has no access of InventoryType', Data: null });
                    });
        });

    router.route('/CreateInventoryType')
        .post(function (req, res) {

            const InventoryTypeMst = datamodel.InventoryTypeMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                IsActive: req.body.IsActive,
                GroupId: req.body.GroupId,
                CreatedBy: req.body.UserId,
                CreatedByRoleId: req.body.UserRoleId,
            };
            dataaccess.Create(InventoryTypeMst, values)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'InventoryType saved successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('InventoryTypeService', 'CreateInventoryType', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('InventoryTypeService', 'CreateInventoryType', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                });
        });

    router.route('/UpdateInventoryType')
        .post(function (req, res) {

            const InventoryTypeMst = datamodel.InventoryTypeMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                GroupId: req.body.GroupId,
                IsActive: req.body.IsActive,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW'),
            };
            var param = { Id: req.body.Id };
            dataaccess.Update(InventoryTypeMst, values, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'InventoryType updated successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('InventoryTypeService', 'UpdateInventoryType', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('InventoryTypeService', 'UpdateInventoryType', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });

    router.route('/BulkUpload')
        .post(function (req, res) {

            var bulkdata = req.body.data;
            var UserId = req.body.UserId;
            var UserRoleId = req.body.UserRoleId;

            if (bulkdata) {
                var bulkRequest = new Promise((resolve, reject) => {
                    async.eachOfSeries(bulkdata, (bd, next) => {

                        const InventoryTypeMst = datamodel.InventoryTypeMst();

                        dataaccess.FindAll(InventoryTypeMst)
                            .then(function (result) {

                                var values = {
                                    Code: bd.Code.toString().trim(),
                                    Desc: bd.Desc,
                                    GroupId: bd.GroupId,
                                    IsActive: bd.Status.toLowerCase().trim() == 'active' ? true : false,
                                    CreatedBy: UserId,
                                    CreatedByRoleId: UserRoleId,
                                    ModifiedBy: UserId,
                                    ModifiedByRoleId: UserRoleId,
                                };

                                dataaccess.Create(InventoryTypeMst, values)
                                    .then(function (results) {
                                        if (results != null) {
                                            next();
                                        }
                                        else {
                                            next({ Success: false, Message: 'Error occurred while saving records', Data: null });
                                        }
                                    }, function (err) {
                                        dataconn.errorlogger('InventoryTypeService', 'BulkUpload', err);
                                        next(err);

                                    });
                            }, function (err) {
                                dataconn.errorlogger('InventoryTypeService', 'BulkUpload', err);
                                next(err);

                            });
                    },
                        err => {
                            if (err) reject(err);
                            else resolve(bulkdata);
                        });
                });

                bulkRequest.then((data) => {
                    res.status(200).json({ Success: true, Message: 'All inventory types saved successfully', Data: data });
                }).catch((err) => {
                    dataconn.errorlogger('InventoryTypeService', 'BulkUpload', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });

            } else {
                res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
            }
        });

    router.route('/GetAllInventoryTypeList')
        .get(function (req, res) {

            const InventoryTypeMst = datamodel.InventoryTypeMst();
            var param = { attributes: ['Code'] };

            dataaccess.FindAll(InventoryTypeMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'InventoryType access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of InventoryType', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('InventoryTypeService', 'GetAllInventoryTypeList', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of InventoryType', Data: null });
                });
        });

    return router;;
};

module.exports = routes;