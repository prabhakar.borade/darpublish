var express = require('express');
var router = express.Router();
var connect = require('../../Data/Connect');
var datamodel = require('../../Data/DataModel');
var dataaccess = require('../../Data/DataAccess');
var dataconn = require('../../Data/DataConnection');
var mailer = require('../../Common/Mailer');
var commonfunc = require('../../Common/CommonFunctions');
var async = require('async');
var promise = connect.Sequelize.Promise;

var routes = function () {

    router.route('/GetAllSublob')
        .get(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            const LOBMst = datamodel.LOBMst();

            var param = {
                attributes: ['Id', 'Code', 'Desc', 'LobId', 'IsActive'],
                include: [{ model: LOBMst, attributes: ['Code'], where: { IsActive: true } }],
                order: ['Code']
            };

            dataaccess.FindAll(SublobMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Sublob Access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('SublobService', 'GetAllSublob', err);
                    res.status(200).json({ Success: false, Message: 'user has no access of Sublob', Data: null });
                });
        });

    router.route('/GetAllActiveSublob')
        .get(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            var param = { where: { IsActive: true }, attributes: ['Id', 'Code'] };

            dataaccess.FindAll(SublobMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Sublob access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('SublobService', 'GetAllActiveSublob', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                });
        });

    router.route('/GetSublobById/:Id')
        .get(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            const LOBMst = datamodel.LOBMst();
            var param = {
                attributes: ['Id', 'Code', 'Desc', 'LobId', 'IsActive'],
                include: [{ model: LOBMst, attributes: ['Code'] }],
                where: { Id: req.params.Id }  
            }; 

            dataaccess.FindOne(SublobMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Sublob access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('SublobService', 'GetSublobById', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                });

        });

    router.route('/GetSubLOBByLOBId/:LOBId')
        .get(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            var LOBIds = [];
            LOBIds = req.params.LOBId.split(',');
            
            var param = {
                 where: { IsActive: true, LobId: { [connect.Op.in]: LOBIds }},
                 attributes: ['Id', 'Code']
            }; 

            dataaccess.FindAll(SublobMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Sublob access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('SublobService', 'GetSubLOBByLOBId', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                });

        });

    router.route('/CheckDuplicateSublob/:Value/:Id')
        .get(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            var param = {
                where: { Code: req.params.Value, Id: { [connect.Op.ne]: req.params.Id } },
                attributes: [[connect.sequelize.fn('count', connect.sequelize.col('Code')), 'Count']]
            };

            dataaccess.FindAll(SublobMst, param)
                .then(function (result) {
                    if (result != null && result.length > 0 && result[0].dataValues.Count != null && result[0].dataValues.Count > 0) {
                        res.status(200).json({ Success: true, Message: 'Sublob already exists', Data: true });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'Sublob does not exists', Data: false });
                    }
                },
                    function (err) {
                        dataconn.errorlogger('SublobService', 'CheckDuplicateSublob', err);
                        res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                    });
        });

    router.route('/CreateSublob')
        .post(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                LobId: req.body.LobId,
                IsActive: req.body.IsActive,
                CreatedBy: req.body.UserId,
                CreatedByRoleId: req.body.UserRoleId,
            };
            dataaccess.Create(SublobMst, values)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Sublob saved successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('SublobService', 'CreateSublob', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('SublobService', 'CreateSublob', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                });
        });

    router.route('/UpdateSublob')
        .post(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                LobId: req.body.LobId,
                IsActive: req.body.IsActive,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW'),
            };
            var param = { Id: req.body.Id };
            dataaccess.Update(SublobMst, values, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Sublob updated successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('SublobService', 'UpdateSublob', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('SublobService', 'UpdateSublob', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });

    router.route('/BulkUpload')
        .post(function (req, res) {

            var bulkdata = req.body.data;
            var UserId = req.body.UserId;
            var UserRoleId = req.body.UserRoleId;

            if (bulkdata) {
                var bulkRequest = new Promise((resolve, reject) => {
                    async.eachOfSeries(bulkdata, (bd, next) => {

                        const SublobMst = datamodel.SublobMst();

                        dataaccess.FindAll(SublobMst)
                            .then(function (result) {

                                var values = {
                                    Code: bd.Code.toString().trim(),
                                    Desc: bd.Desc,
                                    LobId: bd.LobId,
                                    IsActive: bd.Status.toLowerCase().trim() == 'active' ? true : false,
                                    CreatedBy: UserId,
                                    CreatedByRoleId: UserRoleId,
                                    ModifiedBy: UserId,
                                    ModifiedByRoleId: UserRoleId,
                                };

                                dataaccess.Create(SublobMst, values)
                                    .then(function (results) {
                                        if (results != null) {
                                            next();
                                        }
                                        else {
                                            next({ Success: false, Message: 'Error occurred while saving records', Data: null });
                                        }
                                    }, function (err) {
                                        dataconn.errorlogger('SublobService', 'BulkUpload', err);
                                        next(err);

                                    });
                            }, function (err) {
                                dataconn.errorlogger('SublobService', 'BulkUpload', err);
                                next(err);

                            });
                    },
                        err => {
                            if (err) reject(err);
                            else resolve(bulkdata);
                        });
                });

                bulkRequest.then((data) => {
                    res.status(200).json({ Success: true, Message: 'All Sublobs saved successfully', Data: data });
                }).catch((err) => {
                    dataconn.errorlogger('SublobService', 'BulkUpload', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });

            } else {
                res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
            }
        });

    router.route('/GetAllSublobList')
        .get(function (req, res) {

            const SublobMst = datamodel.SublobMst();
            var param = { attributes: ['Code'] };

            dataaccess.FindAll(SublobMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Sublob access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('SublobService', 'GetAllSublobList', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Sublob', Data: null });
                });
        });

    router.route('/CheckActiveSublob/:Id')
        .get(function (req, res) {
           
            const UserSublobMap = datamodel.UserSublobMap();

            var param = { where: { SublobId: req.params.Id } };

            Promise.all([
                dataaccess.FindAndCountAll(UserSublobMap, param)
            ]).then(function (Users) {
                if (Users != null && Users.count > 0) {
                    res.status(200).json({ Success: true, Message: 'Can not deactivate this Sublob, its already used in user master', Data: true });
                }
                else {
                    res.status(200).json({ Success: false, Message: 'Can deactivate, Sublob is not used', Data: false });
                }
            }).catch(err => {
                dataconn.errorlogger('SublobService', 'CheckActiveSublob', err);
                res.status(200).json({ Success: false, Message: 'User has no access of sublob', Data: null });
            });
        });

    return router;;
};

module.exports = routes;