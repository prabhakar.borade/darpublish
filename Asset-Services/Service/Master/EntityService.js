var express = require('express');
var router = express.Router();
var connect = require('../../Data/Connect');
var datamodel = require('../../Data/DataModel');
var dataaccess = require('../../Data/DataAccess');
var dataconn = require('../../Data/DataConnection');
var mailer = require('../../Common/Mailer');
var commonfunc = require('../../Common/CommonFunctions');
var async = require('async');
var promise = connect.Sequelize.Promise;

var routes = function () {

    router.route('/GetAllEntity')
        .get(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            var param = { attributes: ['Id', 'Code', 'Desc', 'IsActive'], order: ['Code'] };

            dataaccess.FindAll(EntityMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Entity Access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of entity', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('EntityService', 'GetAllEntity', err);
                    res.status(200).json({ Success: false, Message: 'user has no access of Entity', Data: null });
                });
        });

    router.route('/GetAllActiveEntity')
        .get(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            var param = { where: { IsActive: true }, attributes: ['Id', 'Code'] };

            dataaccess.FindAll(EntityMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Entity access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Entity', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('EntityService', 'GetAllActiveEntity', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Entity', Data: null });
                });
        });

    router.route('/GetEntityById/:Id')
        .get(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            var param = { where: { Id: req.params.Id } };

            dataaccess.FindOne(EntityMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Entity access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Entity', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('EntityService', 'GetEntityById', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Entity', Data: null });
                });

        });

    router.route('/CheckDuplicateEntity/:Value/:Id')
        .get(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            var param = {
                where: { Code: req.params.Value, Id: { [connect.Op.ne]: req.params.Id } },
                attributes: [[connect.sequelize.fn('count', connect.sequelize.col('Code')), 'Count']]
            };

            dataaccess.FindAll(EntityMst, param)
                .then(function (result) {
                    if (result != null && result.length > 0 && result[0].dataValues.Count != null && result[0].dataValues.Count > 0) {
                        res.status(200).json({ Success: true, Message: 'Entity already exists', Data: true });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'Entity does not exists', Data: false });
                    }
                },
                    function (err) {
                        dataconn.errorlogger('EntityService', 'CheckDuplicateEntity', err);
                        res.status(200).json({ Success: false, Message: 'User has no access of Entity', Data: null });
                    });
        });

    router.route('/CreateEntity')
        .post(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                IsActive: req.body.IsActive,
                CreatedBy: req.body.UserId,
                CreatedByRoleId: req.body.UserRoleId,
            };
            dataaccess.Create(EntityMst, values)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Entity saved successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('EntityService', 'CreateEntity', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('EntityService', 'CreateEntity', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                });
        });

    router.route('/UpdateEntity')
        .post(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            var values = {
                Code: req.body.Code.toString().trim(),
                Desc: req.body.Desc,
                IsActive: req.body.IsActive,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW'),
            };
            var param = { Id: req.body.Id };
            dataaccess.Update(EntityMst, values, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Entity updated successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('EntityService', 'UpdateEntity', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('EntityService', 'UpdateEntity', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });

    router.route('/BulkUpload')
        .post(function (req, res) {

            var bulkdata = req.body.data;
            var UserId = req.body.UserId;
            var UserRoleId = req.body.UserRoleId;

            if (bulkdata) {
                var bulkRequest = new Promise((resolve, reject) => {
                    async.eachOfSeries(bulkdata, (bd, next) => {

                        const EntityMst = datamodel.EntityMst();

                        dataaccess.FindAll(EntityMst)
                            .then(function (result) {

                                var values = {
                                    Code: bd.Code.toString().trim(),
                                    Desc: bd.Desc,
                                    IsActive: bd.Status.toLowerCase().trim() == 'active' ? true : false,
                                    CreatedBy: UserId,
                                    CreatedByRoleId: UserRoleId,
                                    ModifiedBy: UserId,
                                    ModifiedByRoleId: UserRoleId,
                                };

                                dataaccess.Create(EntityMst, values)
                                    .then(function (results) {
                                        if (results != null) {
                                            next();
                                        }
                                        else {
                                            next({ Success: false, Message: 'Error occurred while saving records', Data: null });
                                        }
                                    }, function (err) {
                                        dataconn.errorlogger('EntityService', 'BulkUpload', err);
                                        next(err);

                                    });
                            }, function (err) {
                                dataconn.errorlogger('EntityService', 'BulkUpload', err);
                                next(err);

                            });
                    },
                        err => {
                            if (err) reject(err);
                            else resolve(bulkdata);
                        });
                });

                bulkRequest.then((data) => {
                    res.status(200).json({ Success: true, Message: 'All entities saved successfully', Data: data });
                }).catch((err) => {
                    dataconn.errorlogger('EntityService', 'BulkUpload', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });

            } else {
                res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
            }
        });

    router.route('/GetAllEntityList')
        .get(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            var param = { attributes: ['Code'] };

            dataaccess.FindAll(EntityMst, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Entity access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Entity', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('EntityService', 'GetAllEntityList', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Entity', Data: null });
                });
        });

    router.route('/CheckActiveEntity/:Id')
        .get(function (req, res) {

            const EntityMst = datamodel.EntityMst();
            const UserMst = datamodel.UserMst();

            var param = { where: { EntityId: req.params.Id, IsActive: true } };

            Promise.all([
                dataaccess.FindAndCountAll(UserMst, param)
            ]).then(function (entity) {
                if (entity != null && entity.count > 0) {
                    res.status(200).json({ Success: true, Message: 'Can not deactivate this entity, its already used in user master', Data: true });
                }
                else {
                    res.status(200).json({ Success: false, Message: 'Can deactivate, entity is not used', Data: false });
                }
            }).catch(err => {
                dataconn.errorlogger('EntityService', 'CheckActiveEntity', err);
                res.status(200).json({ Success: false, Message: 'User has no access of BG', Data: null });
            });
        });

    return router;;
};

module.exports = routes;