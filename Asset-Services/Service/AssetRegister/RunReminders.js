var mailer = require('../../Common/Mailer');
var connect = require('../../Data/Connect');
var dataconn = require('../../Data/DataConnection');
const { Template } = require('ejs');

// Pending Reminders ----------------------------------------------------------------------------------------------

module.exports.RunPendingReminders = function(Template, Frequency){

    var querytext = 'SELECT "GetReminders"(:p_template, :p_frequency, :p_ref); FETCH ALL IN "reminders";';
    var params = {
        replacements : {
            p_template: Template,
            p_frequency: Frequency,
            p_ref: 'reminders'
        }, type: connect.sequelize.QueryTypes.SELECT
    }
    connect.sequelize
    .query(querytext, params)
    .then(function (result) {

        result.shift();
        var lobIds = [];
        result.forEach(element => {
            if(!lobIds.find(e => e.LobId == element.LobId)){
                lobIds.push({ LobId: element.LobId });
            }
        });

        lobIds.forEach(lob => {

            var lobresult = [];
            result.forEach(element => {
                if(lob.LobId == element.LobId) { lobresult.push(element); }
            });

            ///Approvers
            var queryApptext = 'SELECT "GetApproversByLobId"(:p_lobId, :p_ref); FETCH ALL IN "approver";';
            var params = {
                replacements: {
                    p_lobId: lob.LobId, p_ref: 'approver'
                }, type: connect.sequelize.QueryTypes.SELECT
            }
            connect.sequelize
            .query(queryApptext, params)
            .then(function (appresult) {
                appresult.shift();

                var toEmail = [];
                appresult.forEach(element => {

                    if(!toEmail.includes(element.EmailId)){
                        toEmail.push({ EmailId: element.EmailId });
                    }
                });
                toEmail.forEach(user => {

                    if(lobresult != null && lobresult.length > 0 ){

                        var subject = 'Asset Register - Approval Pending';
                        var tplname = 'Request/Approval';
                        var templateData = { data: lobresult, ui_url: mailer.ui_url };
                        mailer.sendMail(
                            undefined,
                            undefined,
                            'notification',
                            undefined,
                            user.EmailId,
                            undefined,
                            undefined,
                            subject,
                            tplname,
                            templateData
                        ).then(function (emailsresult) {
                             dataconn.errorlogger('Reminders', 'RunPending', emailsresult);
                        });
                    }
                });
            });
        }); 
    });
}

//-----------------------------------------------------------------------------------------------------------------

// NearExpiry Reminders ----------------------------------------------------------------------------------------------

module.exports.RunNearExpiryReminders = function(Template, Frequency){

    /// CC Users Ids

    var toEmailCC = [];
    var toEmailCCIds = "";

    var querytext = 'SELECT "GetRemindersCC"(:p_template, :p_frequency, :p_ref); FETCH ALL IN "reminder";';
    var params = {
        replacements : {
            p_template: Template,
            p_frequency: Frequency,
            p_ref: 'reminder'
        }, type: connect.sequelize.QueryTypes.SELECT
    }
    connect.sequelize
    .query(queryApptext, params)
    .then(function (appresult) {

        appresult.shift();

        appresult.forEach(element => {
            if(!toEmailCC.find(e => e.EmailId == element.EmailId)){
                toEmailCC.push({ EmailId: element.EmailId });
                toEmailCCIds = toEmailCCIds + element.EmailId + "; "
            }
        });
    });

    var querytext = 'SELECT "GetReminders"(:p_template, :p_frequency, :p_ref); FETCH ALL IN "reminders";';
    var params = {
        replacements : {
            p_template: Template,
            p_frequency: Frequency,
            p_ref: 'reminders'
        }, type: connect.sequelize.QueryTypes.SELECT
    }
    connect.sequelize
    .query(querytext, params)
    .then(function (result) {

        result.shift();

        var toEmail = [];
        var toEmailIds = "";

        result.forEach(element => {
            if(!toEmail.find(e => e.EmpEmailId == element.EmpEmailId)){
                toEmail.push({ EmpEmailId: element.EmpEmailId });
                toEmailIds = toEmailIds + element.EmpEmailId + "; "
            }

            if(!toEmail.find(e => e.EmpEmailId == element.TechEmailId)){
                toEmail.push({ EmpEmailId: element.TechEmailId });
                toEmailIds = toEmailIds + element.TechEmailId + "; "
            }
        });

        var subject = 'Asset Register - Validity Near By Expiry';
        var tplname = 'Request/NearExpiry';

        if(result != null && result.length > 0 ){
            var templateData = { data: result, ui_url: mailer.ui_url };
            mailer.sendMail(
                undefined,
                undefined,
                'notification',
                undefined,
                toEmailIds,
                toEmailCCIds,
                undefined,
                subject,
                tplname,
                templateData
            ).then(function (emailsresult) {
                 dataconn.errorlogger('Reminders', 'NearExpiry', emailsresult);
            });
        }
    });
}

//---------------------------------------------------------------------------------------------------------------


// Expiry Reminders ----------------------------------------------------------------------------------------------

module.exports.RunValidityExpiredReminders = function(Template, Frequency){

    /// CC Users Ids

    var toEmailCC = [];
    var toEmailCCIds = "";

    var querytext = 'SELECT "GetRemindersCC"(:p_template, :p_frequency, :p_ref); FETCH ALL IN "reminder";';
    var params = {
        replacements : {
            p_template: Template,
            p_frequency: Frequency,
            p_ref: 'reminder'
        }, type: connect.sequelize.QueryTypes.SELECT
    }
    connect.sequelize
    .query(queryApptext, params)
    .then(function (appresult) {

        appresult.shift();

        appresult.forEach(element => {
            if(!toEmailCC.find(e => e.EmailId == element.EmailId)){
                toEmailCC.push({ EmailId: element.EmailId });
                toEmailCCIds = toEmailCCIds + element.EmailId + "; "
            }
        });
    });

    var querytext = 'SELECT "GetReminders"(:p_template, :p_frequency, :p_ref); FETCH ALL IN "reminders";';
    var params = {
        replacements : {
            p_template: Template,
            p_frequency: Frequency,
            p_ref: 'reminders'
        }, type: connect.sequelize.QueryTypes.SELECT
    }
    connect.sequelize
    .query(querytext, params)
    .then(function (result) {

        result.shift();

        var toEmail = [];
        var toEmailIds = "";

        result.forEach(element => {
            if(!toEmail.find(e => e.EmpEmailId == element.EmpEmailId)){
                toEmail.push({ EmpEmailId: element.EmpEmailId });
                toEmailIds = toEmailIds + element.EmpEmailId + "; "
            }

            if(!toEmail.find(e => e.EmpEmailId == element.TechEmailId)){
                toEmail.push({ EmpEmailId: element.TechEmailId });
                toEmailIds = toEmailIds + element.TechEmailId + "; "
            }
        });

        var subject = 'Asset Register - Validity Expired';
        var tplname = 'Request/ValidityExpired';

        if(result != null && result.length > 0 ){
            var templateData = { data: result, ui_url: mailer.ui_url };
            mailer.sendMail(
                undefined,
                undefined,
                'notification',
                undefined,
                toEmailIds,
                toEmailCCIds,
                undefined,
                subject,
                tplname,
                templateData
            ).then(function (emailsresult) {
                 dataconn.errorlogger('Reminders', 'ValidityExpired', emailsresult);
            });
        }
    });
}

//---------------------------------------------------------------------------------------------------------------






























