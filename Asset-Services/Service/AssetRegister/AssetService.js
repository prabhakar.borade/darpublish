var express = require('express');
var router = express.Router();
var connect = require('../../Data/Connect');
var datamodel = require('../../Data/DataModel');
var dataaccess = require('../../Data/DataAccess');
var dataconn = require('../../Data/DataConnection');
var mailer = require('../../Common/Mailer');
var commonfunc = require('../../Common/CommonFunctions');
var async = require('async');
var promise = connect.Sequelize.Promise;

var routes = function () {

    router.route('/GetAllAsset/:UserId/:filter/:assetType/:group/:Type')
        .get(function (req, res) {

            var querytext = 'SELECT "GetAllAsset"(:p_UserId, :p_filter, :p_assetType, :p_group, :p_type, :p_ref); FETCH ALL IN "asset";';
            var param = {
                replacements: {
                    p_UserId: req.params.UserId, p_filter: req.params.filter, p_assetType: req.params.assetType, p_group: req.params.group,
                    p_type: req.params.Type, p_ref: 'asset'
                }
                , type: connect.sequelize.QueryTypes.SELECT
            }
            connect.sequelize
                .query(querytext, param)
                .then(function (result) {
                    result.shift();
                    res.status(200).json({ Success: true, Message: 'Asset Access', Data: result });
                },
                    function (err) {
                        dataconn.errorlogger('AssetRegisterService', 'GetAllAsset', err)
                        res.status(200).json({ Success: false, Message: 'User has no access', Data: null });
                    },
                    function (err) {
                        dataconn.errorlogger('AssetService', 'GetAllAsset', err);
                        res.status(200).json({ Success: false, Message: 'user has no access of Asset', Data: null });
                    });
        });

    router.route('/GetAllAssetList/:UserId/:Type')
        .get(function (req, res) {

            var querytext = 'SELECT "GetAllAssetList"(:p_UserId, :p_type, :p_ref); FETCH ALL IN "asset";';
            var param = {
                replacements: {
                    p_UserId: req.params.UserId, 
                    p_type: req.params.Type, p_ref: 'asset'
                }
                , type: connect.sequelize.QueryTypes.SELECT
            }
            connect.sequelize
                .query(querytext, param)
                .then(function (result) {
                    result.shift();
                    res.status(200).json({ Success: true, Message: 'Asset Access', Data: result });
                },
                    function (err) {
                        dataconn.errorlogger('AssetRegisterService', 'GetAllAssetList', err)
                        res.status(200).json({ Success: false, Message: 'User has no access', Data: null });
                    },
                    function (err) {
                        dataconn.errorlogger('AssetService', 'GetAllAssetList', err);
                        res.status(200).json({ Success: false, Message: 'user has no access of Asset', Data: null });
                    });
    });
    
    router.route('/GetAllActiveAsset')
        .get(function (req, res) {

            const AssetRegister = datamodel.AssetRegister();
            var param = { where: { IsActive: true }, attributes: ['Id', 'Code'] };

            dataaccess.FindAll(AssetRegister, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Asset access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Asset', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('AssetService', 'GetAllActiveAsset', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Asset', Data: null });
                });
        });

    router.route('/GetAssetRegisterById/:Id')
        .get(function (req, res) {

            const AssetRegister = datamodel.AssetRegister();
            const Lob = datamodel.LOBMst();
            const InventoryType = datamodel.InventoryTypeMst();
            const Entity = datamodel.EntityMst();
            const Sublob = datamodel.SublobMst();
            const Vendor = datamodel.VendorMst();
            const AssetRegisterHistory = datamodel.AssetRegisterHistory();

            var param = {
                where: { Id: req.params.Id },
                include: [{ model: Lob, attributes: ['Code'] },
                { model: InventoryType, attributes: ['Code'] },
                { model: Entity, attributes: ['Code'] },
                { model: Sublob, attributes: ['Code'] },
                { model: Vendor, attributes: ['Code'] },
                { model: AssetRegisterHistory, attributes: ['Reason'] },
                ]
            };

            dataaccess.FindOne(AssetRegister, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Asset access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Asset', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('AssetService', 'GetAssetRegisterById', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Asset', Data: null });
                });

        });

    router.route('/GetAssetById/:Id')
        .get(function (req, res) {

            var querytext = 'SELECT "GetAssetById"(:p_id, :p_ref); FETCH ALL IN "asset";';
            var param = {
                replacements: {
                    p_id: req.params.Id, p_ref: 'asset'
                }
                , type: connect.sequelize.QueryTypes.SELECT
            }
            connect.sequelize
                .query(querytext, param)
                .then(function (result) {
                    result.shift();
                    res.status(200).json({ Success: true, Message: 'Asset Access', Data: result });
                },
                    function (err) {
                        dataconn.errorlogger('AssetRegisterService', 'GetAllAsset', err)
                        res.status(200).json({ Success: false, Message: 'User has no access', Data: null });
                    },
                    function (err) {
                        dataconn.errorlogger('AssetService', 'GetAllAsset', err);
                        res.status(200).json({ Success: false, Message: 'user has no access of Asset', Data: null });
                    });
        });

    router.route('/CheckApproveRegister/:Value/:Id')
        .get(function (req, res) {

            const AssetRegister = datamodel.AssetRegister();
            var param = {
                where: { StatusId: { [connect.Op.in]: ['2', '-1'] }, Id: { [connect.Op.eq]: req.params.Id } }
            };

            dataaccess.FindAll(AssetRegister, param)
                .then(function (result) {
                    if (result != null && result.length > 0) {
                        res.status(200).json({ Success: true, Message: 'Asset already Approved/Rejected', Data: true });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'Asset does not Approved/Rejected', Data: false });
                    }
                },
                    function (err) {
                        dataconn.errorlogger('AssetService', 'CheckDuplicateAsset', err);
                        res.status(200).json({ Success: false, Message: 'User has no access of Asset', Data: null });
                    });
        });

    router.route('/CreateAssetRegister')
        .post(function (req, res) {

            const AssetRegister = datamodel.AssetRegister();
            var values = {

                EntityId: req.body.EntityId,
                LobId: req.body.LobId,
                SublobId: req.body.SublobId,
                VendorId: req.body.VendorId,
                InventoryTypeId: req.body.InventoryId,
                InventoryName: req.body.InventoryName.toString().trim(),
                IPAddress: req.body.IPAddress.toString().trim(),
                ValidityFrom: req.body.ValidityFrom,
                ValidityTo: req.body.ValidityTo,
                Purpose: req.body.Purpose.toString().trim(),
                ADUser: req.body.ADUser,
                EmpName: req.body.EmpName.toString().trim(),
                EmpEmailId: req.body.EmailId.toString().trim(),
                EmpContactNumber: req.body.ContactNo,
                TechName: req.body.TechName.toString().trim(),
                TechEmailId: req.body.TechEmailId.toString().trim(),
                TechContactNumber: req.body.TechContactNo,
                Dependency: req.body.Dependency.toString().trim(),
                ImpactFailure: req.body.Impact.toString().trim(),
                Reason: req.body.Reason.toString().trim(),
                Remarks: req.body.Remarks.toString().trim(),
                IsActive: true,
                CreatedBy: req.body.UserId,
                CreatedByRoleId: req.body.UserRoleId,
                StatusId: req.body.Action
            };
            console.log("inside create", values)
            dataaccess.Create(AssetRegister, values)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Asset saved successfully', Data: result });

                        if (req.body.Action.toString() == "1")
                            NotifyAllUsers('Submitted', result.Id);
                    }
                    else {
                        dataconn.errorlogger('AssetService', 'CreateAsset', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                    }
                },
                    function (err) {
                        dataconn.errorlogger('AssetService', 'CreateAsset', err);
                        res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
                    });
        });

    router.route('/UpdateAssetRegister')
        .post(function (req, res) {
            console.log("inside update")
            const AssetRegister = datamodel.AssetRegister();
            var values = {
                EntityId: req.body.EntityId,
                LobId: req.body.LobId,
                SublobId: req.body.SublobId,
                VendorId: req.body.VendorId,
                InventoryTypeId: req.body.InventoryId,
                InventoryName: req.body.InventoryName.toString().trim(),
                IPAddress: req.body.IPAddress.toString().trim(),
                ValidityFrom: req.body.ValidityFrom,
                ValidityTo: req.body.ValidityTo,
                Purpose: req.body.Purpose.toString().trim(),
                ADUser: req.body.ADUser,
                EmpName: req.body.EmpName.toString().trim(),
                EmpEmailId: req.body.EmailId.toString().trim(),
                EmpContactNumber: req.body.ContactNo,
                TechName: req.body.TechName.toString().trim(),
                TechEmailId: req.body.TechEmailId.toString().trim(),
                TechContactNumber: req.body.TechContactNo,
                Dependency: req.body.Dependency.toString().trim(),
                ImpactFailure: req.body.Impact.toString().trim(),
                Reason: req.body.Reason.toString().trim(),
                Remarks: req.body.Remarks.toString().trim(),
                IsActive: true,
                CreatedBy: req.body.UserId,
                CreatedByRoleId: req.body.UserRoleId,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW'),
                StatusId: req.body.Action
            };
            var param = { Id: req.body.Id };
            dataaccess.Update(AssetRegister, values, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Asset updated successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('AssetService', 'UpdateAssetRegister', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('AssetService', 'UpdateAssetRegister', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });

    router.route('/DeactivateAssetRegister')
        .post(function (req, res) {
            
            const AssetRegister = datamodel.AssetRegister();
            var values = {

                Remarks: req.body.Remarks.toString().trim(),
                IsActive: false,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW')
            };
            console.log("inside deavtivate", values)
            var param = { Id: req.body.Id };
            dataaccess.Update(AssetRegister, values, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Asset updated successfully', Data: result });
                    }
                    else {
                        dataconn.errorlogger('AssetService', 'DeactivateAssetRegister', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('AssetService', 'DeactivateAssetRegister', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });

    router.route('/ApproveRegister')
        .post(function (req, res) {

            const AssetRegister = datamodel.AssetRegister();
            var values = {

                StatusId: req.body.StatusId,
                ModifiedBy: req.body.UserId,
                ModifiedByRoleId: req.body.UserRoleId,
                ModifiedDate: connect.sequelize.fn('NOW')
            };
            var param = { Id: req.body.Id };
            dataaccess.Update(AssetRegister, values, param)
                .then(function (result) {
                    if (result != null) {

                        const AssetRegisterHistory = datamodel.AssetRegisterHistory();

                        var historyValue = {
                            AssetRegisterId: req.body.Id,
                            Reason: req.body.Reason,
                            StatusId: req.body.Action,
                            Action: req.body.Action,
                            CreatedBy: req.body.UserId,
                            CreatedByRoleId: req.body.UserRoleId,
                        };
                        dataaccess.Create(AssetRegisterHistory, historyValue)
                            .then(function (result) {
                                if (result != null) {
                                    if (req.body.Action.toString() == '2') {
                                        res.status(200).json({ Success: true, Message: 'Asset approved successfully', Data: result });
                                        NotifyAllUsers('Approved', req.body.Id);
                                    }
                                    else {
                                        res.status(200).json({ Success: true, Message: 'Asset rejected successfully', Data: result });
                                        NotifyAllUsers('Rejected', req.body.Id);
                                    }
                                }
                                else {
                                    dataconn.errorlogger('AssetService', 'UpdateAssetRegister', { message: 'No object found', stack: '' });
                                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                                }
                            });
                    }
                    else {
                        dataconn.errorlogger('AssetService', 'UpdateAssetRegister', { message: 'No object found', stack: '' });
                        res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('AssetService', 'UpdateAssetRegister', err);
                    res.status(200).json({ Success: false, Message: 'Error occurred while updating record', Data: null });
                });
        });
    router.route('/GetAllAssetList')
        .get(function (req, res) {

            const AssetRegister = datamodel.AssetRegister();
            var param = { attributes: ['Code'] };

            dataaccess.FindAll(AssetRegister, param)
                .then(function (result) {
                    if (result != null) {
                        res.status(200).json({ Success: true, Message: 'Asset access', Data: result });
                    }
                    else {
                        res.status(200).json({ Success: false, Message: 'User has no access of Asset', Data: null });
                    }
                }, function (err) {
                    dataconn.errorlogger('AssetService', 'GetAllAssetList', err);
                    res.status(200).json({ Success: false, Message: 'User has no access of Asset', Data: null });
                });
        });
     

    function NotifyAllUsers(type, AssetId) {

        if (type == "Submitted") {

            /// Confirmation Mail
            var querytext = 'SELECT "GetAssetById"(:p_id, :p_ref); FETCH ALL IN "asset";';
            var param = {
                replacements: {
                p_id: AssetId, p_ref: 'asset'
                }, type: connect.sequelize.QueryTypes.SELECT
            }
            connect.sequelize
                .query(querytext, param)
                .then(function (result) {
                    result.shift();
                    var toEmail = result[0].EmailId;
                    var subject = 'Asset Register Request Submitted';
                    var templateData = { ui_url: mailer.ui_url, data: result };
                    mailer.sendMail(
                        undefined,
                        undefined,
                        'notification',
                        undefined,
                        toEmail,
                        undefined,
                        undefined,
                        subject,
                        'Request/RequestRaised',
                        templateData
                    ).then(function (emailresult) {
                        console.log(emailresult);
                    });

                    /// Approvers
                    var queApptext = 'SELECT "GetApproversByAssetId"(:p_id, :p_ref); FETCH ALL IN "approver";';
                    var params = {
                        replacements: {
                            p_id: AssetId, p_ref: 'approver'
                        }, type: connect.sequelize.QueryTypes.SELECT
                    }
                    connect.sequelize
                        .query(queApptext, params)
                        .then(function (appresult) {
                            appresult.shift();

                            var toEmail = [];                          
                            appresult.forEach(element => {
                                if (!toEmail.includes(element.EmailId)) {
                                    toEmail.push({ EmailId: element.EmailId });
                                }
                            });
                            toEmail.forEach(user => {

                                var subject = 'Asset Register - Approval Pending';
                                var tplname = 'Request/Approval';
                                var templateData = { data: result, ui_url: mailer.ui_url };
                                mailer.sendMail(
                                    undefined,
                                    undefined,
                                    'notification',
                                    undefined,
                                    user.EmailId,
                                    undefined,
                                    undefined,
                                    subject,
                                    tplname,
                                    templateData
                                ).then(function (emailsresult) {
                                    console.log(emailsresult);
                                });
                            });
                        },
                            function (err) {
                                dataconn.errorlogger('AssetService', 'NotifyAllUsers', err);
                            });
                },
                    function (err) {
                        dataconn.errorlogger('AssetService', 'NotifyAllUsers', err);
                    });
        }
        
        if(type == "Approved") {

            var querytext = 'SELECT "GetAssetById"(:p_id, :p_ref); FETCH ALL IN "asset";';
            var param = {
                replacements: {
                    p_id: AssetId, p_ref: 'asset'
                }, type: connect.sequelize.QueryTypes.SELECT
            }
            connect.sequelize
            .query(querytext, param)
            .then (function (result) {
                result.shift();

                var toEmail = result[0].EmailId;
                var subject= 'Asset Register - Request Approved';
                var templateData = {
                    ui_url: mailer.ui_url, data: result
                };
                mailer.sendMail(
                    undefined,
                    undefined,
                    'notification',
                    undefined,
                    toEmail,
                    undefined,
                    undefined,
                    subject,
                    'Request/Approved',
                    templateData
                ).then(function (emailresult) {
                    console.log(emailresult);
                });
            },
            function(err){
                dataconn.errorlogger('AssetService', 'NotifyAllUsers', err);            
            });
        }
 
        if(type == "Rejected") {

            var querytext = 'SELECT "GetAssetById"(:p_id, :p_ref); FETCH ALL IN "asset";';
            var param = {
                replacements: {
                    p_id: AssetId, p_ref: 'asset'
                }, type: connect.sequelize.QueryTypes.SELECT
            }
            connect.sequelize
            .query(querytext, param)
            .then (function (result) {
                result.shift();

                var toEmail = result[0].EmailId;
                var subject= 'Asset Register - Request Rejected';
                var templateData = {
                    ui_url: mailer.ui_url, data: result
                };
                mailer.sendMail(
                    undefined,
                    undefined,
                    'notification',
                    undefined,
                    toEmail,
                    undefined,
                    undefined,
                    subject,
                    'Request/Rejected',
                    templateData
                ).then(function (emailresult) {
                    console.log(emailresult);
                });
            },
            function(err){
                dataconn.errorlogger('AssetService', 'NotifyAllUsers', err);            
            });
        }

    };

    return router;;
};

module.exports = routes;